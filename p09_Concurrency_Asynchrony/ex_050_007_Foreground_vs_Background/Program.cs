﻿using System;
using System.IO;
using System.Threading;

namespace ex_050_007_Foreground_vs_Background
{
    class Program
    {
        //le travail que vont faire les 2 threads pendant qu'ils s'exécutent
        static void ThreadAtWork(TextWriter tw, int i)
        {
            Console.WriteLine("{0} ({1})", i, Thread.CurrentThread.Name);
            tw.WriteLine("{0} ({1})", i, Thread.CurrentThread.Name);
            Thread.Sleep(1);
        }

        static void Main(string[] args)
        {
            //le premier thread affiche 200 fois l'entier i qui est incrémenté suivi du nom du thread entre parenthèses,
            //dans la console, et dans un fichier
            //La propriété IsBackground n'étant pas initialisée, elle prend la valeur par défaut qui est false
            //Ce thread est donc Foreground
            Thread threadAvant = new Thread(() =>
            {
                using (TextWriter tw = File.CreateText("avant.txt"))
                {
                    for (int i = 0; i < 200; i++)
                    {
                        ThreadAtWork(tw, i);
                    }
                }
            })
            {
                Name = "foreground thread"
            };

            //le deuxième thread affiche 2000 fois l'entier i qui est incrémenté suivi du nom du thread entre parenthèses,
            //dans la console, et dans un fichier
            //La propriété IsBackground est initialisée à true
            //Ce thread est donc Background
            Thread threadArrière = new Thread(() =>
            {
                using (TextWriter tw = File.CreateText("arriere.txt"))
                {
                    for (int i = 0; i < 2000; i++)
                    {
                        ThreadAtWork(tw, i);
                    }
                }
            })
            {
                Name = "background thread",
                IsBackground = true
            };

            //Les 2 threads sont lancés et le thread principal est immédiatement terminé
            threadAvant.Start();
            threadArrière.Start();

            //Vous pouvez observer que "foreground thread" est exécuté jusqu'à la fin,
            //et que le fichier créé avant.txt est bien écrit et fermé correctement (le bloc using est correctement exécuté jusqu'à la fin).
            //Vous pouvez observer que "background thread" n'est pas exécuté jusqu'à la fin (il continue jusqu'à la fin de foreground thread),
            //et que le fichier créé arriere.txt n'est pas complétement écrit (le bloc using n'est pas terminé correctement) et Dispose
            //n'est donc pas correctement appelé. Si vous voulez vous en assurer, avec un background thread, il faudra l'attendre.

            //Note : les 2 fichiers sont accessibles après exécution dans l'explorateur de solutions, au niveau de ce projet.
        }
    }
}
