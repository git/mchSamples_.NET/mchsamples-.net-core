﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ex_051_005_TaskCompletionSource
{
    class Program
    {
        static void Main(string[] args)
        {
            //une dernière façon d'écrire des tâches : en utilisant TaskCompletionSource
            //TaskCompletionSource vous permet de créer une tâche à partir de n'importe quelle opération
            //qui commence et terminera plus tard. Vous décidez manuellement de sa fin en donnant le résultat ou de son échec 
            // en levant une exception.
            //GetBestNumber utilise TaskCompletionSource et le code suivant utilise la tâche créée. 
            //Nous retrouverons TaskCompletionSource plus tard dans les exemples de programmation asynchrone.

            var awaiter = GetBestNumber().GetAwaiter();
            awaiter.OnCompleted(() => Console.WriteLine(awaiter.GetResult()));
            Console.ReadLine();
        }

        static Task<int> GetBestNumber()
        {
            var tcs = new TaskCompletionSource<int>();
            Thread.Sleep(5000);
            tcs.SetResult(73);
            return tcs.Task;
        }
    }
}
