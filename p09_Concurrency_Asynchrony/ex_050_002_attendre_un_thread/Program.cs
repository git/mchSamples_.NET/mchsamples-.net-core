﻿using System;
using System.Threading;

namespace ex_050_002_attendre_un_thread
{
    class Program
    {
        static Thread petitThread;
        static Thread petitThread2;

        static void Main(string[] args)
        {
            Console.WriteLine("\"petit thread\" joue un son pendant 2 secondes,");
            Console.WriteLine("\tpuis attend la fin de \"petit thread 2\",");
            Console.WriteLine("\tpuis affiche PETIT THREAD");
            Console.WriteLine("\"petit thread 2\" joue un son pendant 3 secondes,");
            Console.WriteLine("\tpuis affiche PETIT THREAD 2");
            Console.WriteLine("le thread principal lance les deux petits threads,");
            Console.WriteLine("\taffiche leur état,");
            Console.WriteLine("\tpuis attend 2,5 secondes");
            Console.WriteLine("\taffiche leur état,");
            Console.WriteLine("\tpuis attend les deux petits threads");
            Console.WriteLine("\tet affiche une dernière fois leur état,");

            //crée deux threads
            petitThread = new Thread(Wait2000);
            petitThread2 = new Thread(Wait3000);
            //donner un nom au thread est optionnel mais peut faciliter le débogage
            petitThread.Name = "petit thread";
            petitThread2.Name = "petit thread 2";

            PrintThreadsState(petitThread, petitThread2);

            Console.WriteLine("\"petit thread\" et \"petit thread 2\" démarrent");
            //lance les threads
            petitThread.Start();
            petitThread2.Start();
            //le code suivant s'exécute en parallèle de Write0

            PrintThreadsState(petitThread, petitThread2);

            Thread.Sleep(2500);
            Console.WriteLine("2,5 secondes se sont écoulées.");
            Console.WriteLine("\"petit thread\" est en attente, état : WaitSleepJoin");
            Console.WriteLine("\"petit thread 2\" est en cours d'exécution, état : Running");

            PrintThreadsState(petitThread, petitThread2);

            petitThread.Join();
            petitThread2.Join();

            PrintThreadsState(petitThread, petitThread2);
        }

        //méthode qui va être exécutée en parallèle
        static void Wait2000()
        {
            Console.Beep(26000, 2000);
            petitThread2.Join();
            Console.WriteLine("PETIT THREAD");
        }

        //méthode qui va être exécutée en parallèle
        static void Wait3000()
        {
            Console.Beep(26000, 3000);
            Console.WriteLine("PETIT THREAD 2");
        }

        static void PrintThreadsState(params Thread[] threads)
        {
            Console.WriteLine();
            Console.WriteLine("état des threads:");
            foreach (Thread thread in threads)
            {
                Console.WriteLine("\t{0} : {1}", thread.Name, thread.ThreadState);
            }
            Console.WriteLine();
        }
    }
}
