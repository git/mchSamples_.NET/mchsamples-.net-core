﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ex_051_004_poursuite_continuations
{
    class Program
    {
        static void Main(string[] args)
        {
            //lorsqu'on appelle GetAwaiter sur tacheLongue (de type Task ou Task<...>), 
            //on récupère un "awaiter" dont la méthode OnCompleted permet d'indiquer une instance de délégué 
            //à exécuter dès que tacheLongue sera terminée

            //exemple 1 : tacheLongue
            Task<float> tacheLongue = Task.Run(() =>
            {
                Thread.Sleep(3000);
                return 3.14f;
            });

            var awaiter = tacheLongue.GetAwaiter();
            awaiter.OnCompleted(() =>
                {
                    float result = awaiter.GetResult();
                    Console.WriteLine(result);
                });

            tacheLongue.Wait();

            //exemple 2 : tacheCourte
            //si la tâche est déjà terminée lorsqu'on veut attacher une méthode à exécuter à la fin, 
            // rien de grave, cela fonctionne quand même.
            Task<int> tacheCourte = Task.Run(() => { return 2; });
            var awaiter2 = tacheCourte.GetAwaiter();
            awaiter2.OnCompleted(() =>
                {
                    int result = awaiter2.GetResult();
                    Console.WriteLine(result);
                });

            tacheCourte.Wait();
            System.Threading.Thread.Sleep(10);

            //exemple 3 : ContinueWith, pratique pour chainer
            Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Write(".");
                    System.Threading.Thread.Sleep(100);
                }
                Console.WriteLine();
            }).ContinueWith(tachePrécédente =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("*");
                    System.Threading.Thread.Sleep(100);
                }
                Console.WriteLine();
                return 3;
            }).ContinueWith(tachePrécédente =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("-");
                    System.Threading.Thread.Sleep(100);
                }
                Console.WriteLine(tachePrécédente.Result);
            });
            Console.ReadLine();
        }
    }
}
