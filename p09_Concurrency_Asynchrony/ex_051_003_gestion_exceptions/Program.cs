﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_051_003_gestion_exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            //contrairement aux threads, si une tâche lance une exception, elle peut être capturée par le thread ayant lancé la tâche
            Task<int> task = Task.Run(() => { int a = 1; int b = 0; return a / b; });

            try
            {
                int result = task.Result;
            }
            //l'exception est wrappée dans une AggregateException
            //(l'intérêt est plus visible dans le cas de la programmation en parallèle)
            catch(DivideByZeroException exc)
            {

            }
            //catch (AggregateException aggrEx)
            //{
            //    if (aggrEx.InnerException is DivideByZeroException)
            //    {
            //        Console.WriteLine("division par zéro");
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}
        }
    }
}
