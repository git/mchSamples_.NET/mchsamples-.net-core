﻿using System;
using System.Threading;

namespace ex_050_001_CreatingAThread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("main thread affiche cent 1");
            Console.WriteLine("\"petit thread\" affiche deux cents 0");

            //crée un thread
            Thread thread = new Thread(Write0);
            //donner un nom au thread est optionnel mais peut faciliter le débogage
            thread.Name = "petit thread";

            Console.WriteLine("\"petit thread\" démarre");
            //lance le thread
            thread.Start();
            //le code suivant s'exécute en parallèle de Write0

            for (int i = 0; i < 100; i++)
            {
                Console.Write("1"); Console.Beep(12000, 10);
            }
        }

        //méthode qui va être exécutée en parallèle
        static void Write0()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.Write("0"); Console.Beep(16000, 10);
            }
        }
    }
}
