﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_052_005_problématique_5
{
    class Program
    {
        static Task<int> GetPrimesCountAsync(int start, int count)
        {
            return Task.Run(() => Enumerable.Range(start, count).Count(n =>
                Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));
        }

        static async void DisplayPrimeCounts()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} primes between {1} and {2}",
                                await GetPrimesCountAsync(i*1000000 + 2, 1000000),
                                i * 1000000,
                                (i + 1) * 1000000 - 1);
            }
            Console.WriteLine("Done !");
        }

        static void Main(string[] args)
        {
            DisplayPrimeCounts();
            Console.WriteLine("please wait...");
            Console.ReadLine();
        }


    }
}
