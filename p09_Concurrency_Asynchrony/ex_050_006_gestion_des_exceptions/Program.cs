﻿using System;
using System.Threading;

namespace ex_050_006_gestion_des_exceptions
{
    class Program
    {
        static void DivisionParZéroGérantException()
        {
            int a = 1, b = 0;
            try
            {
                int c = a / b;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void DivisionParZéroNEGérantPASException()
        {
            int a = 1, b = 0;
            int c = a / b;
        }

        static void Main(string[] args)
        {
            //Pour capturer une exception dans une méthode lancée par un thread,
            //il faut mettre le try/catch dans la méthode lancée par le thread
            new Thread(DivisionParZéroGérantException).Start();

            //si on tente de capturer l'exception lancée dans un thread secondaire, dans le thread principal,
            //l'exception n'est pas attrapée (puisqu'elle est lancée dans un autre thread)
            try
            {
                new Thread(DivisionParZéroNEGérantPASException).Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
