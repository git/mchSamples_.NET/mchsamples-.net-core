﻿using System;
using System.Threading.Tasks;

namespace ex_051_001_Démarrer_une_tâche
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("la tâche affiche deux cents 0");

            //crée une tâche
            //(cette version d'écriture date du framework 4.5)
            var task = Task.Run(() => Write0());

            //(la version du framework 4.0 était :)
            //Task.Factory.StartNew(() => Write0());

            for (int i = 0; i < 100; i++)
            {
                Console.Write("1"); Console.Beep(12000, 10);
            }

            //par défaut, une tâche est lancée sur le threadpool, et est donc en arrière plan
            //sans la ligne ci-dessous, le code de la tâche pourrait donc être arrêté avant qu'il ne se termine, 
            //si celui du thread principal se termine avant
            Console.WriteLine("tâche terminée ? {0}", task.IsCompleted);
            task.Wait();
            Console.WriteLine("tâche terminée ? {0}", task.IsCompleted);

            //Wait peut prendre une temps d'attente maximum pour arrêter la tâche
            task = Task.Run(() => Write0());
            task.Wait(500);
            Console.WriteLine("tâche arrêtée avant la fin");
        }

        //méthode qui va être exécutée en parallèle
        static void Write0()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.Write("0"); Console.Beep(16000, 10);
            }
        }
    }
}
