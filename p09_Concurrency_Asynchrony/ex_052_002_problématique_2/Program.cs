﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_052_002_problématique_2
{
    class Program
    {
        static Task<int> GetPrimesCountAsync(int start, int count)
        {
            return Task.Run(() => Enumerable.Range(start, count).Count(n =>
                Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));
        }


        static void DisplayPrimeCounts()
        {
            for (int i = 0; i < 10; i++)
            {
                var temp = i;
                var awaiter = GetPrimesCountAsync(temp * 1000000 + 2, 1000000).GetAwaiter();
                awaiter.OnCompleted(() =>
                Console.WriteLine("{0} primes between {1} and {2}",
                                    awaiter.GetResult(),
                                    temp * 1000000,
                                    (temp + 1) * 1000000 - 1));
            }
            Console.WriteLine("Done !");
        }
        
        static void Main(string[] args)
        {
            Task.Run(() => DisplayPrimeCounts());
            Console.WriteLine("please wait...");
            Console.ReadLine();
        }

        
    }
}
