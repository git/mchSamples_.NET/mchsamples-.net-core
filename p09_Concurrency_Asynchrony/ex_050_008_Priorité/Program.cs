﻿using System;
using System.Threading;

namespace ex_050_008_Priorité
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(() => { for (int i = 0; i < 10000; i++) Console.Write("x"); Console.WriteLine("FIN THREAD1"); });
            Thread thread2 = new Thread(() => { for (int i = 0; i < 10000; i++) Console.Write("y"); Console.WriteLine("FIN THREAD2"); });

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

            Console.WriteLine();
            Console.WriteLine();

            thread1 = new Thread(() => { for (int i = 0; i < 10000; i++) Console.Write("x"); Console.WriteLine("FIN THREAD1"); })
                {
                    Priority = ThreadPriority.Highest
                };
            thread2 = new Thread(() => { for (int i = 0; i < 10000; i++) Console.Write("y"); Console.WriteLine("FIN THREAD2"); })
                {
                    Priority = ThreadPriority.Lowest
                };

            thread1.Start();
            thread2.Start();
        }
    }
}
