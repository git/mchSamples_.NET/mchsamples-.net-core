﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_051_006_TaskDelay
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task.Delay est l'équivalent de Thread.Sleep en version asynchrone
            Task.Delay(2000).GetAwaiter().OnCompleted(() => Console.WriteLine(73));

            Task.Delay(3000).ContinueWith(précédent => Console.WriteLine("coucou"))
                            .ContinueWith(précédent => Console.WriteLine("Appuyez sur entrée"));

            Console.WriteLine("attendez 73 et coucou, puis appuyez sur entrée");
            Console.ReadLine();
        }
    }
}
