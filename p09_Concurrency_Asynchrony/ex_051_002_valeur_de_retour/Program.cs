﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ex_051_002_valeur_de_retour
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task.Run prend un paramètre une instance du délégué Action (i.e., une méthode qui ne prend aucun paramètre et ne rend rien)
            //mais il existe aussi une version générique de Task (Task<TResult>) qui permet à Task.Run de prendre en paramètre
            // une instance du délégué Func<TResult>. 
            //Pour récupérer le résultat de l'instance de délégué, on utilise la propriété Result qui est de type TResult.
            //Si la tâche est terminée, Result donne le résultat. Sinon, le thread qui appelle Result est bloqué jusqu'à ce que la tâche 
            //soit terminée.

            //exemple 1
            Task<int> task = Task.Run(() => { return 1; });
            int result = task.Result;
            Console.WriteLine(result);

            //exemple 2
            Task<float> tacheLongue = Task.Run(() =>
                {
                    Thread.Sleep(3000);
                    return 3.14f;
                });

            Console.WriteLine("Calcul en cours...");
            Console.WriteLine(tacheLongue.Result);
        }
    }
}
