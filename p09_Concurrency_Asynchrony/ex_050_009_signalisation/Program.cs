﻿using System;
using System.Threading;

namespace ex_050_009_notifications_entre_threads
{
    class Program
    {
        static void Main(string[] args)
        {
            //préparation de deux signaux
            var signal = new AutoResetEvent(false);
            var signal2 = new AutoResetEvent(false);

            //thread qui joue 4 fois le refrain de la chanson
            new Thread(() =>
                {
                    for (int i = 0; i < 4; i++)
                    {
                        //attend le signal pour écrire le refrain
                        signal.WaitOne();
                        Console.WriteLine("\tUn'jolie fleur dans une peau d'vache");
                        Console.WriteLine("\tUn'jolie vach' déguisée en fleur,");
                        Console.WriteLine("\tQui fait la belle et qui vous attache,");
                        Console.WriteLine("\tPuis, qui vous mèn' par le bout du coeur");
                        Console.WriteLine();
                        //envoie le signal pour le prochain couplet
                        signal2.Set();
                    }
                    //nettoyage
                    signal.Dispose();
                    signal2.Dispose();
                }).Start();

            Console.WriteLine("Jamais sur Terre, il n'y eut d'amoureux");
            Console.WriteLine("Plus aveugles que moi dans tous les âges,");
            Console.WriteLine("Mais faut dir' qu'je m'étais crevé les yeux");
            Console.WriteLine("En regardant de trop près son corsage...");
            Console.WriteLine();

            //envoie le signal pour le prochain refrain
            signal.Set();
            //attend le signal pour écrire le prochain couplet
            signal2.WaitOne();

            Console.WriteLine("Le ciel l'avait pourvu' des mille appâts");
            Console.WriteLine("Qui vous font prendre feu dès qu'on y touche,");
            Console.WriteLine("L'en avait tant que je ne savais pas");
            Console.WriteLine("Ne savais plus où donner de la bouche...");
            Console.WriteLine();

            //envoie le signal pour le prochain refrain
            signal.Set();
            //attend le signal pour écrire le prochain couplet
            signal2.WaitOne();

            Console.WriteLine("Ell'n'avait pa de tête, ell'n'avait pas");
            Console.WriteLine("L'esprit beaucoup plus grand qu'un dé à coudre,");
            Console.WriteLine("Mais pour l'amour on ne demande pas");
            Console.WriteLine("Aux filles d'avoir inventé la poudre...");
            Console.WriteLine();

            //envoie le signal pour le prochain refrain
            signal.Set();
            //attend le signal pour écrire le prochain couplet
            signal2.WaitOne();

            Console.WriteLine("Puis un jour elle a pris la clef des champs");
            Console.WriteLine("En me laissant à l'âme un mal funeste");
            Console.WriteLine("Et toutes les herbes de la Saint-Jean");
            Console.WriteLine("N'ont pas pu me guérir de cette peste...");
            Console.WriteLine();

            Console.WriteLine("J'lui en ai bien voulu, mais à présent");
            Console.WriteLine("J'ai plus d'rancune et mon coeur lui pardonne");
            Console.WriteLine("D'avoir mis mon coeur à feu et à sang");
            Console.WriteLine("Pour qu'il ne puisse plus servir à personne...");
            Console.WriteLine();

            //envoie le signal pour le prochain refrain
            signal.Set();
        }
    }
}
