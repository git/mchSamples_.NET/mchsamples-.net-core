﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ex_050_011_ThreadPool
{
    class Program
    {
        static void Main(string[] args)
        {
            //méthode avec le framework 4.0
            ThreadPool.QueueUserWorkItem(rien => Console.WriteLine("coucou"));

            //conseillée depuis le framework 4.0 (les tasks sont par défaut sur le thread pool)
            Task.Run(() => Console.WriteLine("coucou2"));

            Thread.Sleep(1000);
        }
    }
}
