﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_052_001_problématique
{
    class Program
    {
        static int GetPrimesCount(int start, int count)
        {
            return Enumerable.Range(start, count).Count(n =>
                Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0));
        }

        static void DisplayPrimeCounts()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} primes between {1} and {2}",
                                    GetPrimesCount(i * 1000000 + 2, 1000000),
                                    i * 1000000,
                                    (i + 1) * 1000000 - 1);
            }
            Console.WriteLine("Done !");
        }

        static void Main(string[] args)
        {
            //DisplayPrimeCounts();

            Task.Run(() => DisplayPrimeCounts());
            Console.WriteLine("please wait...");
            Console.ReadLine();
        }
    }
}
