﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : DbContextInitializer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_014_EF_CF_TPH_Inheritance
{
    /// <summary>
    /// initialiseur de stratégies...
    /// </summary>
    static class DbContextInitializer
    {
        /// <summary>
        /// remplissage de la base avec des données stubbées.
        /// </summary>
        /// <param name="context">base à remplir</param>
        public static void Seed(FormesDBEntities context)
        {
            SetInitializer(context, InitializationStrategies.DropCreateDatabaseAlways);

            //données stubbées
            //simulation de données en utilisant les types du Model
            Forme carré = new Rectangle { Nom = "carré", Longueur = 2, Largeur = 2, X = 1, Y = 3 };
            Forme rectangle = new Rectangle { Nom = "rectangle", Longueur = 3, Largeur = 4, X = 5, Y = 6 };
            Forme cercle = new Cercle { Nom = "cercle", Rayon = 7, X = 8, Y = 9 };

            context.FormesSet.AddRange(new Forme[] { carré, rectangle, cercle });
            context.SaveChanges();
        }

        /// <summary>
        /// les différentes stratégies de création de la base
        /// </summary>
        public enum InitializationStrategies
        {
            CreateDatabaseIfNotExists,
            DropCreateDatabaseIfModelChanges,
            DropCreateDatabaseAlways
        }
        public static void SetInitializer(DbContext context, InitializationStrategies strategy)
        {
            switch (strategy)
            {
                //par défaut : crée la base seulement si elle n'existe pas
                default:
                case InitializationStrategies.CreateDatabaseIfNotExists:
                    context.Database.EnsureCreated();
                    break;

                //recrée la base même si elle existe déjà
                case InitializationStrategies.DropCreateDatabaseAlways:
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                    break;

                //recrée la base seulement si le modèle change : impossible aujourd'hui en Entity Framework Core...
                case InitializationStrategies.DropCreateDatabaseIfModelChanges:
                    throw new NotImplementedException("Le mode DropCreateDatabaseIfModelChanges ne peut pas encore exister sous Entity Framework Core");
            }
        }
    }
}
