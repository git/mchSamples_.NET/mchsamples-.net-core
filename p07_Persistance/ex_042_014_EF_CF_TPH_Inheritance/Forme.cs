﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Forme.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_014_EF_CF_TPH_Inheritance
{
    /// <summary>
    /// la classe mère
    /// </summary>
    class Forme
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId { get; set; }
        public string Nom { get; set; }

        public float X { get; set; }

        public float Y { get; set; }

        public override string ToString()
        {
            return $"La forme {Nom} est placée en ({X:0.00} ; {Y:0.00})";
        }
    }
}
