﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Cercle.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

namespace ex_042_014_EF_CF_TPH_Inheritance
{
    /// <summary>
    /// une classe fille de Forme
    /// </summary>
    class Cercle : Forme
    {
        public float Rayon { get; set; }

        public override string ToString()
        {
            return $"La cercle {Nom} de rayon {Rayon} est centré en ({X:0.00} ; {Y:0.00})";
        }
    }
}
