﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : FormesDBEntities.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using Microsoft.EntityFrameworkCore;

namespace ex_042_014_EF_CF_TPH_Inheritance
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Forme.
    /// 
    /// Pour permettre de réaliser un héritage de type TPH (Table Per class Hierarchy), deux méthodes s'offrent à nous :
    /// METHODE 1 :
    ///     - déclarez un DbSet pour la classe mère
    ///     - déclarez un DbSet pour chaque classe fille
    ///     - vous pouvez utiliser des annotations de données dans les classes
    /// METHODE 2 (fluent API) :
    ///     - réécrivez la méthode OnModelCreating,
    ///     - ajoutez des appels aux méthodes Entity<T>() (une par classe fille)
    ///     - vous pouvez en profiter pour préciser le nom de la colonne qui sert de Discriminator, 
    ///                                             et les noms associés aux classes filles dans cette colonne.
    ///     - le type de la classe fille se précise dans l'appel de HasValue<T> avec T du type de la classe fille.
    ///     
    /// Commentez et décommentez les deux méthodes pour les tester.
    /// </summary>
    class FormesDBEntities : DbContext
    {
        public virtual DbSet<Forme> FormesSet { get; set; }

        //METHODE 1 :
        public virtual DbSet<Rectangle> RectanglesSet { get; set; }
        public virtual DbSet<Cercle> CerclesSet { get; set; }
        //FIN METHODE 1

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_014_EF_CF_TPH_Inheritance.Formes.mdf;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ////METHODE 2 :
            //modelBuilder.Entity<Forme>()
            //    .HasDiscriminator<string>("formeType")
            //    .HasValue<Forme>("formeBase")
            //    .HasValue<Rectangle>("rectangle");

            //modelBuilder.Entity<Forme>()
            //    .HasDiscriminator<string>("formeType")
            //    .HasValue<Forme>("formeBase")
            //    .HasValue<Cercle>("cercle");
            ////FIN METHODE 2

            base.OnModelCreating(modelBuilder);
        }
    }
}
