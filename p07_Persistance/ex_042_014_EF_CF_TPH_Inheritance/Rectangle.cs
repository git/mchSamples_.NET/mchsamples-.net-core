﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Rectangle.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_014_EF_CF_TPH_Inheritance
{
    /// <summary>
    /// une classe fille de Forme
    /// </summary>
    class Rectangle : Forme
    {
        public float Longueur { get; set; }

        public float Largeur { get; set; }

        public override string ToString()
        {
            return $"La rectangle {Nom} de longueur {Longueur} et de largeur {Largeur} est placé en ({X:0.00} ; {Y:0.00})";
        }
    }
}
