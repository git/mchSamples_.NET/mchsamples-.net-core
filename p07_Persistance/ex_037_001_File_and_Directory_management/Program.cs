﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Console;

namespace ex_037_001_File_and_Directory_management
{
    class Program
    {
        /// <summary>
        /// System.IO possède un ensemble de types pour gérer les fichiers, les dossiers et leurs chemins d'accès.
        /// On trouve notamment : 
        /// - les classes statiques File et Directory
        /// - les classes non statiques FileInfo et DirectoryInfo
        /// - une classe Path permettant de construire les chemins d'accès.
        /// L'exemple suivant montre comment : partir du chemin de sortie de ce projet (i.e. le dossier où se trouvera l'exécutable généré), 
        ///                                    puis se diriger dans le dossier dans lequel se trouve le fichier Program.cs que vous lisez
        ///                                    puis utiliser les méthodes statiques de File et Directory pour vérifier que ce fichier existe.
        /// Il montre aussi comment récupérer des informations, copier un fichier, déplacer un fichier, crypter et décrypter, etc...
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Récupère le chemin du dossier courant (par défaut le dossier de sortie donné dans les propriétés du projet)
            string currentDir = Directory.GetCurrentDirectory();
            WriteLine($"Le dossier courant est : {currentDir}");
            ReadKey();

            //récupère le chemin du dossier parent
            DirectoryInfo parentDir = Directory.GetParent(currentDir);
            WriteLine($"Le dossier parent est : {parentDir.Name}, le chemin complet est {parentDir.FullName}");
            ReadKey();

            //récupère le chemin d'un ancêtre
            DirectoryInfo ancestorDir = new DirectoryInfo(Path.Combine(currentDir, "..//..//..//..//"));
            WriteLine($"Le dossier ancêtre, quatre niveaux au-dessus, est : {ancestorDir.Name}, le chemin complet est {ancestorDir.FullName}");
            ReadKey();

            //récupère l'ensemble des sous-dossiers de ce dossier
            //string[] subDirectories = Directory.GetDirectories(ancestorDir.FullName);
            //depuis C#4, la ligne ci-dessous est plus efficace que la ligne ci-dessus
            IEnumerable<string> subDirectories = Directory.EnumerateDirectories(ancestorDir.FullName);
            WriteLine("les sous-dossiers de ce dossier sont :");
            foreach (string subDir in subDirectories)
            {
                WriteLine(Path.GetFileName(subDir));
            }
            ReadKey();

            //vérifie que le dossier ex_037_001_File_and_Directory_management existe
            string dossierProjet = Path.Combine(ancestorDir.FullName, "ex_037_001_File_and_Directory_management");
            bool exist = Directory.Exists(dossierProjet);
            WriteLine($"le dossier ex_037_001_File_and_Directory_management existe-t-il ? {(exist ? "oui" : "non")}");
            ReadKey();

            //s'il existe, rentre dedans et liste l'ensemble des fichiers de ce dossier
            if (exist)
            {
                WriteLine("Il contient les fichiers suivants :");
                //string[] files = Directory.GetFiles(dossierProjet);
                //depuis C#4, la ligne ci-dessous est plus efficace que la ligne ci-dessous
                IEnumerable<string> files = Directory.EnumerateFiles(dossierProjet);
                foreach (string f in files)
                {
                    WriteLine(Path.GetFileName(f));
                }
                ReadKey();


                WriteLine("Il contient les fichiers *.cs suivants :");
                //string[] files = Directory.GetFiles(dossierProjet);
                //depuis C#4, la ligne ci-dessous est plus efficace que la ligne ci-dessous
                IEnumerable<string> filesCS = Directory.EnumerateFiles(dossierProjet, "*.cs");
                foreach (string f in filesCS)
                {
                    WriteLine(Path.GetFileName(f));
                }
                ReadKey();

                WriteLine("on copie le fichier Program.cs en copie.cs");
                //on impose le dossier courant comme étant le dossierProjet
                Directory.SetCurrentDirectory(dossierProjet);
                //on copie le fichier Program.cs en l'appelant copie.cs dans le dossier courant
                File.Copy(filesCS.First(), "copie.cs");
                ReadKey();

                WriteLine("on supprime le fichier copie.cs");
                //on supprime le fichier "copie.cs"
                File.Delete("copie.cs");
            }

            //et plein d'autres méthodes et propriétés que vous pouvez découvrir !
        }
    }
}
