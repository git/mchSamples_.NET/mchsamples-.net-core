﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IArtiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================

using System;
using System.Collections.Generic;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// façade immuable et couche abstraite commune pour le Model et le procurateur
    /// </summary>
    public interface IArtiste
    {
        Guid UniqueId
        {
            get;
        }

        string Prénom
        {
            get;
        }
        string Nom
        {
            get;
        }

        DateTime DateDeNaissance
        {
            get;
        }

        DateTime? DateDeMort
        {
            get;
        }

        ICollection<IAlbum> Albums { get; }
    }
}
