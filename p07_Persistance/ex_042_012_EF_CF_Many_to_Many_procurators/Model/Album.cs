﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Album.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================


using System;
using System.Collections.Generic;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// Album est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation many-many avec la classe Artiste via la propriété Artistes.
    /// La clé primaire est générée lors de l'insertion en table.
    /// 
    /// Aujourd'hui (21 octobre 2016), il n'est pas encore possible dde mapper deux classes POCO avec la Fluent API sans passer par une entité avec une table d'association.
    /// En conséquence, il faut créer une classe pour la table d'association. 
    /// Mais dans cet exemple, je ne souhaite pas modifier mon Model pour faire plaisir à Entity Framework, je le laisse donc tel quel...
    /// </summary>
    public class Album : IAlbum
    {
        public Album()
        {

        }
        public Guid UniqueId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public DateTime DateDeSortie
        {
            get; set;
        }

        public ICollection<IArtiste> Artistes { get; set; } = new List<IArtiste>(); 
    }
    
}
