﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IAlbum.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================

using System;
using System.Collections.Generic;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// façade immuable et couche abstraite commune pour le Model et le procurateur
    /// </summary>
    public interface IAlbum
    {
        Guid UniqueId { get; }
        string Titre { get; }
        DateTime DateDeSortie { get; }
        ICollection<IArtiste> Artistes { get; }
    }
}
