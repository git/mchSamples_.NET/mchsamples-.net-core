﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AlbumEF.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// procurateur pour la classe Album
    /// </summary>
    public class AlbumEF : Album
    {
        /// <summary>
        /// collection d'AlbumArtiste pour le lien avec la table d'association
        /// </summary>
        public ICollection<AlbumArtiste> AlbumsArtistes { get; set; } = new List<AlbumArtiste>();

        public new ICollection<IArtiste> Artistes
        {
            get
            {
                return AlbumsArtistes.Select<AlbumArtiste, IArtiste>(aa => aa.Artiste).ToList();
            }
        }
    }
}
