﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ArtisteEF.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// procurateur pour la classe Artiste
    /// </summary>
    public class ArtisteEF : Artiste
    {
        public new ICollection<IAlbum> Albums
        {
            get
            {
                return ArtistesAlbums.Select<AlbumArtiste, IAlbum>(aa => aa.Album).ToList();
            }
        }

        public ICollection<AlbumArtiste> ArtistesAlbums { get; set; } = new List<AlbumArtiste>();
    }
}
