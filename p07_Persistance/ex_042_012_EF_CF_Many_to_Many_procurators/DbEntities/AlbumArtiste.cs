﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AlbumArtiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-23
//
// ========================================================================

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    /// <summary>
    /// table permettant de réaliser la table d'association
    /// </summary>
    public class AlbumArtiste
    {
        /// <summary>
        /// un album...
        /// </summary>
        public AlbumEF Album { get; set; }
        /// <summary>
        /// ...lié à un artiste
        /// </summary>
        public ArtisteEF Artiste { get; set; }
    }
}
