﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Console;

namespace ex_042_012_EF_CF_Many_to_Many_procurators
{
    public class Program
    {
        /// <summary>
        /// Cet exemple montre comment construire une relation many-many dans la base de données en utilisant la Fluent API d'Entity Framework.
        /// 
        /// Aujourd'hui (21 octobre 2016), il n'est malheureusement toujours pas possible de créer une table d'association automatiquement à partir de deux classes en relation many to many,
        /// sans créer à la main une classe pour la table d'association.
        /// 
        /// En attendant que cela soit mis en oeuvre par Microsoft (bientôt espérons-le...), cet exemple propose une solution pour permettre d'utiliser un modèle, sans le modifier, et sans qu'il soit explicitement lié aux tables.
        /// Nous avons donc ici un Model (dossier Model) avec : 
        /// - une classe Album et sa façade immuable IAlbum,
        /// - une classe Artiste et sa façade immuable IArtiste,
        /// - la classe Album possède une collection d'Artistes,
        /// - la classe Artiste possède une collection d'Albums.
        /// 
        /// Dans le dossier DbEntities, il y a la mise en place d'un procurateur pour la classe Album (AlbumEF) et pour la classe Artiste (ArtisteEF), 
        /// qui sont en liaison many to many en utilisant l'entité AlbumArtiste, c'est-à-dire que :
        /// - AlbumEF possède une collection d'AlbumArtiste,
        /// - ArtisteEF possède une collection d'AlbumArtiste.
        /// 
        /// La classe AlbumArtisteDBEntites crée le modèle et les tables.
        /// 
        /// La classe DbContextInitializer remplit les tables avec des données stubbées.
        /// 
        /// Je conseille plus cette solution que la précédente, car elle n'oblige pas à modifier les classes du modèle. Néanmoins, elle est assez lourde à mettre en oeuvre.
        /// 
        /// Si vous ouvrez la base de données (via l'explorateur d'objets SQL Server), vous pourrez constater la création d'une table d'association.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                //création du DbContext et injection de la dépendance à MyStubDataInitializationStrategy
                using (AlbumArtisteDBEntities db = new AlbumArtisteDBEntities())
                {
                    //choix de la stratégie et remplissage avec des données stubbées
                    DbContextInitializer.Seed(db);
                }
                using (AlbumArtisteDBEntities db = new AlbumArtisteDBEntities())
                {

                    WriteLine("Albums : ");
                    foreach (var album in db.Albums.Include(a => a.AlbumsArtistes).ThenInclude(aa => aa.Artiste))
                    {
                        WriteLine($"\t{album.UniqueId}: {album.Titre} (sorti le : {album.DateDeSortie.ToString("d")})\n");
                        foreach (var artiste in album.Artistes)
                        {
                            WriteLine($"\t\t{artiste.Prénom} {artiste.Nom}");
                        }
                    }

                    WriteLine();

                    WriteLine("Artistes :");
                    foreach (var artiste in db.Artistes.Include(a => a.ArtistesAlbums).ThenInclude(aa => aa.Album))
                    {
                        var annéeDeMort = artiste.DateDeMort.HasValue ? $" - {artiste.DateDeMort.Value.Year}" : "";
                        var titresAlbums = artiste.Albums.Aggregate(String.Empty, (albums, album) => albums + $"\"{album.Titre}\" ");
                        WriteLine($"\t{artiste.UniqueId}: {artiste.Prénom} {artiste.Nom} ({artiste.DateDeNaissance.Year}{annéeDeMort}) (albums: {titresAlbums})");
                    }

                    //WriteLine("Albums : ");
                    //foreach (var album in db.Albums.Include(a => a.AlbumsArtistes))
                    //{
                    //    WriteLine($"\t{album.UniqueId}: {album.Titre} (sorti le : {album.DateDeSortie.ToString("d")})");
                    //    foreach (var artiste in album.Artistes)
                    //    {
                    //        WriteLine($"\t\t{artiste.Prénom} {artiste.Nom}");
                    //    }
                    //}

                }
            }
            catch (NotImplementedException exception)
            {
                WriteLine(exception.Message);
            }
            catch (Exception e)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions suivantes (que vous retrouvez en commentaires dans la classe Program) :");
                WriteLine("Pour créer la base, ouvrez un invite de commandes et placez-vous dans le dossier de ce projet, ou bien,");
                WriteLine("- dans Visual Studio ouvrez la Console du Gestionnaire de package (Outils -> Gestionnaire de package NuGet -> Console du Gestionnaire de package),");
                WriteLine("- dans cette Console, vous devriez être dans le dossier de la solution, déplacez-vous dans celui du projet (ici : cd ex_042_011_EF_CF_Many_to_Many_FluentAPI)");
                WriteLine("- tapez : dotnet restore (pour restaurer les packages .NET Core)");
                WriteLine("- tapez : dotnet ef migrations add MyFirstMigration");
                WriteLine("    note : vous pourrez détruire le dossier Migrations une fois la base créée");
                WriteLine("- tapez : dotnet ef database update");
                WriteLine("    Ceci génère la base de données en utilisant la migration, et en particulier votre classe DBContext et vos classes POCO.");
            }

            ReadLine();
        }
    }
}
