﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Animal.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System.Xml.Serialization;

namespace ex_038_007_XmlSerializer
{
    /// <summary>
    /// Les attributs XmlInclude permettent d'informer le XmlSerializer qu'il existe des classes filles d'Animal qui pourront être sérialisées
    /// Tous les membres et propriétés publiques sont sérialisés
    /// </summary>
    [XmlInclude(typeof(Chat))]
    [XmlInclude(typeof(Chien))]
    public class Animal
    {
        /// <summary>
        /// par défaut, un membre ou une propriété est sérialisé en élément XML. Mais vous pouvez forcer sa sérialisation en attribut avec XmlAttribute.
        /// Vous pouvez en profiter pour modifier le nom de l'attribut XML (ici on le passe en minuscules).
        /// </summary>
        [XmlAttribute(AttributeName = "nom")]
        public string Nom
        {
            get;
            set;
        }

        /// <summary>
        /// XmlElement n'est pas obligatoire mais il donne accès à quelques options comme Order, le nom de l'élément...
        /// </summary>
        [XmlElement(ElementName = "bruit")]
        public string Bruit
        {
            get;
            set;
        }

        /// <summary>
        /// comme pour le BinarySerializer et contrairement au DataContractSerializer, il faut préciser avec XmlIgnore qu'on ne veut pas sérialiser un membre ou une propriété.
        /// </summary>
        [XmlIgnore]
        public int UnePropriétéQuOnNeVeutPasSerialiser
        {
            get;
            set;
        } = 42;

        public Animal(string nom, string bruit)
        {
            Nom = nom;
            Bruit = bruit;
        }

        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public Animal()
        { }
    }
}
