﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Chien.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

namespace ex_038_007_XmlSerializer
{
    public class Chien : Animal
    {
        public int NbOsRongés
        {
            get;
            set;
        }

        public Chien(string name, int nbOsRongés) : base(name, "Wouf Wouf")
        {
            NbOsRongés = nbOsRongés;
        }

        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public Chien()
        { }
    }
}
