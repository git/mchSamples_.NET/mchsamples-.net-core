﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Oiseau.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;

namespace ex_038_007_XmlSerializer
{
    public class Oiseau : Animal
    {
        public TimeSpan TempsDeVol
        {
            get; set;
        }

        public Oiseau(string nom, TimeSpan tempsDeVol) : base(nom, "Cui Cui")
        {
            TempsDeVol = tempsDeVol;
        }

        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public Oiseau() { }
    }
}
