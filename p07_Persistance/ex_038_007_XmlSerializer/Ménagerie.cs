﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Ménagerie.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ex_038_007_XmlSerializer
{
    /// <summary>
    /// Ménagerie contient deux collections : une de Chat et une de Animal.
    /// Ces collections sont privées (List<Chat> et List<Animal>).
    /// Si on les encapsule en lecture seule par des propriétés publiques de type IEnumerable, on ne pourra pas les sérialiser.
    /// XmlSerializer ne sait pas sérialiser ces types. Il veut pouvoir les désérialiser.
    /// 
    /// Notez également qu'une collection d'un type père se sérialise parfaitement en sérialisant les items de types fils.
    /// </summary>
    public class Ménagerie
    {
        public string Nom { get; set; }

        public List<Chat> Chats => chats;

        private List<Chat> chats = new List<Chat> {
            new Chat("Isidore", TimeSpan.FromHours(4)),
            new Chat("Garfield", TimeSpan.FromHours(12)),
            new Chat("GrosMinet", TimeSpan.FromHours(3))};

        /// <summary>
        /// XmlArray vous permet de modifier le nom extérieur de la collection
        /// XmlArrayItem vous permet de modifier le nom d'un élément fils en fonction de son type
        /// </summary>
        [XmlArray(ElementName = "animaux")]
        [XmlArrayItem(ElementName = "chat", Type = typeof(Chat))]
        [XmlArrayItem(ElementName = "chien", Type = typeof(Chien))]
        [XmlArrayItem(ElementName = "oiseau", Type = typeof(Oiseau))]
        public List<Animal> Animaux => animaux;

        private List<Animal> animaux = new List<Animal>
        {
            new Chat("GrosMinet", TimeSpan.FromHours(3)),
            new Oiseau("Titi", TimeSpan.FromHours(1)),
            new Chien("Idefix", 50)
        };

        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public Ménagerie() { }
    }
}
