﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AnimalList.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;
using System.Collections.Generic;

namespace ex_038_007_XmlSerializer
{
    /// <summary>
    /// AnimalList est une classe qui dérive d'une collection.
    /// Notez qu'il est parfaitement sérialisé.
    /// </summary>
    public class AnimalList : List<Animal>
    {
        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public AnimalList()
        {
            Add(new Chat("GrosMinet", TimeSpan.FromHours(3)));
            Add(new Oiseau("Titi", TimeSpan.FromHours(1)));
            Add(new Chien("Idefix", 50));
        }
    }
}
