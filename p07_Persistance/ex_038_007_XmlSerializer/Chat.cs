﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Chat.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;

namespace ex_038_007_XmlSerializer
{
    public class Chat : Animal
    {
        public TimeSpan DuréeSieste
        {
            get;
            set;
        }

        public Chat(string name, TimeSpan duréeSieste) : base(name, "Miaou")
        {
            DuréeSieste = duréeSieste;
        }

        /// <summary>
        /// pour utiliser XmlSerializer, les types doivent avoir un constructeur par défaut
        /// </summary>
        public Chat()
        {

        }
    }
}
