﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using static System.Console;

namespace ex_038_007_XmlSerializer
{
    class Program
    {
        /// <summary>
        /// le XmlSerializer peut sérialiser et désérialiser en XML. 
        /// Voici quelques exemples :
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            List<Animal> animaux = new List<Animal>
            {
                new Chat("GrosMinet", TimeSpan.FromHours(3)),
                new Oiseau("Titi", TimeSpan.FromHours(1)),
                new Chien("Idefix", 50)
            };

            // ===1===
            //on sérialise ici une collection d'Animal. Il suffit juste de donner le type de l'élément à sérialiser au sérialiseur, i.e. ici List<Animal>
            //Comme Animal connait les types Chat et Chien (grâce à XmlInclude), pas besoin de les donner au sérialiseur.
            //Puisque Animal ne connait pas le type Oiseau, il faut préciser au sérialiseur qu'il peut tomber dessus.
            XmlSerializer serializerListAnimal = new XmlSerializer(typeof(List<Animal>), new Type[] { typeof(Oiseau) });

            //sérialisation
            using (Stream stream = File.Create("List_Animal.xml"))
            {
                serializerListAnimal.Serialize(stream, animaux);
            }

            //désérialisation
            List<Animal> deserialized;
            using (Stream stream = File.OpenRead("List_Animal.xml"))
            {
                deserialized = serializerListAnimal.Deserialize(stream) as List<Animal>;
            }

            foreach (var a in deserialized)
            {
                WriteLine(a);
            }


            // ===2===
            //Ici, on va sérialiser une classe Ménagerie qui contient deux collections : une de Chat et une de Animal
            //Notez encore une fois qu'on précise au sérialiseur qu'il peut tomber sur des Oiseau
            //Lisez les commentaires dans Ménagerie pour plus d'explications.
            Ménagerie ménagerie = new Ménagerie { Nom = "mon Zoo" };

            XmlSerializer serializerMénagerie = new XmlSerializer(typeof(Ménagerie), new Type[] { typeof(Oiseau) });

            //sérialisation
            using (Stream stream = File.Create("Ménagerie.xml"))
            {
                serializerMénagerie.Serialize(stream, ménagerie);
            }

            //désérialisation
            Ménagerie deserialized2;
            using (Stream stream = File.OpenRead("Ménagerie.xml"))
            {
                deserialized2 = serializerMénagerie.Deserialize(stream) as Ménagerie;
            }

            WriteLine(deserialized2);


            // ===3===
            //Enfin, AnimalList est une classe qui dérive d'une collection.
            //Elle utilise un attribut légèrement différent.
            //Lisez les commentaires de AnimalList pour le découvrir.
            AnimalList animalList = new AnimalList();

            XmlSerializer serializerAnimalList = new XmlSerializer(typeof(AnimalList), new Type[] { typeof(Oiseau) });

            //sérialisation
            using (Stream stream = File.Create("AnimalList.xml"))
            {
                serializerAnimalList.Serialize(stream, animalList);
            }

            //désérialisation
            Ménagerie deserialized3;
            using (Stream stream = File.OpenRead("AnimalList.xml"))
            {
                deserialized3 = serializerAnimalList.Deserialize(stream) as Ménagerie;
            }

            foreach (var a in deserialized)
            {
                WriteLine(a);
            }

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
