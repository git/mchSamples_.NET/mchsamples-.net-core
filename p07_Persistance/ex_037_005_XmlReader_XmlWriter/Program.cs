﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System;
using System.IO;
using System.Text;
using System.Xml;
using static System.Console;

namespace ex_037_005_XmlReader_XmlWriter
{
    class Program
    {
        // XmlReader et XmlWriter lisent et écrivent des fragments XML
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.UTF8;
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//..//ex_037_005_XmlReader_XmlWriter", "XML"));

            Nounours nounours = new Nounours()
            {
                Nom = "Chewbacca",
                DateDeNaissance = new DateTime(1977, 5, 27),
                NbPoils = 1234567
            };
            WriteLine(nounours);

            SauvegarderNounours(nounours);
            Nounours nounours2 = ChargerNounours();
            WriteLine(nounours2);
        }

        static void SauvegarderNounours(Nounours nounours)
        {
            string xmlFile = "nounours.xml";

            //settings pour que le fichier écrit soit indenté
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            //création du writer
            using (TextWriter tw = File.CreateText(xmlFile))
            {
                using (XmlWriter mWriter = XmlWriter.Create(tw, settings))
                {

                    //écrit l'élément racine "nounours"
                    mWriter.WriteStartElement("nounours");

                    //écrit l'attribut @nom
                    mWriter.WriteAttributeString("nom", nounours.Nom);

                    //écrit le sous-élément naissance
                    mWriter.WriteStartElement("naissance");
                    mWriter.WriteValue(nounours.DateDeNaissance);
                    mWriter.WriteEndElement();

                    //écrit le sous-élément nb_poils
                    mWriter.WriteStartElement("nb_poils");
                    mWriter.WriteValue(nounours.NbPoils);
                    mWriter.WriteEndElement();

                    //ferme la balise nounours
                    mWriter.WriteEndElement();
                }
            }
        }

        static Nounours ChargerNounours()
        {
            string xmlFile = "nounours.xml";

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            settings.IgnoreProcessingInstructions = true;

            XmlReader reader = XmlReader.Create(xmlFile, settings);

            Nounours nounours = new Nounours();

            while (reader.NodeType != XmlNodeType.Element)
            {
                reader.Read();
            }
            nounours.Nom = reader.GetAttribute("nom");
            reader.ReadStartElement("nounours");
            nounours.DateDeNaissance = XmlConvert.ToDateTime(reader.ReadElementContentAsString("naissance", ""), XmlDateTimeSerializationMode.Utc);
            nounours.NbPoils = reader.ReadElementContentAsInt("nb_poils", "");
            reader.ReadEndElement();

            return nounours;
        }
    }
}
