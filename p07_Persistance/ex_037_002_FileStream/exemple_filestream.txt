﻿Attiré avant tout par le free jazz et le post-bop, Bobby Hutcherson a fait plusieurs enregistrements dans ces styles pour le label Blue Note avec Jackie McLean, Eric Dolphy, Andrew Hill, Grachan Moncur III, Joe Chambers, et Freddie Hubbard, comme leader ou comme sideman.
Malgré le grand nombre d'enregistrements d'avant-garde effectués durant cette période, la première session d'enregistrement de Hutcherson pour Blue Note, The Kicker (1963), laisse entrevoir son expérience du hard bop et du blues.
Beaucoup de ses enregistrements récents reviennent à ce hard bop et à un son moins aventureux.
En 1966, la session Stick-Up!, enregistrée chez Blue Note et faisant participer le saxophoniste Joe Henderson, est la première session que Bobby Hutcherson effectue avec le pianiste McCoy Tyner.
Leur collaboration se prolongera pendant plus de quatre décennies.
Little B's Poem (de son album Components) est l'une de ses compositions les plus connues.
En 2007, son quartette inclut Renee Rosnes au piano, Dwayne Bruno à la basse et Al Foster à la batterie.
Il meurt le 15 août 2016 après une longue lutte contre l'emphysème à l'âge de 75 ans.