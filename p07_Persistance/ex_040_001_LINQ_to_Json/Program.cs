﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-13
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Console;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ex_040_001_LINQ_to_Json
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//Json"));
            LireVolcansInXMLFile();

            WriteLine("VOLCANS LUS :");
            foreach (var volcan in listeVolcans)
            {
                WriteLine(volcan);
            }
            WriteLine("".PadRight(WindowWidth - 1, '*'));

            var pariou = new Volcan()
            {
                Nom = "Puy de Pariou",
                Altitude = 1209,
                Latitude = 45.795462f,
                Longitude = 2.970257f,
                Pays = "France",
            };
            pariou.AjouterRoches("Trachy-basalte", "trachy-andésite", "trachyte");
            listeVolcans.Add(pariou);

            WriteLine("VOLCANS APRES AJOUT :");
            foreach (var volcan in listeVolcans)
            {
                WriteLine(volcan);
            }
            WriteLine("".PadRight(WindowWidth - 1, '*'));

            EcrireVolcansInJsonFile();

            WriteLine("\n\nLe dossier Json a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }

        static List<Volcan> listeVolcans = new List<Volcan>();

        static void LireVolcansInXMLFile()
        {
            string jsonText = File.ReadAllText("volcans.json");
            JObject json = JObject.Parse(jsonText);

            listeVolcans = json["volcans"].Select(j => new Volcan()
            {
                Pays = (string)j["volcan"]["pays"],
                Nom = (string)j["volcan"]["nom"],
                Altitude = (float)j["volcan"]["altitude"],
                Latitude = (float)j["volcan"]["coordonnées"]["latitude"],
                Longitude = (float)j["volcan"]["coordonnées"]["longitude"]
            }).ToList();

            foreach (var volcan in listeVolcans)
            {
                var rochesElt = (string)json["volcans"]
                                             .Single(elt => ((string)elt["volcan"]["nom"]).Equals(volcan.Nom))
                                             ["volcan"]["roches"];
                if (rochesElt == null)
                {
                    continue;
                }
                volcan.AjouterRoches(rochesElt.Split());
            }
        }

        static void EcrireVolcansInJsonFile()
        {
            var volcansElts = listeVolcans.Select(volcan => new JObject(
                                                                    new JProperty("volcan",
                                                                        new JObject(
                                                                            new JProperty("pays", volcan.Pays),
                                                                            new JProperty("nom", volcan.Nom),
                                                                            new JProperty("altitude", JsonConvert.ToString(volcan.Altitude)),
                                                                            new JProperty("coordonnées",
                                                                                new JObject(
                                                                                    new JProperty("latitude", JsonConvert.ToString(volcan.Latitude)),
                                                                                    new JProperty("longitude", JsonConvert.ToString(volcan.Longitude)))),
                                                                            new JProperty("roches", volcan.Roches.Count() > 0 ? volcan.Roches.Aggregate((stringRoche, nextRoche) => $"{stringRoche} {nextRoche}") : "")))));

            var volcansFichier = new JObject(new JProperty("volcans", volcansElts));

            File.WriteAllText("volcans2.json", volcansFichier.ToString());
        }
    }
}
