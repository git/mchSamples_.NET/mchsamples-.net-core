﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Volcan.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-13
//
// ========================================================================

using System.Collections.Generic;
using System.Text;

namespace ex_040_001_LINQ_to_Json
{
    /// <summary>
    /// classe de stockage pour les données d'un volcan
    /// </summary>
    public class Volcan
    {
        /// <summary>
        /// nom du volcan
        /// </summary>
        public string Nom
        {
            get;
            set;
        }

        /// <summary>
        /// pays où se trouve ce volcan
        /// </summary>
        public string Pays
        {
            get;
            set;
        }

        /// <summary>
        /// altitude de ce volcan
        /// </summary>
        public float Altitude
        {
            get;
            set;
        }

        /// <summary>
        /// latitude de ce volcan
        /// </summary>
        public float Latitude
        {
            get;
            set;
        }

        /// <summary>
        /// longitude de ce volcan
        /// </summary>
        public float Longitude
        {
            get;
            set;
        }

        /// <summary>
        /// differentes roches de ce volcan
        /// </summary>
        public IEnumerable<string> Roches
        {
            get
            {
                return mRoches;
            }
        }
        /// <seealso cref="Roches"/>
        private List<string> mRoches = new List<string>();

        /// <summary>
        /// ajoute une roche à la collection de roches de ce volcan
        /// </summary>
        /// <param name="rock"></param>
        public void AjouterRoches(params string[] roches)
        {
            mRoches.AddRange(roches);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat($"Nom : {Nom}\n");
            sb.AppendFormat($"Altitude : {Altitude} mètres\n");

            sb.AppendLine();
            sb.AppendFormat($"Coordonnées : {Latitude} ; {Longitude}\n");

            if (mRoches.Count > 0)
            {
                sb.AppendLine("Roches :");
                mRoches.ForEach(rock => sb.AppendFormat($"\t{rock}\n"));
            }

            return sb.ToString();
        }
    }
}
