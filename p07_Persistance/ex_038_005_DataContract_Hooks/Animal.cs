﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Animal.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    /// <summary>
    /// Dans cette classe, nous n'utilisons pas de Deserialization Hook.
    /// Lors de la désérialisation, l'initialiseur de isAlive et le constructeur de Animal ne sont pas appelés.
    /// isAlive prend donc la valeur false par défaut, alors qu'il aurait dû prendre la valeur true imposée pour une construction.
    /// (Note : initialiser isAlive dans l'initialiseur ET dans le constructeur n'a pas de sens, et a juste ici pour objectif de vous montrer que les deux sont by-passés) 
    /// </summary>
    [DataContract]
    class Animal
    {
        [DataMember]
        public string Nom
        {
            get;
            set;
        }

        [DataMember]
        public string Bruit
        {
            get;
            set;
        }

        private bool isAlive = true;

        public Animal(string nom, string bruit)
        {
            isAlive = true;
            Nom = nom;
            Bruit = bruit;
        }

        public override string ToString()
        {
            return $"{Nom} ({Bruit}) isAlive: {isAlive}";
        }
    }
}
