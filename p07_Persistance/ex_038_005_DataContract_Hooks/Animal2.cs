﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Animal2.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    /// <summary>
    /// Dans cette classe, nous utilisons un Deserialization Hook.
    /// Lors de la désérialisation, l'initialiseur de isAlive et le constructeur de Animal ne sont pas appelés.
    /// Mais la méthode décorée par [OnDeserializing] est, elle, appelée juste avant la désérialisation.
    /// (Cette méthode prend un StreamingContext en paramètre, mais il ne sert à rien. Il est utile dans le cas de sérialisation/désérialisation binaire et 
    /// a été gardé ici pour des raisons de symétrie.)
    /// isAlive prend donc la valeur false par défaut, mais lors de l'appel de la méthode juste avant la désérialisation, il est setté à true.
    /// 
    /// Les autres décorateurs Serialization et Deserialization Hook sont :
    /// [OnSerializing] pour indiquer que la méthode doit être appelée juste avant la sérialisation
    /// [OnSerialized] pour indiquer que la méthode doit être appelée juste après la sérialisation
    /// [OnDeserialized] pour indiquer que la méthode doit être appelée juste après la désérialisation.
    /// 
    /// (Note : initialiser isAlive dans l'initialiseur ET dans le constructeur n'a pas de sens, et a juste ici pour objectif de vous montrer que les deux sont by-passés) 
    /// </summary>
    [DataContract]
    class Animal2
    {
        [DataMember]
        public string Nom
        {
            get;
            set;
        }

        [DataMember]
        public string Bruit
        {
            get;
            set;
        }

        private bool isAlive = true;

        public Animal2(string nom, string bruit)
        {
            Init();
            Nom = nom;
            Bruit = bruit;
        }

        [OnDeserializing]
        void Init(StreamingContext sc = new StreamingContext())
        {
            isAlive = true;
        }
        public override string ToString()
        {
            return $"{Nom} ({Bruit}) isAlive: {isAlive}";
        }
    }
}
