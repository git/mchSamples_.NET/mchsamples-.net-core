﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
// Mise à jour   : 2019-12-08
//
// ========================================================================

using ex_038_004_DataContract_Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using static System.Console;

namespace ex_038_005_DataContract_Hooks
{
    class Program
    {
        /// <summary>
        /// Parfois, certaines méthodes doivent être exécutées juste avant ou juste après la sérialisation ou la désérialisation.
        /// On les appelle les Serialization and Deserialization Hooks.
        /// Cet exemple montre un cas d'utilisation d'un hook juste avant désérialisation. La classe Animal ne l'utilise pas alors que la classe Animal2 l'utilise.
        /// Toutes les deux possèdent un membre privé isAlive de type bool initialisé à true (aussi bien dans l'initialiseur que dans le constructeur).
        /// Or, la désérialisation by-pass les initialiseurs de membres et les constructeurs. En conséquence, sans l'utilisation d'un hook, ce membre reste à false
        /// lors de la désérialisation.
        /// Explorez les classes Animal et Animal2 pour mieux comprendre.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            Animal jojo = new Animal("jojo", "pfffff");
            Animal2 jojo2 = new Animal2("jojo2", "pfffff");

            var serializer = new DataContractSerializer(typeof(Animal));

            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText("animal.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                serializer.WriteObject(writer, jojo);
            }

            Animal jojoDésérialisé;
            using (Stream s = File.OpenRead("animal.xml"))
            {
                jojoDésérialisé = serializer.ReadObject(s) as Animal;
            }

            WriteLine(jojoDésérialisé);


            var serializer2 = new DataContractSerializer(typeof(Animal2));

            using (TextWriter tw = File.CreateText("animal2.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                serializer2.WriteObject(writer, jojo2);
            }

            Animal2 jojoDésérialisé2;
            using (Stream s = File.OpenRead("animal2.xml"))
            {
                jojoDésérialisé2 = serializer2.ReadObject(s) as Animal2;
            }

            WriteLine(jojoDésérialisé2);

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
