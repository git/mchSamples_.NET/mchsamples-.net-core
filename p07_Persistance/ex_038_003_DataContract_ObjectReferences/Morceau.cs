﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Morceau.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-06
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_003_DataContract_ObjectReferences
{
    /// <summary>
    /// Morceau est sérialisable en utilisant DataContractSerializer.
    /// Il possède une référence vers un Artiste (le compositeur) et un Album (qui contient ce morceau).
    /// </summary>
    [DataContract]
    class Morceau
    {
        [DataMember(Order = 1)]
        public string Titre
        {
            get; set;
        }

        [DataMember(Order = 2)]
        public Artiste Compositeur
        {
            get; set;
        }

        [DataMember(Order = 3)]
        public Album Album
        {
            get; set;
        }
    }
}
