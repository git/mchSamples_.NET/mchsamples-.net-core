﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Artiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-06
//
// ========================================================================

using System;
using System.Runtime.Serialization;

namespace ex_038_003_DataContract_ObjectReferences
{
    /// <summary>
    /// Artiste est sérialisable en utilisant DataContractSerializer.
    /// </summary>
    [DataContract]
    class Artiste
    {
        [DataMember(Order = 1)]
        public string Prénom
        {
            get; set;
        }

        [DataMember(Order = 2)]
        public string Nom
        {
            get; set;
        }

        [DataMember(Order = 3)]
        public DateTime Naissance
        {
            get; set;
        }

        [DataMember(Order = 4)]
        public string Instrument
        {
            get; set;
        }
    }
}
