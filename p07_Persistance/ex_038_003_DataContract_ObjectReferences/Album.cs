﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Album.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-06
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_003_DataContract_ObjectReferences
{
    /// <summary>
    /// Album est sérialisable en utilisant DataContractSerializer
    /// Il possède deux références vers des artistes (pas de collection pour le moment, ce sera traité dans l'exemple suivant).
    /// </summary>
    [DataContract]
    class Album
    {
        [DataMember(Order = 1)]
        public string Titre
        {
            get; set;
        }

        [DataMember(Order = 3)]
        public Artiste Artiste1
        {
            get; set;
        }

        [DataMember(Order = 4)]
        public Artiste Artiste2
        {
            get; set;
        }

        [DataMember(Order = 2)]
        public int AnnéeDeSortie
        {
            get; set;
        }
    }
}
