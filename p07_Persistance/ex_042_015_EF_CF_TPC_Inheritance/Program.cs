﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using static System.Console;

namespace ex_042_015_EF_CF_TPC_Inheritance
{
    /// <summary>
    /// L'approche CodeFirst d'Entity Framework vous permet de gérer l'héritage entre types de trois façons différentes :
    /// - héritage TPT (Table per Type) : une table par type (dont le type père) [PAS ENCORE IMPLEMENTE DANS ENTITY FRAMEWORK CORE]
    /// - héritage TPH (Table per Class Hierarchy) : une seule table correspondant au type père mais avec toutes les propriétés (nullables) des types fils
    /// - héritage TPC (Table per Concrete Class) : une table par type concret [PAS ENCORE IMPLEMENTE DANS ENTITY FRAMEWORK CORE]
    /// 
    /// Cet exemple montre comment mettre en oeuvre un héritage de type TPC.
    /// Ici, il n'y aura une table par type concret, et aucune pour les types abstraits. L'inconvénient est la duplication des colonnes dans les types concrets.
    /// C'est solution ne marche que lorsque le type père est abstrait.
    /// 
    /// Pour la mettre en oeuvre, il faut :
    /// - que la classe mère soit abstraite (mais elle peut quand même contenir des annotations de données), sinon, une table sera créée pour elle, même si elle est vide
    /// - dans la classe qui dérive de DbContext, utiiser des "Map" pour expliquer qu'il faut hériter les propriétés de la classe mère et mapper chaque entité à une table
    /// (- note importante : avec l'héritage TPC, vous ne pouvez pas utiliser d'identifiant entier généré automatiquement. Il faut utiliser un identifiant de type Guid, sinon, il va générer plusieurs entités avec le même id.)
    /// 
    /// Notez bien que la classe qui dérive de DbContext ne contient qu'un DbSet de Forme.
    /// 
    /// Après avoir exécuté l'exemple, vous pouvez vous rendre dans l'explorateur d'objets SQL Server, et constater qu'Entity Framework a créé une table par type concret, et aucune table pour la classe mère.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("TPC Inheritance is not fully implemented yet in Entity Framework Core.");
        }
    }
}
