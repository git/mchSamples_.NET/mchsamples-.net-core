﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Chien.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    [DataContract]
    class Chien : Animal
    {
        [DataMember]
        public int NbOsRongés
        {
            get;
            set;
        }

        public Chien(string name, int nbOsRongés) : base(name, "Wouf Wouf")
        {
            NbOsRongés = nbOsRongés;
        }
    }
}
