﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using static System.Console;

namespace ex_038_004_DataContract_Collections
{
    class Program
    {
        /// <summary>
        /// le DataContractSerializer peut sérialiser et désérialiser n'importe quel type de collections. 
        /// Voici quelques exemples :
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            List<Animal> animaux = new List<Animal>
            {
                new Chat("GrosMinet", TimeSpan.FromHours(3)),
                new Oiseau("Titi", TimeSpan.FromHours(1)),
                new Chien("Idefix", 50)
            };

            // ===1===
            //on sérialise ici une collection d'Animal. Il suffit juste de donner le type de l'élément à sérialiser au sérialiseur, i.e. ici List<Animal>
            //Comme Animal connait les types Chat et Chien, pas besoin de les donner au sérialiseur.
            //Puisque Animal ne connait pas le type Oiseau, il faut préciser au sérialiseur qu'il peut tomber dessus.
            var serializerListAnimal = new DataContractSerializer(typeof(List<Animal>), new Type[] { typeof(Oiseau) });

            //sérialisation
            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText("List_Animal.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                serializerListAnimal.WriteObject(writer, animaux);
            }


            // ===2===
            //Ici, on va sérialiser une classe Ménagerie qui contient deux collections : une de Chat et une de Animal
            //Notez encore une fois qu'on précise au sérialiseur qu'il peut tomber sur des Oiseau
            //Lisez les commentaires dans Ménagerie pour plus d'explications.
            Ménagerie ménagerie = new Ménagerie { Nom = "mon Zoo" };

            var serializerMénagerie = new DataContractSerializer(typeof(Ménagerie), new Type[] { typeof(Oiseau) });

            //sérialisation
            using (TextWriter tw = File.CreateText("Ménagerie.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                serializerMénagerie.WriteObject(writer, ménagerie);
            }


            // ===3===
            //Enfin, AnimalList est une classe qui dérive d'une collection.
            //Elle utilise un attribut légèrement différent.
            //Lisez les commentaires de AnimalList pour le découvrir.
            AnimalList animalList = new AnimalList();

            var serializerAnimalList = new DataContractSerializer(typeof(AnimalList), new Type[] { typeof(Oiseau) });

            //sérialisation
            using (TextWriter tw = File.CreateText("AnimalList.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                serializerAnimalList.WriteObject(writer, animalList);
            }

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
