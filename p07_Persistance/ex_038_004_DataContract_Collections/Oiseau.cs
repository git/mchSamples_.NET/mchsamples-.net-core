﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Oiseau.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System;
using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    [DataContract]
    class Oiseau : Animal
    {
        [DataMember]
        public TimeSpan TempsDeVol
        {
            get; set;
        }

        public Oiseau(string nom, TimeSpan tempsDeVol) : base(nom, "Cui Cui")
        {
            TempsDeVol = tempsDeVol;
        }
    }
}
