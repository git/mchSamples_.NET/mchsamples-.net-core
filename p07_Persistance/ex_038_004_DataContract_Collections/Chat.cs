﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Chat.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System;
using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    [DataContract]
    class Chat : Animal
    {
        [DataMember]
        public TimeSpan DuréeSieste
        {
            get;
            set;
        }

        public Chat(string name, TimeSpan duréeSieste) : base(name, "Miaou")
        {
            DuréeSieste = duréeSieste;
        }
    }
}
