﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AnimalList.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    /// <summary>
    /// AnimalList est une classe qui dérive d'une collection.
    /// On n'utilise pas ici un attribut [DataContract] mais [CollectionDataContract]
    /// Celui-ci nous permet de plus d'indiquer un nom de balise différent pour les items de la collection (ici unAnimal).
    /// </summary>
    [CollectionDataContract(ItemName = "unAnimal")]
    class AnimalList : List<Animal>
    {
        public AnimalList()
        {
            Add(new Chat("GrosMinet", TimeSpan.FromHours(3)));
            Add(new Oiseau("Titi", TimeSpan.FromHours(1)));
            Add(new Chien("Idefix", 50));
        }
    }
}
