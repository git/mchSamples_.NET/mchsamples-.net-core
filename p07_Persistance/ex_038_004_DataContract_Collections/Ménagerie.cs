﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Ménagerie.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ex_038_004_DataContract_Collections
{
    /// <summary>
    /// Ménagerie contient deux collections : une de Chat et une de Animal.
    /// Ces collections sont privées (List<Chat> et List<Animal>)
    /// et encapsulées en lecture seule par des propriétés de type IEnumerable générique.
    /// Notez que le [DataMember] est placé devant le membre et pas devant la propriété pour une fois. Pourquoi ?
    /// Lors de la désérialisation, le désérialiseur en voyant un type abstrait, ne saura pas quel type utiliser.
    /// Par conséquent, il va créer des tableaux (Chat[] et Animal[]) ce qui ne pourra pas convenir pour notre classe.
    /// Dès lors, on préfère placer dans ce cas le [DataMember] devant les membres privés.
    /// 
    /// Notez également qu'une collection d'un type père se sérialise parfaitement en sérialisant les items de types fils.
    /// </summary>
    [DataContract]
    class Ménagerie
    {
        [DataMember(Order = 0)]
        public string Nom { get; set; }

        public IEnumerable<Chat> Chats => chats;

        [DataMember(Name = "Chats", Order = 1)]
        private List<Chat> chats = new List<Chat> {
            new Chat("Isidore", TimeSpan.FromHours(4)),
            new Chat("Garfield", TimeSpan.FromHours(12)),
            new Chat("GrosMinet", TimeSpan.FromHours(3))};

        public IEnumerable<Animal> Animaux => animaux;

        [DataMember(Name = "Animaux", Order = 2)]
        private List<Animal> animaux = new List<Animal>
        {
            new Chat("GrosMinet", TimeSpan.FromHours(3)),
            new Oiseau("Titi", TimeSpan.FromHours(1)),
            new Chien("Idefix", 50)
        };
    }
}
