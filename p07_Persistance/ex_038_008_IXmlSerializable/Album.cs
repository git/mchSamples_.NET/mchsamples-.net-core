﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Album.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ex_038_008_IXmlSerializable
{
    /// <summary>
    /// Album est sérialisable en utilisant DataContractSerializer
    /// Il possède deux références vers des artistes (pas de collection pour le moment, ce sera traité dans l'exemple suivant).
    /// </summary>
    class Album : IXmlSerializable
    {
        public string Titre
        {
            get; set;
        }

        public Artiste Artiste
        {
            get; set;
        }

        public int AnnéeDeSortie
        {
            get; set;
        }

        public Album()
        {
        }

        public Album(XmlReader reader)
        {
            ReadXml(reader);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            Titre = reader["titre"];
            reader.ReadStartElement("album");
            AnnéeDeSortie = reader.ReadElementContentAsInt("annéeDeSortie", "");
            Artiste = new Artiste(reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("titre", Titre);
            writer.WriteElementString("annéeDeSortie", AnnéeDeSortie.ToString());
            writer.WriteStartElement("artiste");
            Artiste.WriteXml(writer);
            writer.WriteEndElement();
        }

        public override string ToString()
        {
            return $"Album: Titre: {Titre}, Year: {AnnéeDeSortie}, Artist: {Artiste}";
        }
    }
}
