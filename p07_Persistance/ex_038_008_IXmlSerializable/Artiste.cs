﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Artiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
//
// ========================================================================

using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ex_038_008_IXmlSerializable
{
    /// <summary>
    /// Artiste est sérialisable en utilisant DataContractSerializer.
    /// </summary>
    class Artiste : IXmlSerializable
    {
        public string Prénom
        {
            get; set;
        }

        public string Nom
        {
            get; set;
        }

        public DateTime Naissance
        {
            get; set;
        }

        public string Instrument
        {
            get; set;
        }

        public Artiste()
        { }

        public Artiste(XmlReader reader)
        {
            ReadXml(reader);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            Nom = reader["nom"];
            Prénom = reader["prénom"];
            Naissance = XmlConvert.ToDateTime(reader["naissance"], XmlDateTimeSerializationMode.Utc);
            reader.ReadStartElement();
            Instrument = reader.ReadElementContentAsString("instrument", "");
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("nom", Nom);
            writer.WriteAttributeString("prénom", Prénom);
            writer.WriteAttributeString("naissance", XmlConvert.ToString(Naissance, XmlDateTimeSerializationMode.Utc));
            writer.WriteElementString("instrument", Instrument);
        }

        public override string ToString()
        {
            return $"{Prénom} {Nom} ({Instrument}, {Naissance.ToString("d")})";
        }
    }
}
