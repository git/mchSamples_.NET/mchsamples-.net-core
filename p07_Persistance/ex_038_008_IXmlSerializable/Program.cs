﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Artiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-12
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.IO;
using System.Xml;
using static System.Console;

namespace ex_038_008_IXmlSerializable
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            Artiste bojan = new Artiste { Prénom = "Bojan", Nom = "Z", Naissance = new DateTime(1968, 2, 2), Instrument = "Piano" };
            Album duo = new Album { Titre = "Duo", AnnéeDeSortie = 2015, Artiste = bojan };

            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Indent = true;

            using (TextWriter tw = File.CreateText("album.xml"))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, writerSettings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("album", "");
                    duo.WriteXml(writer);
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            settings.IgnoreProcessingInstructions = true;

            using (XmlReader reader = XmlReader.Create("album.xml", settings))
            {
                while (reader.NodeType != XmlNodeType.Element) reader.Read();
                Album a = new Album(reader);
                WriteLine(a);
            }

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
