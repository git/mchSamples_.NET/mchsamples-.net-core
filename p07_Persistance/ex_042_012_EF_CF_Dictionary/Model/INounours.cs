﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : INounours.cs
// Author        : Marc Chevaldonné
// Last Modified : 2019-12-25
//
// ========================================================================

using System;

namespace ex_042_012_EF_CF_Dictionary
{
    public interface INounours
    {
        Guid UniqueId
        {
            get; set;
        }

        string Nom
        {
            get;
            set;
        }

        DateTime DateDeNaissance
        {
            get;
            set;
        }

        int NbPoils
        {
            get;
            set;
        }
    }
}
