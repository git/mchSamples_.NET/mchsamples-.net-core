﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : INounoursEqualityComparer.cs
// Author        : Marc Chevaldonné
// Last Modified : 2019-12-25
//
// ========================================================================

using System.Collections.Generic;

namespace ex_042_012_EF_CF_Dictionary.Model
{
    class INounoursEqualityComparer : EqualityComparer<INounours>
    {
        public override bool Equals(INounours x, INounours y)
        {
            return x.Nom == y.Nom && x.DateDeNaissance == y.DateDeNaissance;
        }

        public override int GetHashCode(INounours obj)
        {
            return obj.GetHashCode();
        }
    }
}
