﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ILit.cs
// Author        : Marc Chevaldonné
// Last Modified : 2019-12-25
//
// ========================================================================

using System;
using System.Collections.ObjectModel;

namespace ex_042_012_EF_CF_Dictionary
{
    public interface ILit
    {
        Guid UniqueId
        {
            get; set;
        }

        string Propriétaire
        {
            get; set;
        }

        ReadOnlyDictionary<INounours, int> Scores
        {
            get;
        }

        int this[INounours nounours]
        {
            get;
            set;
        }
    }
}
