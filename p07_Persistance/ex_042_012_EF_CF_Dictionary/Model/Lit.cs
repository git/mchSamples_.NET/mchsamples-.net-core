﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Lit.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-01
// Last Modified : 2019-12-25
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ex_042_012_EF_CF_Dictionary
{
    public class Lit : ILit
    {
        public Guid UniqueId
        {
            get; set;
        }

        public string Propriétaire
        {
            get; set;
        }

        public Lit()
        {
            Scores = new ReadOnlyDictionary<INounours, int>(mScores);
        }

        /// <summary>
        /// dictionnaire de Nounours (un score entier est associé à chaque nounours)
        /// </summary>
        private Dictionary<INounours, int> mScores { get; set; } = new Dictionary<INounours, int>();

        /// <summary>
        /// permet d'accéder en lecture seule au dictionnaie de scores
        /// </summary>
        public ReadOnlyDictionary<INounours, int> Scores
        {
            get; private set;
        }

        /// <summary>
        /// un indexeur pour pouvoir lire et modifier le score d'un Nounours
        /// </summary>
        /// <param name="nounours"></param>
        /// <returns></returns>
        public int this[INounours nounours]
        {
            get
            {
                return mScores[nounours];
            }
            set
            {
                mScores[nounours] = value;
            }
        }
    }
}
