﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : DictionaryItem.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-01
// Last Modified : 2019-12-25
//
// ========================================================================

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_012_EF_CF_Dictionary
{
    /// <summary>
    /// classe de base permettant de transformer un dictionnaire Dictionary<TKey, TValue> en 
    /// ICollection<Item<TKey, TValue>> permettant la liaison avec Entity Framwork.
    /// Dans la classe Score, TKey est de type Nounours et TValue est de type int.
    /// La propriété Key permet donc de réaliser la relation one to one avec NounoursEx.
    /// La propriété Value permet d'obtenir la valeur associée (ici le score entier).
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class Item<TKey, TValue>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId
        {
            get; set;
        }

        public TKey Key
        {
            get;
            set;
        }

        public TValue Value
        {
            get; set;
        }
    }
}
