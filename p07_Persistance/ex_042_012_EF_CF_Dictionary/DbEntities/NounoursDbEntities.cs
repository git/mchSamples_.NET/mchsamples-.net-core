﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : LitEntity.cs
// Author        : Marc Chevaldonné
// Last Modified : 2019-12-25
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace ex_042_012_EF_CF_Dictionary
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Nounours et CarnetDeSante.
    /// </summary>
    public class NounoursDBEntities : DbContext
    {
        public virtual DbSet<Nounours> NounoursSet { get; set; }
        public virtual DbSet<LitEntity> LitsSet { get; set; }
        public virtual DbSet<Score> ScoresSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_012_EF_CF_Dictionary.Nounours.mdf;Trusted_Connection=True;");
        }

        /// <summary>
        /// méthode appelée lors de la création du modèle. 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //création de la table TableNounours
            modelBuilder.Entity<Nounours>().ToTable("Nounours"); //nom de la table
            modelBuilder.Entity<Nounours>().HasKey(n => n.UniqueId); //définition de la clé primaire
            modelBuilder.Entity<Nounours>().Property(n => n.UniqueId)
                .ValueGeneratedOnAdd() ;
            modelBuilder.Entity<Nounours>().Property(n => n.Nom).IsRequired()
                                                                .HasMaxLength(256); //définition de la colonne Nom
            modelBuilder.Entity<Nounours>().Property(n => n.DateDeNaissance).HasColumnName("Naissance"); //changement du nom de la colonne Naissance
            //note : la colonne NbPoils n'est pas changée : utilisation des conventions EF

            modelBuilder.Entity<Score>().Property(n => n.UniqueId).ValueGeneratedOnAdd();

            //on crée une propriété fantôme (shadow property) pour l'entity Score qu'on utilisera comme clé étrangère (foreign key)
            // En effet, j'ai pris le cas où nous décidons d'utiliser une classe existante pour laquelle aucune clé étrangère n'a été prévue.
            // Il me semblait donc logique qu'on cherche à l'utiliser sans qu'elle soit dans le modèle
            modelBuilder.Entity<Score>().Property<Guid>("ScoreId");

            //on précise qu'il y a une relation entre Lit et Score
            modelBuilder.Entity<LitEntity>().HasMany(l => l.mScores) // on dit que le lit a plusieurs scores
                                        .WithOne(s => s.Lit) // et que chaque score a un lit
                                        .HasForeignKey("ScoreId"); //on utilise la clé étrangère fantôme créée précédemment

            base.OnModelCreating(modelBuilder);
        }

        List<Score> lesScores = new List<Score>();

        /// <summary>
        /// ajoute des lits et des nounours à la base
        /// </summary>
        /// <param name="lesLits"></param>
        /// <param name="lesNounours"></param>
        public void AddAll(IEnumerable<LitEntity> lesLits, IEnumerable<Nounours> lesNounours)
        {
            //met à jour les tables
            NounoursSet.AddRange(lesNounours);
            LitsSet.AddRange(lesLits);
            ScoresSet.AddRange(lesScores);

            SaveChanges();
        }
    }
}
