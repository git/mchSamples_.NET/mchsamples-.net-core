﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Score.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-01
// Last Modified : 2019-12-25
//
// ========================================================================

namespace ex_042_012_EF_CF_Dictionary
{
    /// <summary>
    /// classe permettant de réaliser une relation one to many avec Lit, et one to one avec Nounours
    /// Elle remplace le KeyValuePair<Nounours, int>, type des éléments du dictionaire Scores de Lit.
    /// </summary>
    public class Score : Item<Nounours, int>
    {
        /// <summary>
        /// lien vers le LitEx pour la liaison one to many avec LitEx
        /// </summary>
        public LitEntity Lit
        {
            get; set;
        }
    }
}
