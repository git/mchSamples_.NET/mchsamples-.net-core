﻿// ========================================================================
//
// Copyright (C) 2019-2020 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : DbContextInitializer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-01
// Last Modified : 2019-12-25
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace ex_042_012_EF_CF_Dictionary
{
    /// <summary>
    /// initialiseur de stratégies...
    /// </summary>
    public static class DbContextInitializer
    {
        /// <summary>
        /// remplissage de la base avec des données stubbées.
        /// </summary>
        /// <param name="context">base à remplir</param>
        public static void Seed(NounoursDBEntities context)
        {
            SetInitializer(context, InitializationStrategies.DropCreateDatabaseAlways);

            Nounours[] lesNounours;
            LitEntity[] lesLits;
            List<Score> lesScores = new List<Score>();

            //données stubbées
            //simulation de données en utilisant les types du Model
            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };
            Nounours beluga = new Nounours { Nom = "Beluga", DateDeNaissance = new DateTime(2012, 07, 29), NbPoils = 0 };
            Nounours singe = new Nounours { Nom = "Singe", DateDeNaissance = new DateTime(2009, 08, 09), NbPoils = 1345 };
            Nounours girafe = new Nounours { Nom = "Girafe", DateDeNaissance = new DateTime(2007, 11, 02), NbPoils = 567 };
            lesNounours = new Nounours[] { chewie, yoda, ewok, beluga, singe, girafe };

            LitEntity litDeLuke = new LitEntity() { Propriétaire = "Luke" };
            litDeLuke[chewie] = 100;
            litDeLuke[yoda] = 200;
            litDeLuke[ewok] = 1;

            LitEntity monLit = new LitEntity() { Propriétaire = "Moi" };
            monLit[beluga] = 10;
            monLit[singe] = 100;
            monLit[girafe] = 1000;

            lesLits = new LitEntity[] { litDeLuke, monLit };

            //*****************

            context.AddAll(lesLits, lesNounours);
        }

        /// <summary>
        /// les différentes stratégies de création de la base
        /// </summary>
        public enum InitializationStrategies
        {
            CreateDatabaseIfNotExists,
            DropCreateDatabaseIfModelChanges,
            DropCreateDatabaseAlways
        }
        public static void SetInitializer(DbContext context, InitializationStrategies strategy)
        {
            switch (strategy)
            {
                //par défaut : crée la base seulement si elle n'existe pas
                default:
                case InitializationStrategies.CreateDatabaseIfNotExists:
                    context.Database.EnsureCreated();
                    break;

                //recrée la base même si elle existe déjà
                case InitializationStrategies.DropCreateDatabaseAlways:
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                    break;

                //recrée la base seulement si le modèle change : impossible aujourd'hui en Entity Framework Core...
                case InitializationStrategies.DropCreateDatabaseIfModelChanges:
                    throw new NotImplementedException("Le mode DropCreateDatabaseIfModelChanges ne peut pas encore exister sous Entity Framework Core");
            }
        }
    }
}
