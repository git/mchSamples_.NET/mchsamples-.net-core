﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-13
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System.IO;
using System.Xml;
using System.Xml.Linq;
using static System.Console;

namespace ex_039_001_XDOM_bases
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            XDocument volcansFichier = XDocument.Load("volcans.xml");
            WriteLine(volcansFichier);

            //settings pour que le fichier écrit soit indenté
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (TextWriter tw = File.CreateText("volcansSave.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                volcansFichier.Save(writer);
            }

            XElement testVolcan = XElement.Parse(
                @"<volcan activité='endormi' pays='France' type='gris'>
                    <nom>Puy de Dôme</nom>
                    <endormi_depuis>P6000Y</endormi_depuis>
                    <altitude>1465</altitude>
                    <coordonnées>
                      <latitude>45.7723</latitude>
                      <longitude>2.9658</longitude>
                    </coordonnées>
                  </volcan>");
            WriteLine(testVolcan);

            using (TextWriter tw = File.CreateText("unVolcan.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                volcansFichier.Save(writer);
            }

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
