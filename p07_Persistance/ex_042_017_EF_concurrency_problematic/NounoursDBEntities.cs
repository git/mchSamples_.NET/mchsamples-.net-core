﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : NounoursDBEntities.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-11
//
// ========================================================================

using Microsoft.EntityFrameworkCore;

namespace ex_042_017_EF_concurrency_problematic
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient un DbSet<T> pour permettre de réaliser des opérations CRUD sur le type T, ici Nounours.
    /// </summary>
    class NounoursDBEntities : DbContext
    {
        public virtual DbSet<Nounours> NounoursSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_017_EF_concurrency_problematic.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
