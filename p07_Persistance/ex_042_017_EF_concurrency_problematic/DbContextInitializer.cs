﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : DbContextInitializer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-11
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_017_EF_concurrency_problematic
{
    /// <summary>
    /// initialiseur de stratégies...
    /// </summary>
    static class DbContextInitializer
    {
        /// <summary>
        /// remplissage de la base avec des données stubbées.
        /// </summary>
        /// <param name="context">base à remplir</param>
        public static void Seed(NounoursDBEntities context)
        {
            SetInitializer(context, InitializationStrategies.DropCreateDatabaseAlways);

            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };

            context.NounoursSet.AddRange(new Nounours[] { chewie, yoda, ewok });

            context.SaveChanges();
        }

        /// <summary>
        /// les différentes stratégies de création de la base
        /// </summary>
        public enum InitializationStrategies
        {
            CreateDatabaseIfNotExists,
            DropCreateDatabaseIfModelChanges,
            DropCreateDatabaseAlways
        }
        public static void SetInitializer(DbContext context, InitializationStrategies strategy)
        {
            switch (strategy)
            {
                //par défaut : crée la base seulement si elle n'existe pas
                default:
                case InitializationStrategies.CreateDatabaseIfNotExists:
                    context.Database.EnsureCreated();
                    break;

                //recrée la base même si elle existe déjà
                case InitializationStrategies.DropCreateDatabaseAlways:
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                    break;

                //recrée la base seulement si le modèle change : impossible aujourd'hui en Entity Framework Core...
                case InitializationStrategies.DropCreateDatabaseIfModelChanges:
                    throw new NotImplementedException("Le mode DropCreateDatabaseIfModelChanges ne peut pas encore exister sous Entity Framework Core");
            }
        }
    }
}
