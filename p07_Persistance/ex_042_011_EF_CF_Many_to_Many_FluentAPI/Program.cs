﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Console;
using Microsoft.EntityFrameworkCore;

namespace ex_042_011_EF_CF_Many_to_Many_FluentAPI
{
    public class Program
    {
        /// <summary>
        /// Cet exemple montre comment construire une relation many-many dans la base de données en utilisant les conventions de nommage Entity Framework.
        /// 
        /// Aujourd'hui (21 octobre 2016), il n'est malheureusement toujours pas possible d'utiliser les annotations de données pour réaliser une opération many to many.
        /// Cet exemple propose une solution pour s'en approcher, mais qui nécessite quand même de modifier AlbumArtisteDBEntities (méthode OnModelCreating) et surtout
        /// de créer une classe supplémentaire qui sera mappée à une table d'association.
        /// Je ne conseille donc pas vraiment cette solution qui nécessite la modification des classes du modèle.
        /// 
        /// on utilise les données stubbées de DbContextInitializer
        /// On affiche les Albums et les Artistes.
        /// 
        /// Si vous ouvrez la base de données (via l'explorateur d'objets SQL Server), vous pourrez constater la création d'une table d'association.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                //création du DbContext et injection de la dépendance à MyStubDataInitializationStrategy
                using (AlbumArtisteDBEntities db = new AlbumArtisteDBEntities())
                {
                    //choix de la stratégie et remplissage avec des données stubbées
                    DbContextInitializer.Seed(db);
                }
                using (AlbumArtisteDBEntities db = new AlbumArtisteDBEntities())
                {

                    WriteLine("Albums : ");
                    foreach (var album in db.Albums.Include(a => a.AlbumsArtistes)
                                                   .ThenInclude(aa => aa.Artiste))
                    {
                        WriteLine($"\t{album.UniqueId}: {album.Titre} (sorti le : {album.DateDeSortie.ToString("d")})");
                        foreach (var artiste in album.Artistes)
                        {
                            WriteLine($"\t\t{artiste.Prénom} {artiste.Nom}");
                        }
                    }

                    WriteLine();

                    WriteLine("Artistes :");
                    foreach (var artiste in db.Artistes.Include(a => a.AlbumsArtistes)
                                                       .ThenInclude(aa => aa.Album))
                    {
                        var annéeDeMort = artiste.DateDeMort.HasValue ? $" - {artiste.DateDeMort.Value.Year}" : "";
                        var titresAlbums = artiste.Albums.Aggregate(String.Empty, (albums, album) => albums + $"\"{album.Titre}\" ");
                        WriteLine($"\t{artiste.UniqueId}: {artiste.Prénom} {artiste.Nom} ({artiste.DateDeNaissance.Year}{annéeDeMort}) (albums: {titresAlbums})");
                    }

                }
            }
            catch (NotImplementedException exception)
            {
                WriteLine(exception.Message);
            }
            catch (Exception)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions suivantes (que vous retrouvez en commentaires dans la classe Program) :");
                WriteLine("Pour créer la base, ouvrez un invite de commandes et placez-vous dans le dossier de ce projet, ou bien,");
                WriteLine("- dans Visual Studio ouvrez la Console du Gestionnaire de package (Outils -> Gestionnaire de package NuGet -> Console du Gestionnaire de package),");
                WriteLine("- dans cette Console, vous devriez être dans le dossier de la solution, déplacez-vous dans celui du projet (ici : cd ex_042_010_EF_CF_Many_to_Many)");
                WriteLine("- tapez : dotnet restore (pour restaurer les packages .NET Core)");
                WriteLine("- tapez : dotnet ef migrations add MyFirstMigration");
                WriteLine("    note : vous pourrez détruire le dossier Migrations une fois la base créée");
                WriteLine("- tapez : dotnet ef database update");
                WriteLine("    Ceci génère la base de données en utilisant la migration, et en particulier votre classe DBContext et vos classes POCO.");
                WriteLine("\nDans cet exemple, une base de données SQLServer est créée et en particulier la table Nounours.mdf");
            }

            ReadLine();
        }
    }
}
