﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Artiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-21
//
// ========================================================================


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ex_042_011_EF_CF_Many_to_Many_FluentAPI
{
    /// <summary>
    /// Artiste est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation many-many avec la classe Artiste via la propriété Artistes.
    /// La clé primaire est générée lors de l'insertion en table.
    /// 
    /// Aujourd'hui (21 octobre 2016), il n'est pas encore possible d'utiliser les annotations de données pour les relations many to many.
    /// En conséquence, il faut créer une classe pour la table d'association. 
    /// Dans la classe Artiste, on trouve donc une propriété AlbumsArtistes, collection de AlbumArtiste (correspondant à la table d'association).
    /// On trouve également une propriété calculée Albums qui réalise une projection de la collection précédente pour obtenir les albums, ainsi
    /// qu'une méthode AddAlbum rajoutant une ligne dans la table d'association.
    /// </summary>
    public class Artiste
    {
        public Guid UniqueId
        {
            get; set;
        }

        public string Prénom
        {
            get; set;
        }
        public string Nom
        {
            get; set;
        }

        public DateTime DateDeNaissance
        {
            get; set;
        }

        public DateTime? DateDeMort
        {
            get; set;
        }

        public virtual IEnumerable<Album> Albums
        {
            get
            {
                return AlbumsArtistes.Select(aa => aa.Album);
            }
        }

        public void AddAlbum(Album album)
        {
            AlbumsArtistes.Add(new AlbumArtiste() { Album = album, Artiste = this });
        }

        public virtual ICollection<AlbumArtiste> AlbumsArtistes { get; set; } = new List<AlbumArtiste>();
    }
}
