﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Volcan.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-13
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using static System.Console;

namespace ex_039_002_LINQ_to_XML
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//XML"));

            LireVolcansInXMLFile();
            WriteLine("VOLCANS LUS :");
            foreach (var volcan in listeVolcans)
            {
                WriteLine(volcan);
            }
            WriteLine("".PadRight(WindowWidth - 1, '*'));

            var pariou = new Volcan()
            {
                Nom = "Puy de Pariou",
                Altitude = 1209,
                Latitude = 45.795462f,
                Longitude = 2.970257f,
                Pays = "France",
            };
            pariou.AjouterRoches("Trachy-basalte", "trachy-andésite", "trachyte");
            listeVolcans.Add(pariou);

            WriteLine("VOLCANS APRES AJOUT :");
            foreach (var volcan in listeVolcans)
            {
                WriteLine(volcan);
            }
            WriteLine("".PadRight(WindowWidth - 1, '*'));

            EcrireVolcansInXMLFile();

            WriteLine("\n\nLe dossier XML a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }

        static List<Volcan> listeVolcans = new List<Volcan>();

        static void LireVolcansInXMLFile()
        {
            XDocument volcansFichier = XDocument.Load("volcans.xml");

            listeVolcans = volcansFichier.Descendants("volcan")
                          .Select(eltVolcan => new Volcan()
                          {
                              Pays = eltVolcan.Attribute("pays").Value,
                              Nom = eltVolcan.Element("nom").Value,
                              Altitude = XmlConvert.ToSingle(eltVolcan.Element("altitude").Value),
                              Latitude = XmlConvert.ToSingle(eltVolcan.Element("coordonnées").Element("latitude").Value),
                              Longitude = XmlConvert.ToSingle(eltVolcan.Element("coordonnées").Element("longitude").Value)
                          }).ToList();

            foreach (var volcan in listeVolcans)
            {
                var rochesElt = volcansFichier.Descendants("volcan")
                                             .Single(elt => elt.Element("nom").Value == volcan.Nom)
                                             .Element("roches");
                if (rochesElt == null)
                {
                    continue;
                }
                volcan.AjouterRoches(rochesElt.Value.Split());
            }
        }

        private static void EcrireVolcansInXMLFile()
        {
            XDocument volcansFichier = new XDocument();

            var volcansElts = listeVolcans.Select(volcan => new XElement("volcan",
                                            new XAttribute("pays", volcan.Pays),
                                            new XElement("nom", volcan.Nom),
                                            new XElement("altitude", XmlConvert.ToString(volcan.Altitude)),
                                            new XElement("coordonnées",
                                                new XElement("latitude", XmlConvert.ToString(volcan.Latitude)),
                                                new XElement("longitude", XmlConvert.ToString(volcan.Longitude))),
                                            new XElement("roches", volcan.Roches.Count() > 0 ? volcan.Roches.Aggregate((stringRoche, nextRoche) => $"{stringRoche} {nextRoche}") : "")));

            volcansFichier.Add(new XElement("volcans", volcansElts));

            //settings pour que le fichier écrit soit indenté
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (TextWriter tw = File.CreateText("volcansAjout.xml"))
            using (XmlWriter writer = XmlWriter.Create(tw, settings))
            {
                volcansFichier.Save(writer);
            }
        }
    }
}
