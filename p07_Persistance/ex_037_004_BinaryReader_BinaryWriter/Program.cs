﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System;
using System.IO;
using static System.Console;

namespace ex_037_004_BinaryReader_BinaryWriter
{
    class Program
    {
        // BinaryReader et BinaryWriter lisent et écrivent des types natifs (bool, int, float, double, char, string...)
        static void Main(string[] args)
        {
            Nounours nounours = new Nounours()
            {
                Nom = "Chewbacca",
                DateDeNaissance = new DateTime(1977, 5, 27),
                NbPoils = 1234567
            };
            WriteLine(nounours);

            SauvegarderNounours(nounours);
            Nounours nounours2 = ChargerNounours();
            WriteLine(nounours2);
        }

        static void SauvegarderNounours(Nounours nounours)
        {
            using (FileStream fs = File.Create("nounours.txt"))
            {
                BinaryWriter writer = new BinaryWriter(fs);
                writer.Write(nounours.Nom);
                writer.Write(nounours.DateDeNaissance.ToString());
                writer.Write(nounours.NbPoils);
                writer.Flush();
            }
        }

        static Nounours ChargerNounours()
        {
            Nounours nounours;
            using (FileStream fs = File.OpenRead("nounours.txt"))
            {
                BinaryReader reader = new BinaryReader(fs);
                nounours = new Nounours()
                {
                    Nom = reader.ReadString(),
                    DateDeNaissance = DateTime.Parse(reader.ReadString()),
                    NbPoils = reader.ReadInt32()
                };
            }
            return nounours;
        }
    }
}
