﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : NounoursDBEntities.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-13
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_021_EF_concurrency_customObjects
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient un DbSet<T> pour permettre de réaliser des opérations CRUD sur le type T, ici Nounours.
    /// </summary>
    class NounoursDBEntities : DbContext
    {
        public virtual DbSet<Nounours> NounoursSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_021_EF_concurrency_customObjects.Nounours.mdf;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ////METHODE 2b 
            ////On précise que NbPoils et DateDeNaissance sont utilisés pour les tests de vérification de modifications ou suppressions concurrentes.
            //modelBuilder.Entity<Nounours>().Property<int>(n => n.NbPoils).IsConcurrencyToken();
            //modelBuilder.Entity<Nounours>().Property<DateTime>(n => n.DateDeNaissance).IsConcurrencyToken();
            ////FIN METHODE 2b

            base.OnModelCreating(modelBuilder);
        }
    }
}
