﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-11-07
//
// ========================================================================

using static System.Console;

namespace ex_042_013_EF_CF_TPT_Inheritance
{
    public class Program
    {
        /// <summary>
        /// L'approche CodeFirst d'Entity Framework vous permet de gérer l'héritage entre types de trois façons différentes :
        /// - héritage TPT (Table per Type) : une table par type (dont le type père) [PAS ENCORE IMPLEMENTE DANS ENTITY FRAMEWORK CORE]
        /// - héritage TPH (Table per Class Hierarchy) : une seule table correspondant au type père mais avec toutes les propriétés (nullables) des types fils
        /// - héritage TPC (Table per Concrete Class) : une table par type concret [PAS ENCORE IMPLEMENTE DANS ENTITY FRAMEWORK CORE]
        /// 
        /// Cet exemple montre comment mettre en oeuvre un héritage de type TPT.
        /// Chaque table correspondant à un type fils est reliée à la table correspondant au type père via une relation one to one.
        /// 
        /// Pour la mettre en oeuvre, il suffit de préciser à Entity Framework (via des annotations de données ou la fluent API) comment mapper les types fils aux tables.
        /// Dans l'exemple suivant, les types fils (Rectangle et Cercle) utilisent une annotation de données ([Table("Rectangles")] et [Table("Cercles")]) qui entrainent l'utilisation
        /// d'un héritage Table Per Type. Sans ces annotations (ou les lignes de code en fluent API équivalente cf. exemples précédents), on aurait un héritage de type TPH.
        /// 
        /// Notez bien que la classe qui dérive de DbContext ne contient qu'un DbSet de Forme.
        /// 
        /// Après avoir exécuté l'exemple, vous pouvez vous rendre dans l'explorateur d'objets SQL Server, et constater qu'Entity Framework a créé trois tables (Formes, Rectangles et Cercles),
        /// avec des relations one to one entre Formes et Rectangles et entre Formes et Cercles.
        /// </summary>
        public static void Main(string[] args)
        {
            WriteLine("TPT Inheritance is not fully implemented yet in Entity Framework Core.");
        }
    }
}
