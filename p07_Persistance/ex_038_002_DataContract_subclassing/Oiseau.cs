﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Oiseau.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-05
//
// ========================================================================

using System;
using System.Runtime.Serialization;

namespace ex_038_002_DataContract_subclassing
{
    [DataContract]
    class Oiseau : Animal
    {
        [DataMember]
        public TimeSpan TempsDeVol
        {
            get; set;
        }

        public Oiseau(string nom, TimeSpan tempsDeVol) : base(nom, "Cui Cui")
        {
            TempsDeVol = tempsDeVol;
        }
    }
}
