﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Chat.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-05
//
// ========================================================================

using System;
using System.Runtime.Serialization;

namespace ex_038_002_DataContract_subclassing
{
    [DataContract]
    class Chat : Animal
    {
        [DataMember]
        public TimeSpan DuréeSieste
        {
            get;
            set;
        }

        public Chat(string name, TimeSpan duréeSieste) : base(name, "Miaou")
        {
            DuréeSieste = duréeSieste;
        }
    }
}
