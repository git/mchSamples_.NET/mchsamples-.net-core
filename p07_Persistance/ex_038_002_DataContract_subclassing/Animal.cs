﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Animal.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-05
//
// ========================================================================

using System.Runtime.Serialization;

namespace ex_038_002_DataContract_subclassing
{
    [DataContract, KnownType(typeof(Chat)), KnownType(typeof(Chien))]
    class Animal
    {
        [DataMember]
        public string Nom
        {
            get;
            set;
        }

        [DataMember]
        public string Bruit
        {
            get;
            set;
        }

        public Animal(string nom, string bruit)
        {
            Nom = nom;
            Bruit = bruit;
        }
    }
}
