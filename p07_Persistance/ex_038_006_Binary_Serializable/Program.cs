﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-07
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using static System.Console;

namespace ex_038_006_Binary_Serializable
{
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), "..//..//..//..//ex_038_006_Binary_Serializable", "result"));

            Nounours chewie = new Nounours()
            {
                Nom = "Chewbacca",
                Naissance = new DateTime(1977, 5, 27),
                NbPoils = 1234567
            };

            Nounours yoda = new Nounours()
            {
                Nom = "Yoda",
                Naissance = new DateTime(1980, 5, 21),
                NbPoils = 3
            };

            Nounours ewok = new Nounours()
            { 
                Nom = "Ewok",
                Naissance = new DateTime(1983, 5, 25),
                NbPoils = 3456789
            };

            chewie.Amis.Add(yoda);
            chewie.Amis.Add(ewok);

            WriteLine(chewie);


            //ATTENTION : pour que le Binary Serializer fonctionne, vous devez avoir décoré l'objet à sérialiser !
            //cf. Nounours.cs


            //1. Sérialisation d'un chewie en binaire avec la méthode par défaut
            //On construit le sérialiseur binaire en précisant le formatter (BinaryFormatter ou SoapFormatter)
            //- le formatter binaire est le plus efficace des deux et permet d'obtenir la sortie la plus légère
            //- le formatter Soap permet d'obtenir un formatage dans le style du protocole SOAP. 
            //  Attention, il ne prend pas en charge la sérialisation des types génériques comme List<T> !
            string file = "nounours.bin";
            IFormatter formatter = new BinaryFormatter();
            using (FileStream stream = File.Create(file))
            {
                formatter.Serialize(stream, chewie);
            }

            //- pour désérialiser, on crée le flux (de préférence avec using) en lecture
            //- on appelle Deserialize
            Nounours nounours2;
            using (FileStream stream = File.OpenRead(file))
            {
                nounours2 = formatter.Deserialize(stream) as Nounours;
            }

            WriteLine(nounours2);

            WriteLine("\n\nLe dossier result a été mis à jour par l'exécution de ce programme. Jetez un oeil !");
        }
    }
}
