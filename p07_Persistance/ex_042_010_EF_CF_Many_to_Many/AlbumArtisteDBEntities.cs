﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AlbumArtisteDBEntities.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-21
//
// ========================================================================

using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_010_EF_CF_Many_to_Many
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Album et Artiste.
    /// </summary>
    public class AlbumArtisteDBEntities : DbContext
    {
        public virtual DbSet<Artiste> Artistes { get; set; }
        public virtual DbSet<Album> Albums { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_010_EF_CF_Many_to_Many.mdf;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ajoute deux shadow properties à la classe AlbumArtiste pour qu'elles soient utilisées comme clés étrangères
            modelBuilder.Entity<AlbumArtiste>().Property<Guid>("AlbumId");
            modelBuilder.Entity<AlbumArtiste>().Property<Guid>("ArtisteId");

            //crée une clé primaire à partir des deux propriétés précédentes
            modelBuilder.Entity<AlbumArtiste>().HasKey("AlbumId", "ArtisteId");

            //lie l'entité AlbumArtiste à l'entité Album
            modelBuilder.Entity<AlbumArtiste>()
                .HasOne(aa => aa.Album)
                .WithMany(album => album.AlbumsArtistes)
                .HasForeignKey("AlbumId");

            //lie l'entité AlbumArtiste à l'entité Artiste
            modelBuilder.Entity<AlbumArtiste>()
                .HasOne(aa => aa.Artiste)
                .WithMany(artiste => artiste.AlbumsArtistes)
                .HasForeignKey("ArtisteId");

            base.OnModelCreating(modelBuilder);
        }
    }
}
