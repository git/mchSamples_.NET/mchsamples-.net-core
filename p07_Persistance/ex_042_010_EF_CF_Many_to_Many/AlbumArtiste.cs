﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AlbumArtiste.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-21
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ex_042_010_EF_CF_Many_to_Many
{
    /// <summary>
    /// table permettant de réaliser la table d'association
    /// </summary>
    public class AlbumArtiste
    {
        /// <summary>
        /// un album...
        /// </summary>
        public Album Album { get; set; }
        /// <summary>
        /// ...lié à un artiste
        /// </summary>
        public Artiste Artiste { get; set; }
    }
}
