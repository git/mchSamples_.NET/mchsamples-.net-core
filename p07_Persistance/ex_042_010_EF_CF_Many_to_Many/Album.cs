﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Album.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-21
//
// ========================================================================


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ex_042_010_EF_CF_Many_to_Many
{
    /// <summary>
    /// Album est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation many-many avec la classe Artiste via la propriété Artistes.
    /// La clé primaire est générée lors de l'insertion en table.
    /// 
    /// Aujourd'hui (21 octobre 2016), il n'est pas encore possible d'utiliser les annotations de données pour les relations many to many.
    /// En conséquence, il faut créer une classe pour la table d'association. 
    /// Dans la classe Album, on trouve donc une propriété AlbumsArtistes, collection de AlbumArtiste (correspondant à la table d'association).
    /// On trouve également une propriété calculée Artistes qui réalise une projection de la collection précédente pour obtenir les artistes, ainsi
    /// qu'une méthode AddArtiste rajoutant une ligne dans la table d'association.
    /// </summary>
    [Table("Albums")]
    public class Album
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public DateTime DateDeSortie
        {
            get; set;
        }

        public virtual IEnumerable<Artiste> Artistes
        {
            get
            {
                return AlbumsArtistes.Select(aa => aa.Artiste);
            }
        }

        public void AddArtiste(Artiste artiste)
        {
            AlbumsArtistes.Add(new AlbumArtiste() { Album = this, Artiste = artiste });
        }

        public virtual ICollection<AlbumArtiste> AlbumsArtistes { get; set; } = new List<AlbumArtiste>(); 
    }
}
