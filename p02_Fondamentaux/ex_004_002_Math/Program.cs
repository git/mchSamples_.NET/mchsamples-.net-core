﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using System;
using static System.Console;

namespace ex_004_002_Math
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //constantes de la classe Math :
            WriteLine($"CONSTANTES DE LA CLASSE MATH");
            //epsilon
            WriteLine($"E(e) : {Math.E}");
            //PI
            WriteLine($"Pi : {Math.PI}");

            WriteLine();

            //méthodes pour arrondir
            WriteLine($"METHODES POUR ARRONDIR");
            //arrondir 
            WriteLine($"Arrondir 1,9876 à 2 chiffres après la virgule : {Math.Round(1.9876, 2)}");
            //tronquer
            WriteLine($"Partie entière de 1,9876 : {Math.Truncate(1.9876)}");
            //plus grand entier inférieur
            WriteLine($"Plus grand entier inférieur de 1,9876 : {Math.Floor(1.9876)}");
            //plus petit entier supérieur
            WriteLine($"Plus petit entier supérieur de 1,9876 : {Math.Ceiling(1.9876)}");

            WriteLine();

            //calcul de maximums et minimums
            WriteLine($"maximum entre 6 et 4 : {Math.Max(6, 4)}");
            WriteLine($"minimum entre 6 et 4 : {Math.Min(6, 4)}");
            WriteLine($"maximum entre 6, 4 et 5 : {Math.Max(Math.Max(6, 4), 5)}"); //on peut faire mieux avec LINQ et les collections

            WriteLine();

            //signes et valeurs absolues
            WriteLine($"valeur absolue de -4,567 : {Math.Abs(-4.567)}");
            WriteLine($"signe de -4.567 : {Math.Sign(-4.567)}");

            WriteLine();

            //racines carrées, puissances, exponentielles et logarithmes
            WriteLine($"Racine carrée de 625 : {Math.Sqrt(625)}");
            WriteLine($"8 à la puissance 3 : {Math.Pow(8, 3)}");
            WriteLine($"exponentielle de 1 (Exp(1)) : {Math.Exp(1)}");
            WriteLine($"logarithme népérien de e (ln(e)) : {Math.Log(Math.E)}");
            WriteLine($"logarithme décimal de 10 (log(10)) : {Math.Log10(10)}");

            WriteLine();

            //trigonométrie
            WriteLine($"sin(pi/2) : {Math.Sin(Math.PI / 2)}");
            WriteLine($"cos(pi/2) : {Math.Cos(Math.PI / 2)}");
            WriteLine($"tan(pi/2) : {Math.Tan(Math.PI / 2)}");
            //il existe aussi : le sinus hyperbolique (Sinh), le cosinus hyperbolique (Cosh), la tangente hyperbolique (Tanh)
            //                  arcsinus (Asin), arccosinus (Acos) et arctan (Atan)

            WriteLine();

            //autres exemples :
            WriteLine($"exp(e) = {Math.Exp(Math.E)}");
            WriteLine($"ln(exp(1)) = {Math.Log(Math.Exp(1))}");
            WriteLine($"sqrt(|cos(1)|) = {Math.Sqrt(Math.Abs(Math.Cos(1)))}");

            WriteLine();

            //L'assemblage System.Numerics.dll introduit également deux nouvelles classes :
            // BigInteger : un très grand entier sans perte de précision
            // Complex : une structure représentant un nombre complexe
            System.Numerics.BigInteger carlos = System.Numerics.BigInteger.Pow(2, 200);
            WriteLine($"2 à la puissance 200 :\n{carlos}");

            System.Numerics.Complex complex = new System.Numerics.Complex(2, 3);
            System.Numerics.Complex complex2 = new System.Numerics.Complex(-5, 2);
            WriteLine($"Partie réelle de complex : {complex.Real} / partie imaginaire de complex : {complex.Imaginary}");
            //les opérateurs sont réécrits pour les nombres complexes
            System.Numerics.Complex complex3 = complex + complex2;
            WriteLine($"(2 + 3i) + (-5 + 2i) = ({complex3.Real} + {complex3.Imaginary}i)");
            complex3 = System.Numerics.Complex.Conjugate(complex);
            WriteLine($"conjugué de (2 + 3i) = ({complex3.Real} + {complex3.Imaginary}i)");
        }
    }
}
