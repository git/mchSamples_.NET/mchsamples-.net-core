﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
//
// ========================================================================

using System;
using static System.Console;

namespace ex_004_010_GuidStruct
{
    class Program
    {
        /// <summary>
        /// la structure (voir chapitre 20) Guid permet de définir des indentifiants presque uniques.
        /// Lorsque vous générez un de ces identifiants, il est très fort probable qu'il soit unique au monde.
        /// Il y a 2 puissance 128 Guid différents.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Guid g = Guid.NewGuid();
            WriteLine(g);

            Guid g2 = new Guid("{9128de4d-1423-4e08-8ca6-a9effbba1f86}");
            Guid g3 = new Guid("9128de4d14234e088ca6a9effbba1f86");
            WriteLine(g2 == g3);

            WriteLine(Guid.Empty);
        }
    }
}
