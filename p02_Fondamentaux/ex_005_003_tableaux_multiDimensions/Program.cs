﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using static System.Console;

namespace ex_005_003_tableaux_multiDimensions
{
    class Program
    {
        static void Main(string[] args)
        {
            //TABLEAUX A PLUSIEURS DIMENSIONS
            int[,] mat = new int[2, 3]; //2 lignes et 3 colonnes
            int[,] mat2 = { { 1, 2, 3 }, { 4, 5, 6 } };
            int[,,] tab10;//3 dimensions
            //pour copier, on peut utiliser la méthode Clone
            //parcours d'un tableau à plusieurs dimensions
            WriteLine("\nTABLEAUX A DEUX DIMENSIONS");
            for (int ligne = 0; ligne < mat2.GetLength(0); ligne++)
            {
                for (int colonne = 0; colonne < mat2.GetLength(1); colonne++)
                {
                    Write($"{mat2[ligne, colonne]} ");
                }
                WriteLine();
            }

            //TABLEAUX EPARSES
            int[][] jagged = new int[3][];//tableaux à trois lignes de tailles différentes
            jagged[0] = new int[2];
            jagged[0][0] = 10;
            jagged[0][1] = 20;
            jagged[1] = new int[4];
            jagged[1][0] = 30;
            jagged[1][1] = 40;
            jagged[1][2] = 50;
            jagged[1][3] = 60;
            jagged[2] = new int[3];
            jagged[2][0] = 70;
            jagged[2][1] = 80;
            jagged[2][2] = 90;
            WriteLine("\nTABLEAUX EPARSES");
            for (int ligne = 0; ligne < jagged.GetLength(0); ligne++)
            {
                for (int colonne = 0; colonne < jagged[ligne].Length; colonne++)
                {
                    Write($"{jagged[ligne][colonne]} ");
                }
                WriteLine();
            }
            //initialisation directe
            int[][] jagged2 = new int[][]
                              {
                                  new int[] {10, 20},
                                  new int[] {30, 40, 50, 60},
                                  new int[] {70, 80, 90}
                              };


        }
    }
}
