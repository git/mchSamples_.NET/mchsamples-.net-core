﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using System.Text;
using static System.Console;

namespace ex_013_001_FormattingParsingTypesNumeriquesEtBool
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
            //Formatage avec ToString
            bool b = true;
            string s = b.ToString();
            WriteLine($"b vaut {s}");

            int i = 123, j = 1234, k = 12345;
            s = i.ToString();
            WriteLine($"i vaut {i}");

            WriteLine();

            WriteLine("FORMATS STANDARDS");
            //il existe énormément de variantes :
            //Format standards
            float f = 12345.6789f;
            float g = 0.00000006f;
            float h = 123456789.123f;
            WriteLine($"f = {f.ToString()}");
            WriteLine($"g = {g.ToString()}");
            WriteLine($"h = {h.ToString()}");
            WriteLine("\nG : notation exponentielle pour les grands ou les petits nombres");
            WriteLine($"f.ToString(\"G\") : {f.ToString("G")}");
            WriteLine($"g.ToString(\"G\") : {g.ToString("G")}");
            WriteLine($"h.ToString(\"G\") : {h.ToString("G")}");

            WriteLine("\nG3 : limite la notation à 3 chiffres maximum");
            WriteLine($"f.ToString(\"G3\") : {f.ToString("G3")}");
            WriteLine($"g.ToString(\"G3\") : {g.ToString("G3")}");
            WriteLine($"h.ToString(\"G3\") : {h.ToString("G3")}");

            WriteLine("\nF2 : arrondit à deux décimales");
            WriteLine($"f.ToString(\"F2\") : {f.ToString("F2")}");
            WriteLine($"g.ToString(\"F2\") : {g.ToString("F2")}");
            WriteLine($"h.ToString(\"F2\") : {h.ToString("F2")}");

            WriteLine("\nE : force la notation exponentielle (avec 6 chiffres, existe aussi avec E2, E3...");
            WriteLine($"f.ToString(\"E\") : {f.ToString("E")}");
            WriteLine($"g.ToString(\"E2 \") : {g.ToString("E2")}");
            WriteLine($"h.ToString(\"E3\") : {h.ToString("E3")}");

            WriteLine("\nC : currency (monnaie)");
            WriteLine($"f.ToString(\"C\") : {f.ToString("C")}");
            //number format info
            System.Globalization.NumberFormatInfo monnaie = new System.Globalization.NumberFormatInfo();
            monnaie.CurrencySymbol = "$$";
            WriteLine($"g.ToString(\"C2 \") : {g.ToString("C2", monnaie)}");
            //culture info
            System.Globalization.CultureInfo info = new System.Globalization.CultureInfo("en-GB");
            WriteLine($"h.ToString(\"C3\") : {h.ToString("C3", info)}");

            WriteLine();

            float p = 0.5349f, q = 0.53492f, r = 0.534923f;
            WriteLine($"p = {p.ToString()}");
            WriteLine($"q = {q.ToString()}");
            WriteLine($"r = {r.ToString()}");
            WriteLine("\nP : pourcentage");
            WriteLine($"p.ToString(\"P\") : {p.ToString("P")}");
            WriteLine($"a.ToString(\"P0 \") : {q.ToString("P0")}");
            WriteLine($"r.ToString(\"P1\") : {r.ToString("P1")}");

            WriteLine();

            WriteLine($"i = {i.ToString()}");
            WriteLine($"j = {j.ToString()}");
            WriteLine($"k = {k.ToString()}");
            WriteLine("\nD4 : pad left avec des 0 à gauche pour avoir au moins 4 chiffres");
            WriteLine($"i.ToString(\"D4\") : {i.ToString("D4")}");
            WriteLine($"j.ToString(\"D4\") : {j.ToString("D4")}");
            WriteLine($"k.ToString(\"D4\") : {k.ToString("D4")}");

            WriteLine();

            //formats personnalisés
            WriteLine("FORMATS PERSONNALISES");
            float l = 123.456f;
            WriteLine($"l = {l}");
            WriteLine("\n .## limite à deux nombres max après la virgule");
            WriteLine($"l.ToString(\".##\") : {l.ToString(".##")}");
            WriteLine($"l.ToString(\".###\") : {l.ToString(".###")}");
            WriteLine("\n .00 comme précédemment mais complète avec des 00");
            WriteLine($"l.ToString(\".00\") : {l.ToString(".00")}");
            WriteLine($"l.ToString(\".0000\") : {l.ToString(".0000")}");
            WriteLine($"l.ToString(\"00.00\") : {l.ToString("00.00")}");
            WriteLine($"l.ToString(\"0000.00\") : {l.ToString("0000.00")}");

            int m = 5, n = -5, o = 0;
            WriteLine();
            WriteLine($"m = {m}");
            WriteLine($"n = {n}");
            WriteLine($"o = {o}");
            WriteLine("\n +#;(#);zero rend le nombre \"+lenombre\" si positif, \"(lenombre)\" si négatif, \"zero\" si = 0");
            WriteLine($"m.ToString(\"+#;(#);zero\") : {m.ToString("+#;(#);zero")}");
            WriteLine($"n.ToString(\"+#;(#);zero\") : {n.ToString("+#;(#);zero")}");
            WriteLine($"o.ToString(\"+#;(#);zero\") : {o.ToString("+#;(#);zero")}");

            WriteLine();

            //parsing
            WriteLine("PARSING");

            //méthodes statiques Parse, mais il vaut mieux la mettre dans un bloc try
            WriteLine("i = int.Parse(\"yaha\");");
            try
            {
                i = int.Parse("yaha");
            }
            catch (FormatException e)
            {
                WriteLine($"le parse a échoué (exception : {e.Message})");
            }
            WriteLine("i = int.Parse(123);");

            try
            {
                i = int.Parse("123");
            }
            catch (FormatException e)
            {
                WriteLine($"le parse a échoué (exception : {e.Message})");
            }
            WriteLine(i);

            //ou avec TryParse : rend true si a fonctionné et le résultat dans le paramètre out
            //rend false si n'a pas fonctionné et il ne faut surtout pas regarder le résultat dans out
            // pas d'exception de lancée
            WriteLine("int.TryParse(\"yaha\", out i) + \" échec\"");
            WriteLine($"{int.TryParse("yaha", out i)} échec");
            WriteLine("int.TryParse(\"123\", out i) + \" \" + i");
            WriteLine($"{int.TryParse("123", out i)} {i}");

            //attention à la culture !
            //WriteLine(double.Parse("1.23"));
            WriteLine("double.Parse(\"1,23\")");
            WriteLine(double.Parse("1,23"));
            WriteLine("double.Parse(\"1.23\", System.Globalization.CultureInfo.InvariantCulture)");
            WriteLine(double.Parse("1.23", System.Globalization.CultureInfo.InvariantCulture));

            //on peut compléter le parsing avec des styles (flags)
            //autoriser les parenthèses :
            string stringToParse = "(123)";
            WriteLine($"stringToParse = {stringToParse}");
            WriteLine($"int.TryParse(stringToParse, out i) = {int.TryParse(stringToParse, out i)}");
            WriteLine("int.TryParse(stringToParse, System.Globalization.NumberStyles.AllowParentheses, System.Globalization.CultureInfo.CurrentCulture, out i) = {0}\ni vaut = {1}",
                int.TryParse(stringToParse,
                             System.Globalization.NumberStyles.AllowParentheses,
                             System.Globalization.CultureInfo.CurrentCulture,
                             out i),
                          i);
        }
    }
}
