﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_008_004_null_operators
{
    public class Program
    {
        public static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //C# fournit deux opérateurs pour simplifier le travail avec les nulls

            //Null-coalescing operator : ??
            //"si l'opérande n'est pas nulle, rends-la moi ; sinon, donne-moi la valeur par défaut que je t'ai renseignée"
            WriteLine("NULL-COLAESCING OPERATOR \n");
            WriteLine("s est une chaîne de caractères nulle");
            WriteLine("J'affiche s ?? \"rien\", c'est-à-dire que si s est null, on affichera la valeur par défaut : rien, sinon on affichera la valeur de s");
            string s = null;
            string result = s ?? "rien";
            WriteLine($"-> {result}");

            WriteLine();
            WriteLine("rentrez du texte, que je stockerai dans s :");
            s = ReadLine();
            WriteLine("J'affiche s ?? \"rien\", c'est-à-dire que si s est null, on affichera la valeur par défaut : rien, sinon on affichera la valeur de s");
            result = s ?? "rien";
            WriteLine($"-> {result}");
            WriteLine();

            //Elvis Operator ou Null-conditional operator : ?.
            //avec cet opérateur, pas besoin de tester si l'opérande est nulle avant d'appeler une méthode ou une propriété :
            // si elle n'est pas nulle, fonctionne comme le .
            // si elle est nulle, renvoie null (sans lancer l'exception NullReferenceException)

            WriteLine("Elvis operator ou Null-Conditional operator \n");
            WriteLine("s2 est une chaîne de caractères nulle");
            WriteLine("Je veux stocker s2.ToUpper() dans s3, mais puisque s2 est nulle, ça lance une exception :");
            string s2 = null;
            string s3 = null;
            try
            {
                s3 = s2.ToUpper();
                WriteLine(s3 != null ? s3 : "null");
            }
            catch(NullReferenceException exception)
            {
                WriteLine(exception);
            }
            WriteLine();

            WriteLine("Maintenant je veux stocker s2?.ToUpper() dans s3. Puisque s2 est nulle, null est stocké dans s3 mais sans lancer d'exception.");
            s3 = s2?.ToUpper();
            WriteLine(s3 != null ? s3 : "null");
            WriteLine();
            WriteLine("\nmême chose avec le texte que vous allez rentrer :");
            s2 = ReadLine();
            s3 = s2?.ToUpper();
            WriteLine(s3 != null ? s3 : "null");

        }
    }
}
