﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
// Mise à jour   : 2017-10-03
//
// ========================================================================

using static System.Console;
using static System.Text.Encoding;

namespace ex_004_001_TypesNumeriques
{
    class Program
    {
        /// <summary>
        /// Dans cet exemple, plusieurs variables de différents types sont présentées. Notez qu'un constante
        /// de type float nécessite un f pour être différenciée d'un double
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OutputEncoding = UTF8;

            //types entiers
            byte b; //variable codée sur un octet (n'importe quelle valeur entre 0 et 255)
            sbyte sb; //variable codée sur un octet mais signée (n'importe quelle valeur entre -128 et 127)
            short sh; //entier signé sur 16 bits. de -32768 à 32767 (automatiquement converti en int pendant une opération arithmétique)
            ushort ush; //idem short, mais non signé : de 0 à 65535
            int i; //entier codé sur 32 bits.
            i = 10;
            uint ui; //entier non signé codé sur 32 bits.
            long l; //entier signé codé sur 64 bits.
            l = 10L;
            ulong ul; //entier non signé codé sur 64 bits.

            //types réels
            float f; //codé sur 32 bits
            f = 4.5f;
            double d; //codé sur 64 bits
            d = 4.5;
            decimal dec; //codé sur 128 bits, spécialement créé pour les applications financières. Plus précis mais 10 fois plus lent.
            dec = 1.2m;

            //le qualificatif const permet de garantir qu'une "variable" ne sera pas modifiée
            const int ci = 3;

            //membres des types numériques
            WriteLine($"int.MinValue = {int.MinValue}");
            WriteLine($"int.MaxValue = {int.MaxValue}");
            WriteLine($"float.MinValue = {float.MinValue}");
            WriteLine($"float.MaxValue = {float.MaxValue}");
            WriteLine($"float.Epsilon = {float.Epsilon}");
            WriteLine($"float.NegativeInfinity = {float.NegativeInfinity}");
            WriteLine($"float.PositiveInfinity = {float.PositiveInfinity}");

            //valeurs par défaut :
            //numerique : 0


            //Depuis C# 7.0, il y a eu quelques améliorations dans l'écriture des constantes numériques :
            // - on peut maintenant utiliser l'underscore _ pour améliorer la lisibilité des grands nombres :
            d = 123_456_789;
            WriteLine(d);

            // - on peut également l'utiliser sur les valeurs en hexadécimal :
            d = 0x7_5B_CD_15;
            WriteLine(d);

            // - et également sur les nombres en binaire, ce qui peut éviter d'avoir à apprendre le nombre en hexadécimal :)
            d = 0b111_0101_1011_1100_1101_0001_0101;
            WriteLine(d);
        }
    }
}
