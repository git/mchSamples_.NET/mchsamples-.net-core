﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_008_002_switch
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Random random = new Random();
            int i = random.Next(6);

            switch (i)
            {
                case 0:
                    WriteLine("i vaut 0"); //1 ou plusieurs instruction puis break; ("obligatoire")
                    break;
                case 1:
                case 2:
                    WriteLine("i vaut 1 ou 2"); // pour que deux case partagent les mêmes instructions,
                    break;                              // il faut que le premier soit vide et sans break;
                case 3:
                    WriteLine("goto à la place de break"); //si le case n'est pas vide, je peux quand même rejoindre un autre case,
                    goto case 4;                                    // à l'aide d'un goto
                case 4:
                    WriteLine("i vaut 3 ou 4");
                    break;
                default:
                    WriteLine("i vaut autre chose");
                    break;
            }

            string[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };

            string jour = jours[random.Next(jours.Length)];

            //le switch fonctionne pour les chaînes de caractères !!!
            switch (jour)
            {
                case "lundi":
                    WriteLine($"J'aime pas le {jour}");
                    break;
                case "mardi":
                    WriteLine($"Faut travailler un peu... le {jour}");
                    break;
                case "mercredi":
                    WriteLine("Ah l'époque où on ne travaillait pas le mercredi !");
                    break;
                case "jeudi":
                case "vendredi":
                    goto case "mardi";
                case "samedi":
                    WriteLine("Vivement le weekend");
                    break;
                case "dimanche":
                    WriteLine("Je travaillerai demain");
                    break;
                default:
                    WriteLine("Quel jour on est !!!!????");
                    break;
            }
        }
    }
}
