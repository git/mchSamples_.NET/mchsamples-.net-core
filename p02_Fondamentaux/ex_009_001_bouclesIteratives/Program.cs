﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_009_001_bouclesIteratives
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };

            //BOUCLE WHILE
            WriteLine("boucle while");
            int i = 0;
            while (i < jours.Length)
            {
                Write($"{jours[i]} ");
                i++;
            }

            WriteLine();

            //BOUCLE DO-WHILE
            WriteLine("boucle do-while");
            int j = 0;
            do
            {
                Write($"{jours[j]} ");
                j++;
            }
            while (j < jours.Length);

            WriteLine();

            //BOUCLE FOR
            WriteLine("boucle for");
            for (int k = 0; k < jours.Length; k++)
            {
                Write($"{jours[k]} ");
            }

            WriteLine();

            //BOUCLE FOREACH
            WriteLine("boucle foreach");
            foreach (string jour in jours)
            {
                Write($"{jour} ");
            }

            WriteLine();

            foreach (string jour in jours)
            {
                if (jour == "Mercredi")
                    continue; //continue est autorisé : passe directement à l'itération suivante 
                if (jour == "Samedi")
                    break;    //break est autorisé : arrête la boucle
                WriteLine(jour);
            }
        }
    }
}
