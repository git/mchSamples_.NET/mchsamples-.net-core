﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
// Updated       : 2017-09-19
//
// ========================================================================

using System;
using static System.Console;

namespace ex_004_009_Tuple
{
    partial class Program
    {
        /// <summary>
        /// la classe Tuple est générique (voir chapitre 22) et permet de stocker plusieurs références ou valeurs de différents types.
        /// Tuple est notamment très pratique pour avoir plusieurs retours de méthodes.
        /// Tuple possède des propriétés (voir chapitre 16) en lecture seule permettant d'accéder aux différents éléments du Tuple.
        /// Par exemple, si on déclare Tuple<string, int, float>, alors Item1 permet d'accéder au string, Item2 à l'int, Item3 au float.
        /// 
        /// Les Tuples sont des types "valeur" et leurs éléments sont publics et mutables. Ils implémentent le protocole d'égalité des types "valeur",
        /// i.e. 2 Tuples sont égaux (et ont le même hash code) si et seulement si leurs éléments sont égaux deux à deux (et ont le même hashcode).
        /// Ceci rend les Tuples très utiles dans de nombreuses situations. Par exemple : 
        ///     - un dictionnaire avec plusieurs clés : utilisez un tuple comme clef
        ///     - une liste avec plusieurs valeurs à chaque indice : utilisez un tuple
        ///     
        /// Depuis, C#7.0, il y a eu de nombreux ajouts aux Tuples (voir le 2ème fichier : Program.CSharp7.cs)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //création d'un Tuple
            Tuple<string, int, float> tuple = new Tuple<string, int, float>("ObiWan Kenobi", 4, 3.2f);
            Tuple<string, int, float> tuple2 = Tuple.Create("ObiWan Kenobi", 4, 3.2f);

            WriteLine($"{tuple.Item1} {tuple.Item2} {tuple.Item3}");

            WriteLine();
            int a = 3;
            var result = DoubleEtTriple(a);
            WriteLine($"le double et le triple de {a} sont {result.Item1} et {result.Item2}");

            NouveautésCSharp7();
        }

        /// <summary>
        /// une méthode qui rend le double et le triple d'un nombre
        /// </summary>
        /// <param name="a">nombre à doubler et tripler</param>
        /// <returns>le double et le triple du nombre</returns>
        static Tuple<int, int> DoubleEtTriple(int a)
        {
            return Tuple.Create(a * 2, a * 3);
        }
    }
}
