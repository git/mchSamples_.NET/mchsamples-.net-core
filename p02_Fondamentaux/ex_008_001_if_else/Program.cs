﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_008_001_if_else
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            int vitesseMax = 70;
            WriteLine($"La vitesse maximum autorisée est de {vitesseMax} km/h");
            Random random = new Random();
            int vitesse = random.Next(50, 150);
            WriteLine($"Vous roulez à {vitesse} km/h");

            if (vitesse <= vitesseMax)
            {
                WriteLine("Vous ne dépassez pas la vitesse maximum");
            }
            else
            {
                WriteLine("Vous dépassez la vitesse maximum autorisée");
                if (vitesse < vitesseMax + 20)
                {
                    WriteLine("L'amende forfaitaire s'élève à 68 euros");
                    WriteLine("1 point est retiré du permis de conduire");
                }
                else if (vitesse < vitesseMax + 50)
                {
                    WriteLine("L'amende forfaitaire s'élève à 135 euros");
                    if (vitesse < vitesseMax + 30)
                    {
                        WriteLine("2 points sont retirés du permis de conduire");
                    }
                    else if (vitesse < vitesseMax + 40)
                    {
                        WriteLine("3 points sont retirés du permis de conduire");
                    }
                    else if (vitesse < vitesseMax + 50)
                    {
                        WriteLine("4 points sont retirés du permis de conduire");

                    }
                }
                else
                {
                    WriteLine("L'amende fofaitaire s'élève à 1500 euros");
                    WriteLine("6 points sont retirés du permis de conduire");
                }
                if (vitesse >= vitesseMax + 30)
                {
                    WriteLine("Jusqu'à 3 ans de suspension de permis de conduire");
                    if (vitesse >= vitesseMax + 40)
                    {
                        if (vitesse >= vitesseMax + 50)
                        {
                            WriteLine("Obligation d'accomplir, à ses frais, un stage de sensibilisation à la sécurité routière, confiscation du véhicule dont le prévenu s'est servi pour commettre l'infraction, s'il en est propriétaire.");
                        }
                        WriteLine("Une suspension immédiate du permis de conduire s'ajoute aux sanctions ci-dessus");
                    }
                }
            }
        }
    }
}
