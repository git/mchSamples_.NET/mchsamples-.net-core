﻿## How to change the arguments passed to the Main method?

##### Method 1:
- Access the properties of the project (right click on the Project file -> Properties).
- Access the debug tab
- You can now change the application arguments!
##### Method 2:
- Change the launchSettings.json file in Properties  under the project file
- to (for instance):
``` json
{
  "profiles": {
    "ex_003_003_Main_arguments": {
      "commandName": "Project",
      "commandLineArgs": "I love C# and .NET Core"
    }
  }
}
```