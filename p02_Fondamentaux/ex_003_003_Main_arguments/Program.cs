﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-21
//
// ========================================================================

using System;
using static System.Console;

namespace ex_003_003_Main_arguments
{
    class Program
    {
        //les arguments sont passés dans un tableau de chaîne de caractères
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            WriteLine("Coucou le monde !");

            //on peut en savoir le nombre en utilisant la propriété Length
            //ici, {0} sera remplacé par la valeur de args.Length
            WriteLine($"Vous avez passé {args.Length} argument(s).");

            WriteLine("Voici les arguments que vous avez passés :");

            //foreach permet de parcourir les éléments de ce tableau d'arguments
            foreach (string arg in args)
            {
                // \t permet d'insérer une tabulation
                //ici, {0} sera remplacé par la valeur de arg
                WriteLine($"\t{arg}");
            }
        }
    }
}
