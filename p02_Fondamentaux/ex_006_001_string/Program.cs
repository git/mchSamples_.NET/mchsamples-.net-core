﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using System;
using static System.Console;

namespace ex_006_001_string
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            WindowHeight = LargestWindowHeight;
            WindowWidth += 15;

            WriteLine("STRING : Chaîne de caractères immuable");

            //construction
            WriteLine("CONSTRUCTION");
            string s1 = "Bonjour";

            //caractères spéciaux avec le caractère d'échappement \
            WriteLine("CARACTERES SPECIAUX");
            string s2 = "\'coucou1\' \"coucou2\" \\coucou3\\";
            WriteLine(s2);
            s2 = "nouvelle\nligne";
            WriteLine(s2);
            s2 = "retour\rcharriot";
            WriteLine(s2);
            s2 = "horizontal\ttab";
            WriteLine(s2);
            s2 = "alerte audio \a"; // \a fait un son lorsque le string est affiché
            WriteLine(s2);

            //verbatim
            WriteLine("\n VERBATIM");
            string s3 = "\\\\server\\machin\\trucmuche\\home"; // chaîne non verbatim, il faut utiliser le caractère d'échappement pour afficher 
                                                               // les backslashs
            WriteLine(s3);
            s3 = @"\\server\machin\truchmuche\home";           // chaîne verbatim, les caractères spéciaux sont traités comme les autres
            WriteLine(s3);

            //string vide
            WriteLine("\n CHAINE DE CARACTERES VIDE");
            string s4 = "";
            WriteLine($"la chaîne de caractères : \"{s4}\" a une longueur de : {s4.Length} caractère(s)");
            //ou
            s4 = String.Empty;
            WriteLine($"la chaîne de caractères : \"{s4}\" a une longueur de : {s4.Length} caractère(s)");
            WriteLine($"s4 == \"\" ? {s4 == ""}");
            WriteLine($"s4 == String.Empty ? {s4 == string.Empty}");
            WriteLine($"s4.Length == 0 ? {s4.Length == 0}");

            //string null (string est reference type)
            WriteLine("\n CHAINE DE CARACTERES NULLE");
            string s5 = null;
            WriteLine($"s5 == \"\" ? {s5 == ""}");
            try
            {
                WriteLine($"s5.Length == 0 ? {s5.Length == 0}");
            }
            catch
            {
                WriteLine($"s5.Length == 0 ? False");
            }

            WriteLine($"string.IsNullOrEmpty(s4) ? {string.IsNullOrEmpty(s4)}");
            WriteLine($"string.IsNullOrEmpty(s5) ? {string.IsNullOrEmpty(s5)}");
            WriteLine($"string.IsNullOrWhiteSpace(\"   \") ? {string.IsNullOrWhiteSpace("   ")}");

            //accéder et chercher dans les string
            WriteLine($"\n RECHERCHER DANS LES STRING");
            string s6 = "barbapapa";
            WriteLine(s6);
            WriteLine($"3ème caractère : {s6[2]}");
            WriteLine($"s6.Contains(\"pa\") ? {s6.Contains("pa")}");
            WriteLine($"s6.EndsWith(\"pa\") ? {s6.EndsWith("pa")}");
            WriteLine($"s6.StartsWith(\"ba\") ? {s6.StartsWith("ba")}");
            WriteLine($"s6.IndexOf(\"ba\") ? {s6.IndexOf("ba")}");

            //manipuler les string
            WriteLine($"\n MANIPULER LES STRING");
            WriteLine($"CONCATENATION DE STRING");
            //concatenation de string
            string s7 = "Jim" + " " + "Raynor";
            WriteLine($"\"Jim\" + \" \" + \"Raynor\" = \"{s7}\"");
            //substring
            WriteLine($"SUBSTRING");
            string s8 = s7.Substring(0, 3);
            WriteLine($"\"Jim Raynor\".Substring(0, 3) = {s8}");
            s8 = s7.Substring(2, 3);
            WriteLine($"\"Jim Raynor\".Substring(2, 3) = {s8}");
            s8 = s7.Substring(5);
            WriteLine($"\"Jim Raynor\".Substring(5) = {s8}");

            //insert, remove, replace, ToUpper, ToLower
            WriteLine($"REMOVE");
            s8 = s7.Remove(2, 3);
            WriteLine($"\"Jim Raynor\".Remove(2, 3) = {s8}");
            WriteLine($"INSERT");
            s8 = s8.Insert(2, "m R");
            WriteLine($"\"Jiaynor\".Insert(2, \"m R\") = {s8}");
            WriteLine($"REPLACE");
            s8 = "Les chaussettes de l'archiduchesse";
            s8 = s8.Replace(" ", "_");
            WriteLine($"\"Les chaussettes de l'archiduchesse\".Replace(\" \", \"_\") = {s8}");
            WriteLine($"TO UPPER, TO LOWER");
            s8 = s8.ToUpper();
            WriteLine($"\"Les_chaussettes_de_l'archiduchesse\".ToUpper() = {s8}");
            s8 = s8.ToLower();
            WriteLine($"\"LES_CHAUSSETTES_DE_L'ARCHIDUCHESSE\".ToLower() = {s8}");


            //PadLeft, PadRight
            WriteLine("\n PADLEFT, PADRIGHT, TRIM");
            s8 = s8.PadLeft(50).PadRight(60, '!');
            WriteLine($"\"les_chaussettes_de_l'archiduchesse\".PadLeft(50).PadRight(60, \'!\') = \n{s8}");


            //Trim : enlève les blancs (whitespace, tab, new lines...)
            s8 = s8.Trim(); //existe aussi avec TrimStart et TrimEnd
            WriteLine($"\"                les_chaussettes_de_l'archiduchesse!!!!!!!!!!\".Trim() = \n{s8}");

            //Split et Join
            WriteLine("\n SPLIT ET JOIN");
            s8 = "Les chaussettes de l'archiduchesse";
            WriteLine($"\"{s8}\" est composé des mots suivants \n(\"Les chaussettes de l'archiduchesse\".Split()): ");
            string[] mots = s8.Split();
            foreach (string m in mots)
            {
                WriteLine($"\t- {m}");
            }
            WriteLine("On peut regrouper les mots précédents (tableau de string : string[] mots;) avec Join");
            string s9 = string.Join(" ", mots);
            WriteLine($"string.Join(\" \", mots) = {s9}");

            //Format
            WriteLine("\n STRING.FORMAT");
            string format = "Il y a {0} élèves inscrits en {1} année à l'IUT pour l'année scolaire {2}";
            WriteLine("Le format : \"{0}\"", format);
            string chaineFormatée = string.Format(format, 110, "2ème", "2011-2012");
            WriteLine("chaîne formatée avec le format : {0}", chaineFormatée);
            WriteLine("on peut aussi tout faire dans le WriteLine, sans préciser string.Format : ");
            WriteLine("Il y a {0} élèves inscrits en {1} année à l'IUT pour l'année scolaire {2}", 110, "2ème", "2010-2011");
            format = "Nb élèves : {0, -5} Année : {1, -5} Année universitaire : {2, 15}";
            WriteLine("format plus élaboré, pour imposer des tailles : \n{0}", format);
            WriteLine("Quelques chaînes formatées (avec ou sans string.Format pour voir la différence d'écriture) : ");
            chaineFormatée = string.Format(format, 110, "2ème", "2010-2011");
            WriteLine(chaineFormatée);
            chaineFormatée = "Nb élèves : " + 110.ToString().PadRight(5) + " Année : " + "2ème".PadRight(5) + " Année universitaire : " + "2010-2011".PadLeft(15);
            WriteLine(chaineFormatée);
            chaineFormatée = "Nb élèves : " + 98.ToString().PadRight(5) + " Année : " + "2ème".PadRight(5) + " Année universitaire : " + "2010-2011".PadLeft(15);
            WriteLine(chaineFormatée);

            //String Interpolation
            WriteLine("\n STRING INTERPOLATION avec $");
            WriteLine($"Il y a {135} élèves inscrits en {"1ère"} année à l'IUT pour l'année scolaire {"2016 - 2017"}");

            //comparaison de string
            WriteLine("\n COMPARAISON DE STRING");
            //equality comparison
            WriteLine($"\"yaha\" == \"YAHA\" ? " + ("yaha" == "YAHA"));
            WriteLine($"\"yaha\".Equals(\"YAHA\") ? " + ("yaha".Equals("YAHA")));
            WriteLine($"string.Equals(\"yaha\", \"YAHA\") ? " + string.Equals("yaha", "YAHA"));
            WriteLine($"\"yaha\".Equals(\"YAHA\", StringComparison.CurrentCultureIgnoreCase) ? " + ("yaha".Equals("YAHA", StringComparison.CurrentCultureIgnoreCase)));
            WriteLine($"string.Equals(\"yaha\", \"YAHA\", StringComparison.CurrentCultureIgnoreCase) ? " + string.Equals("yaha", "YAHA", StringComparison.CurrentCultureIgnoreCase));

            WriteLine($"string.Equals(\"éèàô\", \"eeao\") ? " + string.Compare("éèàô", "eeao", StringComparison.CurrentCultureIgnoreCase));

        }
    }
}
