﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
// Mise à jour   : 2017-09-18
//
// ========================================================================

using System;
using static System.Console;

namespace ex_008_003_ternary_operator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //L'opérateur conditionnel (conditional operator), aussi appelé opérateur ternaire (ternary operator),
            //est le seul opérateur à prendre trois opérandes.
            //Il a la forme suivante : c ? a : b
            //où c est la condition,
            //   a est la valeur de retour si c est true
            //   b est la valeur de retour si c est false

            WriteLine("Rentrez quelque chose, je vous dirai si c'est un nombre ou non :");
            string chaine = ReadLine();
            bool condition = float.TryParse(chaine, out var nombre);
            WriteLine(condition ? $"j'ai reconnu le nombre : {nombre.ToString()}" : "ce que vous avez entré n'est pas un nombre");
        }
    }
}
