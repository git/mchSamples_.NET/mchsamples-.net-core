﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using static System.Console;

namespace ex_005_002_copie_de_tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //WindowHeight = LargestWindowHeight;


            //COPIE DE TABLEAUX
            WriteLine("\nCopie de tableaux");

            //1. copie de références
            WriteLine("\nCopie de références\n");
            //préparation des tableaux
            int[] tab6 = { 1, 2, 3 };
            int[] tab7 = { 5, 6, 7, 8, 9, 10 };
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab7");
            foreach (int i in tab7) Write($"{i} ");
            WriteLine();
            //recopie des références
            WriteLine("tab7 = tab6;");
            tab7 = tab6; //attention, on ne copie que les références ! le contenu est donc partagé ! La preuve :
            //affichage des tableaux après copie des références
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab7");
            foreach (int i in tab7) Write($"{i} ");
            WriteLine();
            //modification d'une case d'un des tableaux
            WriteLine("tab7[1] = 4;");
            tab7[1] = 4;
            //affichage des contenus après copie modification d'un des deux tableaux
            WriteLine("contenu de tab7");
            foreach (int i in tab7) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();

            //2. copie de contenus, méthode 1 : new + CopyTo
            WriteLine("\nCopie de contenus, méthode 1\n");
            //préparation des tableaux
            int[] tab8 = { 5, 6, 7, 8, 9, 10 };
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab8");
            foreach (int i in tab8) Write($"{i} ");
            WriteLine();
            //recopie des contenus
            WriteLine("tab8 = new int[tab6.Length];\ntab6.CopyTo(tab8, 0);");
            tab8 = new int[tab6.Length]; // on alloue la place de tab6 pour tab8
            tab6.CopyTo(tab8, 0); // on recopie le contenu de tab6 dans tab8 (ils ont la même taille à cause de la ligne d'avant, donc tout va bien)
            //affichage des résultats
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab8");
            foreach (int i in tab8) Write($"{i} ");
            WriteLine();
            //modification d'un des deux tableaux
            WriteLine("tab8[1] = 2;");
            tab8[1] = 2;
            //affichage des contenus après modification d'un des deux tableaux
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab8");
            foreach (int i in tab8) Write($"{i} ");
            WriteLine();

            //3. copie de contenus, méthode 2 : Clone
            WriteLine("\nCopie de contenus, méthode 2\n");
            //préparation des tableaux
            int[] tab9 = { 5, 6, 7, 8, 9, 10 };
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab9");
            foreach (int i in tab9) Write($"{i} ");
            WriteLine();
            //clonage des tableaux
            WriteLine("tab9 = (int[])tab6.Clone();");
            tab9 = (int[])tab6.Clone();
            //affichage après clonage
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab9");
            foreach (int i in tab9) Write($"{i} ");
            WriteLine();
            //modification d'un des deux tableaux
            WriteLine("tab9[1] = 2;");
            tab9[1] = 2;
            //affichage après modification
            WriteLine("contenu de tab6");
            foreach (int i in tab6) Write($"{i} ");
            WriteLine();
            WriteLine("contenu de tab9");
            foreach (int i in tab9) Write($"{i} ");
            WriteLine();
        }
    }
}
