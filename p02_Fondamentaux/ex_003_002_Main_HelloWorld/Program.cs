﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-21
//
// ========================================================================

//Ici, nous avons besoin de using System; car la classe Console est dans l'espace de noms System.
using System;
using static System.Console;

//Ce programme affiche "Coucou le monde !" dans la console.
namespace ex_003_002_Main_HelloWorld
{
    class Program
    {
        //aucun argument
        static void Main()
        {
            WriteLine("Coucou le monde !");
        }
    }
}
