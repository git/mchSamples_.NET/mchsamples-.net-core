﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_013_002_FormattingParsingDateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;
      
            //Formatages standards et sensible à la culture
            DateTime maintenant = DateTime.Now;

            WriteLine(maintenant.ToString("d"));
            WriteLine(maintenant.ToString("D"));
            WriteLine(maintenant.ToString("t"));
            WriteLine(maintenant.ToString("T"));
            WriteLine(maintenant.ToString("f"));
            WriteLine(maintenant.ToString("F"));
            WriteLine(maintenant.ToString("g"));
            WriteLine(maintenant.ToString("m"));
            WriteLine(maintenant.ToString("y"));

            //Formatage standard et insensible à la culture
            WriteLine(maintenant.ToString("o")); //conseillé lors de l'écriture dans un fichier (pour garantir la lecture)

            //Formatages personnalisés
            WriteLine(maintenant.ToString("yyyy-MM-dd HH:mm:ss"));
            WriteLine(maintenant.ToString("dd MMM yyyy HH:mm:ss"));
            WriteLine(maintenant.ToString("ddd dd MMMM yyyy HH:mm:ss"));
            WriteLine(maintenant.ToString("dddd dd MMMM yyyy HH:mm:ss"));

            //le troisième argument de ParseExact permet de préciser la culture. Si null, alors prend la culture par défaut sur votre .NET
            string dateString = maintenant.ToString("o");
            DateTime dateParsée = DateTime.ParseExact(dateString, "o", null);
            WriteLine(dateParsée);

            dateString = maintenant.ToString("dddd dd MMMM yyyy HH:mm:ss");
            dateParsée = DateTime.ParseExact(dateString, "dddd dd MMMM yyyy HH:mm:ss", null);
            WriteLine(dateParsée);
        }
    }
}
