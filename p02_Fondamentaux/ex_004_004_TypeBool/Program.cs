﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using static System.Console;

namespace ex_004_004_TypeBool
{
    class Program
    {
        static void Main(string[] args)
        {
            //type booléen
            bool boolean; //true ou false

            boolean = true;
            WriteLine($"true : {true}");
            WriteLine($"false : {false}");

            WriteLine($"FalseString : {bool.FalseString}");
            WriteLine($"TrueString : {bool.TrueString}");
        }
    }
}
