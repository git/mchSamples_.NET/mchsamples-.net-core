﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;

namespace ex_010_002_optionalParameters
{
    class ExFunct
    {
        //arguments optionnels (et arguments nommés)
        public string SomeString(int x = 1, int y = 2, int z = 3)
        {
            return string.Format($"x : {x}, y : {y}, z : {z}");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            ExFunct f = new ExFunct();

            WriteLine("arguments optionnels et arguments nommés");
            WriteLine($"SomeString() : {f.SomeString()}");
            WriteLine($"SomeString(4) : {f.SomeString(4)}");
            WriteLine($"SomeString(4, 5) : {f.SomeString(4, 5)}");
            WriteLine($"SomeString(4, 5, 6) : {f.SomeString(4, 5, 6)}");
            WriteLine($"SomeString(y:5, z:6) : {f.SomeString(y: 5, z: 6)}");
            WriteLine($"SomeString(y:5) : {f.SomeString(y: 5)}");
            //WriteLine($"SomeString(y:5, 6) : {f.SomeString(y:5, 6)}");
            //ne compile pas (pas d'arguments positionnels après un argument nommé)
        }
    }
}
