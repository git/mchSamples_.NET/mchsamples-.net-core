﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using System;
using static System.Console;

namespace ex_004_006_TimeSpan_et_DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //TimeSpan
            //construction d'une durée à partir d'un constructeur
            TimeSpan durée_d_un_cours = new TimeSpan(1, 50, 00);
            //construction d'une durée à partir d'un nombre de minutes (ou heures, ou secondes...)
            TimeSpan retard = TimeSpan.FromMinutes(21.20);
            //opération (addition, soustraction) de durées
            TimeSpan durée_du_cours_aujourd_hui = durée_d_un_cours + retard;
            //durée nulle
            TimeSpan durée_nulle = TimeSpan.Zero;
            //affichage d'une durée
            WriteLine($"Le cours d'aujourd'hui est de {durée_d_un_cours} + {retard} de retard = {durée_du_cours_aujourd_hui}");
            WriteLine();

            //DateTime
            //construction d'un DateTime
            DateTime débutDeJazzEnTete2012 = new DateTime(2012, 10, 23, 20, 00, 00);
            DateTime finDeJazzEnTete2012 = new DateTime(2012, 10, 27, 23, 59, 00);
            //soustraction de DateTime
            TimeSpan duréeDeJazzEnTete2012 = finDeJazzEnTete2012.Subtract(débutDeJazzEnTete2012);
            //affichage de DateTime
            WriteLine($"Le festival jazz en tête a duré : {duréeDeJazzEnTete2012}");
            WriteLine($"Le festival jazz en tête a duré : {duréeDeJazzEnTete2012.Days} jours et {duréeDeJazzEnTete2012.Hours} heures");
            WriteLine($"Le festival jazz en tête a duré : {duréeDeJazzEnTete2012.Days} jours et {Math.Round(duréeDeJazzEnTete2012.TotalHours - (duréeDeJazzEnTete2012.Days * 24))} heures");
            //on peut ajouter ou soustraire du temps à 1 DateTime
            DateTime débutDernierConcert = finDeJazzEnTete2012 - TimeSpan.FromHours(4);
            //autre solution équivalente
            débutDernierConcert = finDeJazzEnTete2012.Subtract(TimeSpan.FromHours(4));
            WriteLine($"Le dernier concert de Jazz en Tete commence le : {débutDernierConcert.ToString("dd/MM/yyy")} à {débutDernierConcert.ToString("hh:mm")}");

            WriteLine();

            //gestion des décalages horaires
            DateTime début_de_la_finale_de_l_USOpen = new DateTime(2010, 09, 13, 20, 00, 00, DateTimeKind.Utc);
            WriteLine($"La finale commencera : {début_de_la_finale_de_l_USOpen.ToLocalTime().ToString("f")} chez nous");
            WriteLine($"La finale commencera : {(début_de_la_finale_de_l_USOpen - TimeSpan.FromHours(4)).ToString("g")} à New York");
            WriteLine();

            //on peut parser une chaîne de caractères !
            string date_réjouissances = "jeudi 29 septembre 2011 13:30";
            WriteLine("Convertir jeudi 29 septembre 2011 13:30 en DateTime ? oui bien sûr !");
            DateTime réjouissances = DateTime.Parse(date_réjouissances);
            WriteLine(réjouissances);

            //la date et l'heure actuelles (au moment de l'exécution de la ligne)
            //est donnée par :
            DateTime maintenant = DateTime.Now;
            WriteLine($"Maintenant : {maintenant}");
        }
    }
}
