﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using static System.Console;

namespace ex_004_005_TypeChar
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //type char
            char c; //codé sur 16 bits ( != du C++ ), caractère du système Unicode
            c = 'A';
            c = '\x41'; //0x41 (ou 65 en décimal) dans Unicode. lettre A
            c = (char)65; //idem
            c = '\u0041'; //idem
            //quelques caractères utiles
            c = '\n'; //à la ligne
            c = '\t'; //tabulation horizontale
            c = '\v'; //tabulation verticale
            c = '\\'; //backslash
            c = '\''; //single quote
            c = '\"'; //double quote
            c = '\0'; //null

            //quelques membres du type char :
            char a = 'A';
            char deux = '2';
            char virgule = ',';
            char blanc = ' ';
            //IsDigit : le caractère est-il un chiffre ?
            WriteLine($"char.IsDigit('A') ? {char.IsDigit(a)}");
            WriteLine($"char.IsDigit('2') ? {char.IsDigit(deux)}");
            WriteLine($"char.IsDigit(',') ? {char.IsDigit(virgule)}");
            WriteLine($"char.IsDigit(' ') ? {char.IsDigit(blanc)}");
            WriteLine();
            //IsLetter indique s'il s'agit d'une lettre
            WriteLine($"char.IsLetter('A') ? {char.IsLetter(a)}");
            WriteLine($"char.IsLetter('2') ? {char.IsLetter(deux)}");
            WriteLine($"char.IsLetter(',') ? {char.IsLetter(virgule)}");
            WriteLine($"char.IsLetter(' ') ? {char.IsLetter(blanc)}");
            //il y a aussi IsLower pour les minuscules,  IsUpper pour les majuscules, IsLetterOrDigit pour lettres ou nombres
            WriteLine();

            //IsPunctuation indique s'il s'agit d'un caractère de ponctuation
            WriteLine($"char.IsPunctuation('A') ? {char.IsPunctuation(a)}");
            WriteLine($"char.IsPunctuation('2') ? {char.IsPunctuation(deux)}");
            WriteLine($"char.IsPunctuation(',') ? {char.IsPunctuation(virgule)}");
            WriteLine($"char.IsPunctuation(' ') ? {char.IsPunctuation(blanc)}");
            WriteLine();

            //IsWhiteSpace indique s'il s'agit d'un blanc
            WriteLine($"char.IsWhiteSpace('A') ? {char.IsWhiteSpace(a)}");
            WriteLine($"char.IsWhiteSpace('2') ? {char.IsWhiteSpace(deux)}");
            WriteLine($"char.IsWhiteSpace(',') ? {char.IsWhiteSpace(virgule)}");
            WriteLine($"char.IsWhiteSpace(' ') ? {char.IsWhiteSpace(blanc)}");
            WriteLine();

            //ToUpper et ToLower convertisse en majuscules ou en minuscules.
            WriteLine($"char.ToLower('A') = {char.ToLower('A')}");
            WriteLine($"char.ToUpper('a') = {char.ToUpper('a')}");
            WriteLine($"char.ToUpper('é') = {char.ToUpper('é')}");
        }
    }
}
