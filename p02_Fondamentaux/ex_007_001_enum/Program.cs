﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using System;
using static System.Console;
namespace ex_007_001_enum
{
    class Program
    {
        /// <summary>
        /// voici une énumération.
        /// public n'est pas obligatoire. S'il est indiqué, l'énumération est utilisable en dehors de la classe.
        /// </summary>
        [Flags]//pas obligatoire mais conseillé pour améliorer le ToString de l'enum dans le cas des combinaisons
        public enum MusicType : byte
        {
            Classic = 1,            //00000001
            Jazz = 2,               //00000010
            Pop = 4,                //00000100
            Rock = 8,               //00001000
            //on peut rajouter des combinaisons d'enum : ici, PopRock est à la fois Pop et Rock
            //il faut pour cela que toutes les valeurs d'enum puissent s'exclure mutuellement
            PopRock = Pop | Rock    //00001100
        }

        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            //déclaration d'une variable de type MusicType
            MusicType mMuzik;
            //affectation d'une valeur possible à cette variable
            mMuzik = MusicType.Jazz;
            WriteLine(mMuzik);

            //un exemple d'utilisation
            Write("la variable mMuzik est de type : ");
            switch (mMuzik)
            {
                case MusicType.Classic:
                    WriteLine("Classique"); break;
                case MusicType.Jazz:
                    WriteLine("Jazz"); break;
                case MusicType.Pop:
                    WriteLine("Pop"); break;
                case MusicType.Rock:
                    WriteLine("Rock"); break;
            }
            WriteLine();

            //un enum peut être considéré comme un instance de la classe Enum
            //exemple d'utilisation
            WriteLine("liste des valeurs de l'énumération");
            foreach (string s in Enum.GetNames(typeof(MusicType)))
                WriteLine(s);
            WriteLine();

            //autre utilisation
            Write("la variable mMuzik est de type : ");
            switch (mMuzik)
            {
                case MusicType.Classic:
                    WriteLine(MusicType.Classic.ToString()); break;
                case MusicType.Jazz:
                    WriteLine(MusicType.Jazz.ToString()); break;
                case MusicType.Pop:
                    WriteLine(MusicType.Pop.ToString()); break;
                case MusicType.Rock:
                    WriteLine(MusicType.Rock.ToString()); break;
            }
            WriteLine();

            //combinaison
            mMuzik = MusicType.Jazz | MusicType.Rock; // 00001010
            WriteLine(mMuzik); //si vous utilisez [Flags], affiche "Jazz, Rock", sinon, affiche "10"
            //on veut vérifier que mMuzik est du Jazz :
            if ((mMuzik & MusicType.Jazz) != 0) //mMuzik & Jazz = 00001010 & 00000010 = 00000010 != 0 c'est donc du jazz (entre autres)
            {
                WriteLine("Includes Jazz");
            }
            //on veut vérifier que mMuzik n'est pas de la Pop :
            if ((mMuzik & MusicType.Pop) == 0) //mMuzik & Pop = 00001010 & 00000100 = 00000000 == 0 ce n'est donc pas de la Pop
            {
                WriteLine("pas pop");
            }
            //finalement on veut rajouter Pop
            mMuzik |= MusicType.Pop; //00001010 | 00000100 = 00001110
            if ((mMuzik & MusicType.Pop) != 0) //mMuzik & Pop = 00001110 & 00000100 = 00000100 != 0 c'est donc de la Pop
            {
                WriteLine("pop");
            }
            //et enlever Jazz
            mMuzik ^= MusicType.Jazz; //00001110 ^ 00000010 = 00001100
            if ((mMuzik & MusicType.Jazz) == 0) //mMuzik & Jazz = 00001100 & 00000010 = 00000000 == 0 ce n'est donc plus du jazz
            {
                WriteLine("pas jazz");
            }
        }
    }
}
