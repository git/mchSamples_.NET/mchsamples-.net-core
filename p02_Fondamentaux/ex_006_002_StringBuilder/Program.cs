﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-22
//
// ========================================================================

using System;
using System.Text;
using static System.Console;

namespace ex_006_002_StringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            WindowWidth += 15;
            WindowHeight = LargestWindowHeight;

            Write("STRING BUILDER : version ");
            BackgroundColor = ConsoleColor.DarkBlue;
            ForegroundColor = ConsoleColor.Yellow;
            Write("mutable");
            ResetColor();
            WriteLine(" de string");

            //construction
            WriteLine("\n CONSTRUCTION");
            WriteLine("à l'aide d'un string : StringBuilder s1 = new StringBuilder(\"Bonjour\");");
            StringBuilder s1 = new StringBuilder("Bonjour");
            WriteLine(s1.ToString());
            WriteLine("à l'aide d'une sous-chaîne d'un string : s1 = new StringBuilder(\"Bonjour\", 0, 3, 3);");
            s1 = new StringBuilder("Bonjour", 0, 3, 3);
            WriteLine(s1);

            //manipuler les string builder
            WriteLine("\n MANIPULER LES STRING BUILDER");
            //append
            WriteLine("APPEND");
            StringBuilder s7 = new StringBuilder().Append("Jim").Append(" ").Append("Raynor");
            WriteLine($"new StringBuilder().Append(\"Jim\").Append(\" \").Append(\"Raynor\"); contient {s7}");
            WriteLine("APPEND FORMAT");
            string format = "Il y a {0} élèves inscrits en {1} année à l'IUT pour l'année scolaire {2}";
            WriteLine("Le format : {0}", format);
            StringBuilder s9 = new StringBuilder().AppendFormat(format, 110, "2ème", "2011-2012");
            WriteLine("new StringBuilder().AppendFormat(format, 110, \"2ème\", \"2011-2012\"); vaut :\n{0}", s9);
            WriteLine();

            //insert, remove, replace
            StringBuilder s8 = new StringBuilder("Jim Raynor");
            WriteLine("StringBuilder s8 = new StringBuilder(\"Jim Raynor\");");
            WriteLine("REMOVE");
            s8.Remove(2, 3);
            WriteLine("s8.Remove(2, 3) vaut {0}", s8);
            WriteLine("INSERT");
            s8.Insert(2, "m R");
            WriteLine("s8.Insert(2, \"m R\") vaut {0}", s8);
            WriteLine("CLEAR");
            s8.Clear();
            WriteLine("s8.Clear()");
            WriteLine("REPLACE");
            WriteLine("s8.Append(\"Les chaussettes de l'archiduchesse\");");
            s8.Append("Les chaussettes de l'archiduchesse");
            s8.Replace(" ", "_");
            WriteLine("s8.Replace(\" \", \"_\") vaut {0}", s8);
        }
    }
}
