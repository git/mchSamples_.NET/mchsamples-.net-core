﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_016_OneToMany_FluentAPI
{
    /// <summary>
    /// Morceau est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-many avec la classe Morceau via la propriété Album.
    /// </summary>
    public class Morceau
    {
        public int MorceauId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public Album Album
        {
            get; set;
        }
    }
}
