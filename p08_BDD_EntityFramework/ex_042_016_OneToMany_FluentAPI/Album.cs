﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_016_OneToMany_FluentAPI
{
    /// <summary>
    /// Album est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-many avec la classe Morceau via la propriété Morceaux.
    /// </summary>
    public class Album
    {
        public int AlbumId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public DateTime DateDeSortie
        {
            get; set;
        }

        public ICollection<Morceau> Morceaux { get; set; } = new List<Morceau>();
    }
}
