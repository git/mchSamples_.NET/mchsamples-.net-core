﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_016_OneToMany_FluentAPI
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Album et Morceau.
    /// </summary>
    public class AlbumDBEntities : DbContext
    {
        public DbSet<Album> Albums { get; set; }
        public DbSet<Morceau> Morceaux { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=ex_042_016_OneToMany_FluentAPI.Albums.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //création de la table Album
            modelBuilder.Entity<Album>().HasKey(a => a.AlbumId); //définition de la clé primaire
            modelBuilder.Entity<Album>().Property(a => a.AlbumId)
                                           .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            //création de la table Morceau
            modelBuilder.Entity<Morceau>().HasKey(m => m.MorceauId); //définition de la clé primaire
            modelBuilder.Entity<Morceau>().Property(m => m.MorceauId)
                                           .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            // Add the shadow property to the model
            modelBuilder.Entity<Morceau>()
                .Property<int>("AlbumForeignKey");

            // Use the shadow property as a foreign key
            modelBuilder.Entity<Morceau>()
                .HasOne(m => m.Album)
                .WithMany(a => a.Morceaux)
                .HasForeignKey("AlbumForeignKey");

            base.OnModelCreating(modelBuilder);
        }
    }
}
