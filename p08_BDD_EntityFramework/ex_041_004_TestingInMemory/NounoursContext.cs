﻿using Microsoft.EntityFrameworkCore;

namespace ex_041_004_TestingInMemory
{
    public class NounoursContext : DbContext
    {
        public DbSet<Nounours> Nounours { get; set; }

        public NounoursContext()
        { }

        public NounoursContext(DbContextOptions<NounoursContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_041_004_TestingInMemory.Nounours.mdf;Trusted_Connection=True;");
            }
        }
    }
}
