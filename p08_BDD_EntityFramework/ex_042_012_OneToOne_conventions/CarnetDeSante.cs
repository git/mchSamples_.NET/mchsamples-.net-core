﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_012_OneToOne_conventions
{
    /// <summary>
    /// CarnetDeSante est une classe POCO, i.e. Plain Old CLR Object
    /// Elle a une relation 1-1 avec la classe Nounours via la propriété Owner.
    /// Notez l'annotation ForeignKey("Nounours") sur la propriété UniqueId qui indique qu'il ne faut pas générer
    ///     une clé primaire pour ses instances, mais utiliser celle du Owner associé
    /// </summary>
    [Table("Carnets")]
    public class CarnetDeSante
    {
        [Key, ForeignKey("Owner")]
        public int UniqueId
        {
            get; set;
        }

        public DateTime LastModified
        {
            get; set;
        }

        public Nounours Owner
        {
            get; set;
        }

        public override string ToString()
        {
            return $"{UniqueId} : carnet de {Owner.Nom}, modifié la dernière fois le {LastModified.ToString("d")}";
        }
    }
}
