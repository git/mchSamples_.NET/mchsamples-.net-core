﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ex_042_012_OneToOne_conventions
{
    class StubbedContext : NounoursDBEntities
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CarnetDeSante>().HasData(
                    new CarnetDeSante { UniqueId=1, LastModified = DateTime.Today },
                    new CarnetDeSante { UniqueId=2, LastModified = new DateTime(1980, 5, 21) },
                    new CarnetDeSante { UniqueId=3, LastModified = new DateTime(1983, 5, 25) }
                );

            modelBuilder.Entity<Nounours>().HasData(
                new Nounours { UniqueId=1, Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 },
                new Nounours { UniqueId=2, Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 },
                new Nounours { UniqueId=3, Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 }
                );
        }
    }
}
