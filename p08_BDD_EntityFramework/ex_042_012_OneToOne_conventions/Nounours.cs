﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_012_OneToOne_conventions
{
    /// <summary>
    /// Nounours est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-1 avec la classe CarnetDeSante via la propriété Carnet.
    /// La clé primaire est générée lors de l'insertion en table (et est utilisée dans la classe CarnetDeSante
    ///     pour faire l'association. cf. CarnetDeSante).
    [Table("TableNounours")]
    public class Nounours
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UniqueId
        {
            get; set;
        }

        public CarnetDeSante Carnet { get; set; }

        [Required]
        [MaxLength(256)]
        public string Nom
        {
            get;
            set;
        }

        [Column("Naissance", TypeName="date")]
        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"{UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
