﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_012_OneToOne_conventions
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Nounours et CarnetDeSante.
    /// </summary>
    public class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
        public DbSet<CarnetDeSante> Carnets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=ex_042_012_OneToOne_conventions.Nounours.db");
        }
    }
}
