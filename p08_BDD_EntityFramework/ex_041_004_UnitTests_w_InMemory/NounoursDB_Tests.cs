using ex_041_004_TestingInMemory;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;

namespace ex_041_004_UnitTests_w_InMemory
{
    public class NounoursDB_Tests
    {
        [Fact]
        public void Add_Test()
        {
            var options = new DbContextOptionsBuilder<NounoursContext>()
                .UseInMemoryDatabase(databaseName: "Add_Test_database")
                .Options;

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                Nounours chewie = new Nounours { Nom = "Chewbacca" };
                Nounours yoda = new Nounours { Nom = "Yoda" };
                Nounours ewok = new Nounours { Nom = "Ewok" };

                context.Nounours.Add(chewie);
                context.Nounours.Add(yoda);
                context.Nounours.Add(ewok);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                Assert.Equal(3, context.Nounours.Count());
                Assert.Equal("Chewbacca", context.Nounours.First().Nom);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            var options = new DbContextOptionsBuilder<NounoursContext>()
                .UseInMemoryDatabase(databaseName: "Modify_Test_database")
                .Options;

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                Nounours chewie = new Nounours { Nom = "Chewbacca" };
                Nounours yoda = new Nounours { Nom = "Yoda" };
                Nounours ewok = new Nounours { Nom = "Ewok" };

                context.Nounours.Add(chewie);
                context.Nounours.Add(yoda);
                context.Nounours.Add(ewok);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                string nameToFind = "ew";
                Assert.Equal(2, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                nameToFind = "ewo";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                var ewok = context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).First();
                ewok.Nom = "Wicket";
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                string nameToFind = "ew";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                nameToFind = "wick";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
            }
        }
    }
}
