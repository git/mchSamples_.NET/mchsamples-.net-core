﻿using Microsoft.Data.SqlClient;
using System;
using System.Linq;
using static System.Console;

namespace ex_042_003_EF_CF_Fluent_API
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };

            try
            {
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    if (db.NounoursSet.Count() > 0)
                    {
                        WriteLine("La base n'est pas vide !");
                        foreach (var n in db.NounoursSet)
                        {
                            WriteLine($"\t{n}");
                        }
                        WriteLine("début du nettoyage...");

                        foreach (var n in db.NounoursSet)
                        {
                            WriteLine($"Suppression de {n}");
                            db.NounoursSet.Remove(n);
                        }

                        WriteLine("Base avant sauvegarde des changements :");
                        foreach (var n in db.NounoursSet)
                        {
                            WriteLine($"\t{n}");
                        }
                        db.SaveChanges();
                        WriteLine("Base après sauvegarde des changements :");
                        foreach (var n in db.NounoursSet)
                        {
                            WriteLine($"\t{n}");
                        }
                    }

                    db.NounoursSet.AddRange(new Nounours[] { chewie, yoda, ewok });

                    db.SaveChanges();
                    WriteLine("Base après ajout des 3 nounours et sauvegarde des changements :");
                    foreach (var n in db.NounoursSet)
                    {
                        WriteLine($"\t{n}");
                    }

                }
            }
            catch (SqlException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
