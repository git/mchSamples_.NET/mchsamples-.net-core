﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using static System.Console;

namespace ex_042_013_OneToOne_FluentAPI
{
    public class Program
    {
        /// <summary>
        /// Cet exemple montre comment construire une relation 1-1 dans la base de données en utilisant la Fluent API d'Entity Framework.
        /// </summary>
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    WriteLine("Contenu de la base (nounours) : ");
                    foreach (var n in db.NounoursSet.Include(n => n.Carnet))
                    {
                        WriteLine($"\t{n}, LastModified: {n.Carnet.LastModified.ToString("d")}");
                    }

                    WriteLine("Contenu de la base (carnets de santé) : ");
                    foreach (var c in db.Carnets)
                    {
                        WriteLine($"\t{c}");
                    }

                    WriteLine("\nAjout d'un nounours et de son carnet de santé\n");
                    
                    Nounours porg = new Nounours { Nom = "Porg", DateDeNaissance = new DateTime(2017, 7, 19), NbPoils = 123 };
                    CarnetDeSante carnetPorg = new CarnetDeSante { LastModified = DateTime.Now, Owner = porg };
                    porg.Carnet = carnetPorg;

                    db.AddRange(porg, carnetPorg);

                    db.SaveChanges();
                }
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    WriteLine("Contenu de la base (nounours) : ");
                    foreach (var n in db.NounoursSet.Include(n => n.Carnet))
                    {
                        WriteLine($"\t{n}, LastModified: {n.Carnet.LastModified.ToString("d")}");
                    }

                    WriteLine("Contenu de la base (carnets de santé) : ");
                    foreach (var c in db.Carnets)
                    {
                        WriteLine($"\t{c}");
                    }
                }
            }
            catch (NotImplementedException exception)
            {
                WriteLine(exception.Message);
            }
            catch (SqliteException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
