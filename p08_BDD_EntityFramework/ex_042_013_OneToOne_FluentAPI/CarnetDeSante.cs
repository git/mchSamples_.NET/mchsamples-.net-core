﻿using System;

namespace ex_042_013_OneToOne_FluentAPI
{
    /// <summary>
    /// CarnetDeSante est une classe POCO, i.e. Plain Old CLR Object
    /// Elle a une relation 1-1 avec la classe Nounours via la propriété Owner.
    /// </summary>
    public class CarnetDeSante
    {
        public int UniqueId
        {
            get; set;
        }

        public DateTime LastModified
        {
            get; set;
        }

        public Nounours Owner
        {
            get; set;
        }

        public override string ToString()
        {
            return $"{UniqueId} : carnet de {Owner.Nom}, modifié la dernière fois le {LastModified.ToString("d")}";
        }
    }
}
