﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_013_OneToOne_FluentAPI
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Nounours et CarnetDeSante.
    /// </summary>
    public class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
        public DbSet<CarnetDeSante> Carnets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=ex_042_013_OneToOne_FluentAPI.Nounours.db");
        }

        /// <summary>
        /// méthode appelée lors de la création du modèle. 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //création de la table TableNounours
            modelBuilder.Entity<Nounours>().ToTable("TableNounours"); //nom de la table
            modelBuilder.Entity<Nounours>().HasKey(n => n.UniqueId); //définition de la clé primaire
            modelBuilder.Entity<Nounours>().Property(n => n.UniqueId)
                                           .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion
            modelBuilder.Entity<Nounours>().Property(n => n.Nom).IsRequired()
                                                                .HasMaxLength(256); //définition de la colonne Nom
            modelBuilder.Entity<Nounours>().Property(n => n.DateDeNaissance).HasColumnName("Naissance").HasColumnType("date"); //changement du nom de la colonne Naissance

            //création de la table "Carnets"
            modelBuilder.Entity<CarnetDeSante>().ToTable("Carnets"); // nom de la table
            modelBuilder.Entity<CarnetDeSante>().HasKey(c => c.UniqueId); //définition de la clé primaire
            modelBuilder.Entity<CarnetDeSante>().Property(c => c.UniqueId)
                                                .ValueGeneratedNever(); // définition du mode de génération de la clé : pas de génération automatique
            //note : la colonne LastModified n'est pas touchée : utilisation des conventions EF


            //on précise qu'il y a une relation entre CarnetDeSante et Nounours
            modelBuilder.Entity<Nounours>() //l'entité Nounours...
                        .HasOne(n => n.Carnet) //a une propriété obligatoire Carnet...
                        .WithOne(c => c.Owner) //reliée à la propriété Owner du Carnet...
                        .HasForeignKey<CarnetDeSante>(c => c.UniqueId);//dont la propriété UniqueId est une Foreign Key
            //remplace la ForeignKey

            base.OnModelCreating(modelBuilder);
        }
    }
}
