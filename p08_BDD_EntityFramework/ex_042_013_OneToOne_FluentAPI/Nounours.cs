﻿using System;

namespace ex_042_013_OneToOne_FluentAPI
{
    /// <summary>
    /// Nounours est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-1 avec la classe CarnetDeSante via la propriété Carnet.
    public class Nounours
    {
        public int UniqueId
        {
            get; set;
        }

        public CarnetDeSante Carnet { get; set; }

        public string Nom
        {
            get;
            set;
        }

        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"{UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
