﻿using System;
using ex_041_004_TestingInMemory;
using Microsoft.EntityFrameworkCore;

namespace ex_041_004_ConsoleTests_w_SQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            Nounours chewie = new Nounours { Nom = "Chewbacca" };
            Nounours yoda = new Nounours { Nom = "Yoda" };
            Nounours ewok = new Nounours { Nom = "Ewok" };

            using (var context = new SQLiteNounoursContext())
            {
                // Crée des nounours et les insère dans la base
                Console.WriteLine("Creates and inserts new Nounours");
                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }

            using (var context = new SQLiteNounoursContext())
            {
                foreach(var n in context.Nounours)
                {
                    Console.WriteLine($"{n.Id} - {n.Nom}");
                }
                context.SaveChanges();
            }
        }
    }

    public class SQLiteNounoursContext : NounoursContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if(!options.IsConfigured)
            {
                options.UseSqlite($"Data Source=ex_041_004_SQLite.Nounours.db");
            }
        }
    }
}
