﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_002_EF_CF_data_annotations
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_002_EF_CF_data_annotations.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
