﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_008_DataSeeding_before_EF2_1
{
    [Table("TableNounours")]
    public class Nounours
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId
        {
            get; set;
        }

        [Required]
        [MaxLength(256)]
        //[Column("name", Order=0, TypeName ="varchar(200)")]
        public string Nom
        {
            get;
            set;
        }

        [Column("Naissance", TypeName = "date")]
        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        [NotMapped]
        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"{UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
