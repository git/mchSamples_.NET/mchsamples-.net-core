﻿using Microsoft.Data.SqlClient;
using System;
using System.Linq;
using static System.Console;

namespace ex_042_008_DataSeeding_before_EF2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    //Nettoyage
                    if (db.NounoursSet.Count() > 0)
                    {
                        WriteLine("nettoyage de la base car elle n'était pas vide");
                        foreach (var n in db.NounoursSet)
                        {
                            db.NounoursSet.Remove(n);
                        }
                        db.SaveChanges();
                    }

                    //Seeding
                    WriteLine("remplissage avec du stub");
                    DataSeeder.SeedData(db);

                    WriteLine("Contenu de la base :");
                    foreach (var n in db.NounoursSet)
                    {
                        WriteLine($"\t{n}");
                    }

                }
            }
            catch (SqlException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
