﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_008_DataSeeding_before_EF2_1
{
    public static class DataSeeder
    {
        public static void SeedData(DbContext context)
        {
            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };

            context.AddRange(new Nounours[] { chewie, yoda, ewok });

            context.SaveChanges();
        }
    }
}
