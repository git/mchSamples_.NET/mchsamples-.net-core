﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_008_DataSeeding_before_EF2_1
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_008_DataSeeding_before_EF2_1.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
