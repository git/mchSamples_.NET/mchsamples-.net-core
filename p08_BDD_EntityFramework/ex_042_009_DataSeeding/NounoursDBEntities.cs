﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_009_DataSeeding
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_009_DataSeeding.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
