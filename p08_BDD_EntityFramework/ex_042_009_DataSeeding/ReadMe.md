﻿# ex_042_009_DataSeeding
*14/01/2020 &sdot; Marc Chevaldonné*  

---

Cet exemple montre comment il est recommandé d'utiliser du Stub avec Entity Framework Core depuis la version 2.1.  

---

## Comment est construit cet exemple ?
* Le projet est de type .NET Core
* Il contient trois classes :
  * ```Nounours```
  * ```NounoursDBEntities```
  * ```NounoursDBEntitiesWithStub```
  
Le contenu des classes ```Nounours``` et ```NounoursDBEntities``` correspond à ce qui a été vu dans les exemples précédents. Seule la classe ```NounoursDBEntitiesWithStub``` sera donc expliquée ici.

### La classe ```NounoursDBEntitiesWithStub```

* ```NounoursDBEntitiesWithStub``` est une classe fille de ```NounoursDBEntites```.
Son rôle est de proposer un Stub en plus de ce que sait déjà faire sa classe mère. Elle ne sera donc utilisée que pour des tests unitaires ou fonctionnels.
En conséquence, elle reprend tout ce que fait sa classe mère et ne change que la méthode ```OnModelCreating``` qui appelle la méthode de la classe mère puis ajoute des instances d'entités, grâce à la méthode d'extension ```HasData```.
```csharp
using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_009_DataSeeding
{
    class NounoursDBEntitiesWithStub : NounoursDBEntities
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Nounours>().HasData(
                new Nounours { UniqueId = Guid.Parse("{4422C524-B2CB-43EF-8263-990C3CEA7CAE}"), Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 },
                new Nounours { UniqueId = Guid.Parse("{A4F84D92-C20F-4F2D-B3F9-CA00EF556E72}"), Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 },
                new Nounours { UniqueId = Guid.Parse("{AE5FE535-F041-445E-B570-28B75BC78CB9}"), Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 }
                );
        }
    }
}
```
* __Note importante__ : remarquez que la création des instances d'entités donne aussi l'```UniqueId``` puisqu'il ne s'agit pas d'un ajout "classique" dans la base mais la table est créée avec ces instances.
* __Note importante__ : l'utilisation de ```UniqueId``` vous permettra d'ajouter des entités liées dans le Stub.
* __Explication__ : l'utilisation de ```HasData``` dans ```OnModelCreating``` fait que vos données stubbées feront parties de la migration : rien à voir avec un ajout depuis votre appli consommatrice.
* __Note__ : souvent, on en profite pour réécrire également ```OnConfiguring``` afin de changer de fournisseur (pour prendre par exemple un *SQLite in memory* puisque ce contexte stubbé est voué à être utilisé par des tests).
  
* Elle est ensuite utilisée dans ```Program``` pour remplir la base de manière tout à fait classique et ne nécessite aucun appel supplémentaire.
```csharp
using (NounoursDBEntities db = new NounoursDBEntitiesWithStub())
{
    WriteLine("Contenu de la base :");
    foreach (var n in db.NounoursSet)
    {
        WriteLine($"\t{n}");
    }
}
```

## Comment exécuter cet exemple ?
Pour tester cette application, n'oubliez pas les commandes comme présentées dans l'exemple ex_041_001 : pour générer l'exemple, il vous faut d'abord préparer les migrations et les tables.
  * Ouvrez la *Console du Gestionnaire de package*, pour cela, dirigez-vous dans le menu *Outils*, puis *Gestionnaire de package NuGet*, puis *Console du Gestionnaire de package*.
  * Dans la console que vous venez d'ouvrir, déplacez-vous dans le dossier du projet .NET Core, ici :
```
cd .\p08_BDD_EntityFramework\ex_042_009_DataSeeding
``` 
  *Note*:
  si vous n'avez pas installé correctement EntityFrameworkCore, il vous faudra peut-être utiliser également :

* ```dotnet tool install --global dotnet-ef``` si vous utilisez la dernière version de .NET Core (3.1 aujourd'hui),  

* ```dotnet tool install --global dotnet-ef --version 3.0.0``` si vous vous utiliser spécifiquement .NET Core 3.0.


  * Migration : 
```
dotnet ef migrations add migration_ex_042_009 --context NounoursDBEntitiesWithStub
```
  * Création de la table :
```
dotnet ef database update --context NounoursDBEntitiesWithStub
```
  * Génération et exécution
Vous pouvez maintenant générer et exécuter l'exemple **ex_042_009_DataSeeding**.

  * Le résultat de l'exécution va ressembler à :
  ```
Contenu de la base :
        ae5fe535-f041-445e-b570-28b75bc78cb9: Ewok (25/05/1983, 3456789 poils)
        4422c524-b2cb-43ef-8263-990c3cea7cae: Chewbacca (27/05/1977, 1234567 poils)
        a4f84d92-c20f-4f2d-b3f9-ca00ef556e72: Yoda (21/05/1980, 3 poils)
```

  * Comment vérifier le contenu des bases de données SQL Server ?
Vous pouvez vérifier le contenu de votre base en utilisant l'*Explorateur d'objets SQL Server*.
* Pour cela, allez dans le menu *Affichage* puis *Explorateur d'objets SQL Server*.  
<img src="../ex_041_001_ConnectionStrings/readmefiles/sqlserver_01.png" width="500"/>  

* Déployez dans l'*Explorateur d'objets SQL Server* :
  *  *SQL Server*, 
  *  puis *(localdb)\MSSQLLocalDB ...*, 
  *  puis *Bases de données*
  *  puis celle portant le nom de votre migration, dans mon cas : *ex_042_009_DataSeeding.Nounours.mdf*
  *  puis *Tables*   
  *  Faites un clic droit sur la table *dbo.Nounours* puis choisissez *Afficher les données*  
<img src="../ex_041_001_ConnectionStrings/readmefiles/sqlserver_02.png" width="460"/>  

  *  Vous devriez maintenant pouvoir voir les données suivantes dans le tableau :  
 
  |UniqueId   |Nom|Naissance|NbPoils
  |---|---|---|---
  |ae5fe535-f041-445e-b570-28b75bc78cb9|Ewok|25/05/1983|3456789 
  |4422c524-b2cb-43ef-8263-990c3cea7cae|Chewbacca|27/05/1977|1234567
  |a4f84d92-c20f-4f2d-b3f9-ca00ef556e72|Yoda|21/05/1980|3

