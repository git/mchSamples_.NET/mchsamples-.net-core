﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_015_OneToMany_conventions
{
    /// <summary>
    /// Album est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-many avec la classe Morceau via la propriété Morceaux.
    /// La clé primaire est générée lors de l'insertion en table.
    /// </summary>
    public class Album
    {
        public int AlbumId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public DateTime DateDeSortie
        {
            get; set;
        }

        public ICollection<Morceau> Morceaux { get; set; } = new List<Morceau>();
    }
}
