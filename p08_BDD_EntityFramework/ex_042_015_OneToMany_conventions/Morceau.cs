﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_015_OneToMany_conventions
{
    /// <summary>
    /// Morceau est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-many avec la classe Morceau via la propriété Album.
    /// La clé primaire est générée lors de l'insertion en table.
    /// </summary>
    public class Morceau
    {
        public int MorceauId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        public Album Album
        {
            get; set;
        }
    }
}
