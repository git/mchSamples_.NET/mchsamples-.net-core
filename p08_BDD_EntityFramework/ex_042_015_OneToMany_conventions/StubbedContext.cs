﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ex_042_015_OneToMany_conventions
{
    class StubbedContext : AlbumDBEntities
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Album kindofblue = new Album { AlbumId=1, Titre = "Kind of Blue", DateDeSortie = new DateTime(1959, 8, 17) };
            Album dialogue = new Album { AlbumId=2, Titre = "Dialogue", DateDeSortie = new DateTime(1965, 9, 1) };

            modelBuilder.Entity<Album>().HasData(kindofblue, dialogue);

            modelBuilder.Entity<Morceau>().Property<int>("AlbumId");

            modelBuilder.Entity<Morceau>().HasData(new { MorceauId = 1, AlbumId = 1, Titre = "So What" },
                                             new { MorceauId = 2, AlbumId = 1, Titre = "Freddie Freeloader" },
                                             new { MorceauId = 3, AlbumId = 1, Titre = "Blue in Green" },
                                             new { MorceauId = 4, AlbumId = 1, Titre = "All Blues" },
                                             new { MorceauId = 5, AlbumId = 1, Titre = "Flamenco Sketches" },
                                             new { MorceauId = 6, AlbumId = 2, Titre = "Catta" },
                                             new { MorceauId = 7, AlbumId = 2, Titre = "Idle While" },
                                             new { MorceauId = 8, AlbumId = 2, Titre = "Les Noirs Marchant" },
                                             new { MorceauId = 9, AlbumId = 2, Titre = "Dialogue" },
                                             new { MorceauId = 10, AlbumId = 2, Titre = "Ghetto Lights" },
                                             new { MorceauId = 11, AlbumId = 2, Titre = "Jasper" }
                );
        }
    }
}
