﻿# ex_042_004_Keys_conventions
*06/01/2020 &sdot; Marc Chevaldonné*  

---

Cet exemple traite des clés primaires associées aux entités.

Prérequis : je n'explique pas à travers cet exemple les principes de base d'**Entity Framework Core** et en particulier les chaînes de connexion et le lien entre entité et table.
Pour plus de renseignements sur :
* les chaînes de connexion : *ex_041_001_ConnectionStrings*
* les liens entre entités et tables : *ex_042_001_EF_CF_conventions*, *ex_042_002_EF_CF_data_annotations* et *ex_042_003_EF_CF_Fluent_API*

Cet exemple montre le cas particulier de la gestion des clés primaires lors de l'utilisation des **conventions d'écriture**.  
Vous pourrez trouver une version plus ou moins équivalente avec les *data annotations* ici : **ex_042_005_Keys_data_annotations**.  
Vous pourrez trouver une version plus ou moins équivalente avec la *Fluent API* ici : **ex_042_006_Keys_FluentAPI**.  

---

## Les clés primaires
Une clé permet de rendre unique chaque instance d'une entité. La plupart des entités n'ont qu'une seule clé qui est alors transformée en *clé primaire* pour les bases de données relationnelles.  
*Note: une entité peut avoir d'autres clés, on parle d'__alternate keys__. Elles seront présentées dans les exemples sur les relations entre entités.*  
Si on utilise les *conventions d'écriture*, une propriété pour être transformée en clé doit respecter les contraintes suivantes :
* elle doit être nommée ```Id``` ou ```ID```,
* elle doit être nommée ```<typeDeLEntite>Id```, e.g. ```NounoursId```.

Les autres contraintes sur une clé dans le cas de l'utilisation des *conventions d'écriture* sont :
* elle doit être de type ```int```, ```string```, ```byte[]```. Toutefois, certains types nécessitent l'utilisation de converteurs pour être utilisés avec certains fournisseurs. Je conseille donc l'utilisation de ```int``` qui marche avec la grande majorité des fournisseurs.
* elle est générée lors de l'insertion en base.

Les autres modes (*data annotations* et *Fluent API*) offrent plus de solutions quant à la gestion des clés.

## La classe ```Nounours```
La classe ```Nounours``` utilise les conventions d'écriture.
* Par défaut, les propriétés utilisées comme clés primaires sont en mode **Generated on add**. 
Une nouvelle valeur est donc générée lors de l'insertion d'une nouvelle entité en base. Les valeurs des autres propriétés ne sont pas générées lors de l'insertion ou de la mise à jour. 
* Dans cette classe, j'ai respecté la contrainte de nommage qui propose ```Id``` ou ```ID```
```csharp
public int ID
{
    get; set;
}
```

## La classe ```Cylon```
La classe ```Cylon``` utilise les conventions d'écriture.
* Dans cette classe, j'ai respecté la contrainte de nommage qui propose ```<TypeDeLEntité>Id```
```csharp
public int CylonId
{
    get; set;
}
```

### La classe ```Program```
Cette classe est le point d'entrée du programme :
* Elle crée des instances de ```Nounours``` et de ```Cylon``` et les ajoute en base après avoir nettoyé les tables au préalables. 
* Elle affiche les ```Nounours``` et les ```Cylon```.
*Notez la génération des identifiants.*

## Comment exécuter cet exemple ?
Pour tester cette application, n'oubliez pas les commandes comme présentées dans l'exemple ex_041_001 : pour générer l'exemple, il vous faut d'abord préparer les migrations et les tables.
  * Ouvrez la *Console du Gestionnaire de package*, pour cela, dirigez-vous dans le menu *Outils*, puis *Gestionnaire de package NuGet*, puis *Console du Gestionnaire de package*.
  * Dans la console que vous venez d'ouvrir, déplacez-vous dans le dossier du projet .NET Core, ici :
```
cd .\p08_BDD_EntityFramework\ex_042_004_Keys_conventions
``` 
  *Note*:
  si vous n'avez pas installé correctement EntityFrameworkCore, il vous faudra peut-être utiliser également :

* ```dotnet tool install --global dotnet-ef``` si vous utilisez la dernière version de .NET Core (3.1 aujourd'hui),  

* ```dotnet tool install --global dotnet-ef --version 3.0.0``` si vous vous utiliser spécifiquement .NET Core 3.0.


  * Migration : 
```
dotnet ef migrations add migration_ex_042_004
```
  * Création de la table :
```
dotnet ef database update
```
  * Génération et exécution
Vous pouvez maintenant générer et exécuter l'exemple **ex_042_004_Keys_conventions**.

  * Comment vérifier le contenu des bases de données SQL Server ?
Vous pouvez vérifier le contenu de votre base en utilisant l'*Explorateur d'objets SQL Server*.
* Pour cela, allez dans le menu *Affichage* puis *Explorateur d'objets SQL Server*.  
<img src="../ex_041_001_ConnectionStrings/readmefiles/sqlserver_01.png" width="500"/>  

* Déployez dans l'*Explorateur d'objets SQL Server* :
  *  *SQL Server*, 
  *  puis *(localdb)\MSSQLLocalDB ...*, 
  *  puis *Bases de données*
  *  puis celle portant le nom de votre migration, dans mon cas : *ex_042_004_Keys_conventions.Nounours.mdf*
  *  puis *Tables*   
  *  Faites un clic droit sur la table *dbo.Nounours* puis choisissez *Afficher les données*  
<img src="../ex_041_001_ConnectionStrings/readmefiles/sqlserver_02.png" width="460"/>  

* Le résultat de l'exécution peut être :
```
database after cleaning and adding 3 Nounours and 9 Cylons and saving changes :
        Nounours 1: Chewbacca (27/05/1977, 1234567 poils)
        Nounours 2: Yoda (21/05/1980, 3 poils)
        Nounours 3: Ewok (25/05/1983, 3456789 poils)
        Cylon 1: John Cavil, Number 1
        Cylon 2: Leoben Conoy, Number 2
        Cylon 3: D'Anna Biers, Number 3
        Cylon 4: Simon, Number 4
        Cylon 5: Aaron Doral, Number 5
        Cylon 6: Caprica 6, Number 6
        Cylon 7: Daniel, Number 7
        Cylon 8: Boomer, Number 8
        Cylon 9: Athena, Number 8
```
*Note: les identifiants peuvent varier en fonction du nombre d'exécution de l'exemple depuis la création de la base de données.*