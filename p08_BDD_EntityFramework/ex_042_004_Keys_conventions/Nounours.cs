﻿using System;

namespace ex_042_004_Keys_conventions
{
    public class Nounours
    {
        public int ID
        {
            get; set;
        }

        public string Nom
        {
            get;
            set;
        }

        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"Nounours {ID}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
