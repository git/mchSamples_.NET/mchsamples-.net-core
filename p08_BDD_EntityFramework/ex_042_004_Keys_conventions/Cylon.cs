﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ex_042_004_Keys_conventions
{
    public class Cylon
    {
        public int CylonId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Generation
        {
            get; set;
        }

        public override string ToString()
        {
            return $"Cylon {CylonId}: {Name}, Number {Generation}";
        }
    }
}
