﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_004_Keys_conventions
{
    class DBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
        public DbSet<Cylon> CylonsSet { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_004_Keys_conventions.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
