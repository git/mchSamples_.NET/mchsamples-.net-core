﻿using Microsoft.EntityFrameworkCore;

namespace ex_041_001_ConnectionStrings
{
    class SqlServerContext : DbContext
    {
        public DbSet<Nounours> Nounours { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_041_001_ConnectionStrings.Nounours.mdf;Trusted_Connection=True;");
    }
}
