﻿using System;
using System.Runtime.InteropServices;

namespace ex_041_001_ConnectionStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Nounours chewie = new Nounours { Nom = "Chewbacca" };
            Nounours yoda = new Nounours { Nom = "Yoda" };
            Nounours ewok = new Nounours { Nom = "Ewok" };

            if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                using (var context = new SqlServerContext())
                {
                    // Crée des nounours et les insère dans la base
                    Console.WriteLine("Creates and inserts new Nounours with SqlServer");
                    context.Add(chewie);
                    context.Add(yoda);
                    context.Add(ewok);
                    context.SaveChanges();
                }

                using (var context = new SqlServerContext())
                {
                    foreach(var n in context.Nounours)
                    {
                        Console.WriteLine($"{n.Id} - {n.Nom}");
                    }
                    context.SaveChanges();
                }   
            }

            using (var context = new SQLiteContext())
            {
                // Crée des nounours et les insère dans la base
                Console.WriteLine("Creates and inserts new Nounours with SQLite");
                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }

            using (var context = new SQLiteContext())
            {
                foreach(var n in context.Nounours)
                {
                    Console.WriteLine($"{n.Id} - {n.Nom}");
                }
                context.SaveChanges();
            }
        }
    }
}
