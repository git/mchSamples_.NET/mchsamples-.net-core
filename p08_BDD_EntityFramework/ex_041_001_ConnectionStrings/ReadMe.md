﻿# ex_041_001_ConnectionStrings
*31/12/2019 &sdot; Marc Chevaldonné*
*Dernière mise à jour : 09/01/2020 &sdot; Marc Chevaldonné* 

---

Cet exemple a pour but de présenter les chaîne de connexion (*connection strings*).
* Les *connection strings* servent à se connecter à une base de données.
* Elles diffèrent en fonction des *providers*
* Parfois, elles nécessitent des informations telles que un nom d'utilisateur et un mot de passe qu'on peut vouloir cacher

Dans cet exemple, j'ai voulu montrer deux *connection strings* : une pour SQL Server et une autre pour SQLite.
  
---
## Configuration
Il faut penser à ajouter les NuGet suivants :
* Microsoft.EntityFrameworkCore : pour le projet en général
* Microsoft.EntityFrameworkCore.SqlServer : pour le *provider* SQL Server
* Microsoft.EntityFrameworkCore.Sqlite : pour le *provider* SQLite 
* Microsoft.EntityFrameworkCore.Tools : pour bénéficier des outils de Design, de migrations, etc.


De plus, pour SQLite, il faut penser également à rajouter 
dans l'exemple :
* le chemin du *starting working directory* : on peut le faire par exemple en modifiant le **.csproj** en ajoutant la ligne suivante :
```xml
<StartWorkingDirectory>$(MSBuildProjectDirectory)</StartWorkingDirectory>
```

## Comment fonctionne l'exemple ?
Ce projet contient les classes suivantes :
* ```Nounours``` : elle est la classe du modèle que j'ai faite la plus simple possible. J'expliquerai dans un exemple ultérieure son mode de fonctionnement. Elle contient un ```Id```et un ```Nom``` qui donneront deux colonnes dans une table Nounours avec les mêmes noms.
* ```SqlServerContext``` : première classe qui dérive de ```DbContext``` et qui va permettre de réaliser la connexion avec la base de données de type MSSqlServer
* ```SQLiteContext``` : deuxième classe qui dérive de ```DbContext```et qui va permettre de réaliser la connexion avec la base de données de type SQLite.  

Dans les deux classes qui dérivent de ```DbContext```, on doit donner une *connection string*. Celle-ci est donnée via la méthode protégée et virtuelle ```OnConfiguring```, 
via l'instance de ```DbContextOptionsBuilder```, à travers l'une des méthodes d'extension :
* ```UseSqlServer``` : pour SqlServer, où on peut voir une *connection string* plus ou moins complexe indiquant qu'elle est en local (```Server=(localdb)\mssqllocaldb;```), ainsi que le nom de la base de données (```Database=ex_041_001_ConnectionStrings.Nounours.mdf;```)
* ```UseSqlite``` : pour SQLite, où on peut voir le nom de la base de données ```Data Source=ex_041_001_ConnectionStrings.Nounours.db``` qui sera placée par défaut dans le dossier du projet si vous l'exécutez depuis Visual Studio.
  
C'est tout ce que cet exemple souhaite mettre en valeur : les chaînes de connexion.

## Comment générer et exécuter l'exemple ?
Pour générer l'exemple, il vous faut d'abord préparer les migrations et les tables.

* (__Windows__ Visual Studio 2019) : ouvrez la *Console du Gestionnaire de package*, pour cela, dirigez-vous dans le menu *Outils*, puis *Gestionnaire de package NuGet*, puis *Console du Gestionnaire de package*.
* (__MacOSX__ Visual Studio 2019 For Mac) : ouvrez un Terminal.
* Dans la console que vous venez d'ouvrir, déplacez-vous dans le dossier du projet, ici :
```
cd .\p08_BDD_EntityFramework\ex_041_001_ConnectionStrings
```
  
*Note*:
si vous n'avez pas installé correctement EntityFrameworkCore, il vous faudra peut-être utiliser également :
   
* ```dotnet tool install --global dotnet-ef``` si vous utilisez la dernière version de .NET Core (3.1 aujourd'hui),  

* ```dotnet tool install --global dotnet-ef --version 3.0.0``` si vous vous utiliser spécifiquement .NET Core 3.0.


### Migrations
*Note :* normalement, la commande pour effectuer une migration est :
```
dotnet ef migrations add monNomDeMigration
```
mais comme ici, nous sommes dans le cas particulier où nous avons deux contextes, nous devons préciser les noms des ```DbContext```à migrer :
```
dotnet ef migrations add ex_041_001_SqlServer --context SqlServerContext
dotnet ef migrations add ex_041_001_SQLite --context SQLiteContext
```
*Note : sous MacOSX, n'utilisez que SQLite, soit :*
```
dotnet ef migrations add ex_041_001_SQLite --context SQLiteContext
```
### Création des tables
Tapez ensuite les commandes suivantes :
```
dotnet ef database update --context SqlServerContext
dotnet ef database update --context SQLiteContext
```
*Note : sous MacOSX, n'utilisez que SQLite, soit :*
```
dotnet ef database update --context SQLiteContext
```
### Génération et exécution
Vous pouvez maintenant générer et exécuter l'exemple.

Le résultat de l'exécution peut donner :
* sur Windows :
```
Creates and inserts new Nounours with SqlServer
1 - Chewbacca
2 - Yoda
3 - Ewok
Creates and inserts new Nounours with SQLite
1 - Chewbacca
2 - Yoda
3 - Ewok
```
* sur MacOSX :
```
Creates and inserts new Nounours with SQLite
1 - Chewbacca
2 - Yoda
3 - Ewok
```

## Comment vérifier le contenu des bases de données SQL Server et SQLite ?
### SqlServer (seulement sur Windows)
Vous pouvez vérifier le contenu de votre base en utilisant l'*Explorateur d'objets SQL Server*.
* Pour cela, allez dans le menu *Affichage* puis *Explorateur d'objets SQL Server*.  
<img src="./readmefiles/sqlserver_01.png" width="500"/>  

* Déployez dans l'*Explorateur d'objets SQL Server* :
  *  *SQL Server*, 
  *  puis *(localdb)\MSSQLLocalDB ...*, 
  *  puis *Bases de données*
  *  puis celle portant le nom de votre migration, dans mon cas : *myFirstDatabase.Nounours.mdf*
  *  puis *Tables*   
  *  Faites un clic droit sur la table *dbo.Nounours* puis choisissez *Afficher les données*  
<img src="./readmefiles/sqlserver_02.png" width="460"/>  

  *  Vous devriez maintenant pouvoir voir les données suivantes dans le tableau :  
 
  |Id   |Nom
  |---|---
  |1|Chewbacca
  |2|Yoda
  |3|Ewok

Notez qu'il est également possible d'utiliser l'*Explorateur d'objets SQL Server* pour ajouter, modifier ou supprimer des données dans les tables.

### SQLite (Windows et MacOSX)
Pour vérifier le contenu de votre base SQLite, vous pouvez utiliser le programme *DB Browser* :
* Rendez-vous sur la page : https://sqlitebrowser.org/dl/ et téléchargez le programme *DB Browser*.
* Lancez *DB Browser for SQLite*
* Glissez-déposez au milieu de la fenêtre de *DB Browser for SQLite* le fichier *ex_041_001_ConnectionStrings.Nounours.db* qui a été généré par l'exécution du programme et qui se trouve près de *ex_041_001_ConnectionStrings.csproj*.
![DB Browser for SQLite](./readmefiles/dbbrowser_01.png)
* Choisissez l'onglet *Parcourir les données*
* Observez les résultats obtenus
![DB Browser for SQLite](./readmefiles/dbbrowser_02.png)

  
  
  
---
Copyright &copy; 2019-2020 Marc Chevaldonné