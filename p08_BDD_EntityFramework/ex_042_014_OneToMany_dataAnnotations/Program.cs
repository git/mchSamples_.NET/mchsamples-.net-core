﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using static System.Console;

namespace ex_042_014_OneToMany_dataAnnotations
{
    class Program
    {
        /// <summary>
        /// Cet exemple montre comment construire une relation 1-many dans la base de données en utilisant les conventions de nommage Entity Framework.
        /// 
        /// On affiche les Albums et les Morceaux.
        /// Constatez que les identifiants sont bien les mêmes à cause de la relation 1-many.
        /// 
        /// Si vous ouvrez la base de données (via l'explorateur d'objets SQL Server), vous pourrez constater que la table Morceaux 
        ///     contient une colonne Album_UniqueId qui permet d'assurer la relation 1-many.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                using (AlbumDBEntities db = new AlbumDBEntities())
                {
                    WriteLine("Albums : ");
                    foreach (var a in db.Albums.Include(a => a.Morceaux))
                    {
                        WriteLine($"\t{a.AlbumId}: {a.Titre} (sorti le : {a.DateDeSortie.ToString("d")})");
                        foreach (var m in a.Morceaux)
                        {
                            WriteLine($"\t\t{m.Titre}");
                        }
                    }

                    WriteLine();

                    WriteLine("Morceaux :");
                    foreach (var m in db.Morceaux)
                    {
                        WriteLine($"\t{m.MorceauId}: {m.Titre} (album : {m.Album.Titre})");
                    }

                    WriteLine("\nAjout d'un album et 6 morceaux...\n");

                    Album captainMarvel = new Album { Titre = "Captain Marvel", DateDeSortie = new DateTime(1972, 3, 3) };
                    Morceau[] morceaux = {  new Morceau { Titre = "La Fiesta", Album = captainMarvel },
                                            new Morceau { Titre = "Five Hundred Miles High", Album = captainMarvel },
                                            new Morceau { Titre = "Captain Marvel", Album = captainMarvel },
                                            new Morceau { Titre = "Time's Lie", Album = captainMarvel },
                                            new Morceau { Titre = "Lush Life", Album = captainMarvel },
                                            new Morceau { Titre = "Day Waves", Album = captainMarvel }
                                        };
                    foreach (var m in morceaux)
                    {
                        captainMarvel.Morceaux.Add(m);
                    }

                    db.Add(captainMarvel);
                    db.SaveChanges();
                }
                using (AlbumDBEntities db = new AlbumDBEntities())
                {
                    WriteLine("Albums : ");
                    foreach (var a in db.Albums.Include(a => a.Morceaux))
                    {
                        WriteLine($"\t{a.AlbumId}: {a.Titre} (sorti le : {a.DateDeSortie.ToString("d")})");
                        foreach (var m in a.Morceaux)
                        {
                            WriteLine($"\t\t{m.Titre}");
                        }
                    }

                    WriteLine();

                    WriteLine("Morceaux :");
                    foreach (var m in db.Morceaux)
                    {
                        WriteLine($"\t{m.MorceauId}: {m.Titre} (album : {m.Album.Titre})");
                    }
                }
            }
            catch (NotImplementedException exception)
            {
                WriteLine(exception.Message);
            }
            catch (SqliteException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
