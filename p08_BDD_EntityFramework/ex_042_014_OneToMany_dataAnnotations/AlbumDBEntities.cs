﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_014_OneToMany_dataAnnotations
{
    /// <summary>
    /// La classe qui dérive de DbContext est celle qui permettra de faire les opérations CRUD sur le modèle.
    /// Cette classe contient deux DbSet<T> pour permettre de réaliser des opérations CRUD sur les types T, ici Album et Morceau.
    /// </summary>
    public class AlbumDBEntities : DbContext
    {
        public DbSet<Album> Albums { get; set; }
        public DbSet<Morceau> Morceaux { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=ex_042_014_OneToMany_dataAnnotations.Albums.db");
        }
    }
}
