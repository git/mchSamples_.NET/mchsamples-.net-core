﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ex_042_014_OneToMany_dataAnnotations
{
    class StubbedContext : AlbumDBEntities
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Album kindofblue = new Album { AlbumId=1, Titre = "Kind of Blue", DateDeSortie = new DateTime(1959, 8, 17) };
            Album dialogue = new Album { AlbumId=2, Titre = "Dialogue", DateDeSortie = new DateTime(1965, 9, 1) };

            modelBuilder.Entity<Album>().HasData(kindofblue, dialogue);

            modelBuilder.Entity<Morceau>().HasData(new Morceau { MorceauId = 1, AlbumForeignKey = 1, Titre = "So What" },
                                             new Morceau { MorceauId = 2, AlbumForeignKey = 1, Titre = "Freddie Freeloader" },
                                             new Morceau { MorceauId = 3, AlbumForeignKey = 1, Titre = "Blue in Green" },
                                             new Morceau { MorceauId = 4, AlbumForeignKey = 1, Titre = "All Blues" },
                                             new Morceau { MorceauId = 5, AlbumForeignKey = 1, Titre = "Flamenco Sketches" },
                                             new Morceau { MorceauId = 6, AlbumForeignKey = 2, Titre = "Catta" },
                                             new Morceau { MorceauId = 7, AlbumForeignKey = 2, Titre = "Idle While" },
                                             new Morceau { MorceauId = 8, AlbumForeignKey = 2, Titre = "Les Noirs Marchant" },
                                             new Morceau { MorceauId = 9, AlbumForeignKey = 2, Titre = "Dialogue" },
                                             new Morceau { MorceauId = 10, AlbumForeignKey = 2, Titre = "Ghetto Lights" },
                                             new Morceau { MorceauId = 11, AlbumForeignKey = 2, Titre = "Jasper" }
                );
        }
    }
}
