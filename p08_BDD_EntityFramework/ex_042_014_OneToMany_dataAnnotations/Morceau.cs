﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_014_OneToMany_dataAnnotations
{
    /// <summary>
    /// Morceau est une classe POCO, i.e. Plain Old CLR Object.
    /// Elle a une relation 1-many avec la classe Morceau via la propriété Album.
    /// La clé primaire est générée lors de l'insertion en table.
    /// </summary>
    [Table("Morceaux")]
    public class Morceau
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MorceauId
        {
            get; set;
        }

        public string Titre
        {
            get; set;
        }

        [ForeignKey("AlbumForeignKey")]
        public Album Album
        {
            get; set;
        }

        public int AlbumForeignKey
        {
            get; set;
        }
    }
}
