﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ex_042_006_Keys_FluentAPI
{
    class Ordinateur
    {
        public string Modele
        {
            get; set;
        }

        public string CodeId
        {
            get; set;
        }

        public int Année
        {
            get; set;
        }

        public override string ToString()
        {
            return $"Computer {CodeId} ({Modele}, {Année})";
        }
    }
}
