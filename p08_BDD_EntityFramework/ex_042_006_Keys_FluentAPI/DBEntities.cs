﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_006_Keys_FluentAPI
{
    class DBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
        public DbSet<Cylon> CylonsSet { get; set; }
        public DbSet<Ordinateur> Ordinateurs { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_006_Keys_FluentAPI.Nounours.mdf;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //définition de la clé primaire de Nounours
            modelBuilder.Entity<Nounours>().HasKey(n => n.UniqueId);
            //définition du mode de génération de la clé : génération à l'insertion
            modelBuilder.Entity<Nounours>().Property(n => n.UniqueId).ValueGeneratedOnAdd();

            //définition de la clé primaire de Cylon
            modelBuilder.Entity<Cylon>().HasKey(c => c.FrakId);
            //définition du mode de génération de la clé : génération gérée par l'utilisateur (jamais par la base)
            modelBuilder.Entity<Cylon>().Property(c => c.FrakId).ValueGeneratedNever();

            //définition d'une clé primaire composite pour Ordinateur
            modelBuilder.Entity<Ordinateur>().HasKey(o => new { o.CodeId, o.Modele });
            //une clé composite ne peut pas être générée par la base

            base.OnModelCreating(modelBuilder);
        }
    }
}
