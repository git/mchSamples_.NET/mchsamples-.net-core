﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ex_042_006_Keys_FluentAPI
{
    public class Cylon
    {
        public int FrakId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Generation
        {
            get; set;
        }

        public override string ToString()
        {
            return $"Cylon {FrakId}: {Name}, Number {Generation}";
        }
    }
}
