﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_011_SinglePropertyNavigation_FluentAPI
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Nounours>().HasKey(n => n.UniqueId);
            modelBuilder.Entity<Nounours>().Property(n => n.UniqueId).ValueGeneratedOnAdd();

            modelBuilder.Entity<Nounours>().Property(n => n.DateDeNaissance).HasColumnName("Naissance").HasColumnType("date");

            modelBuilder.Entity<Nounours>().HasOne(n => n.Information);

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlite($"Data Source=ex_042_011_SinglePropertyNavigation_FluentAPI.Nounours.db");
        }
    }
}
