﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_010_SinglePropertyNavigation_conventions
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlite($"Data Source=ex_042_010_SinglePropertyNavigation_conventions.Nounours.db");
        }
    }
}
