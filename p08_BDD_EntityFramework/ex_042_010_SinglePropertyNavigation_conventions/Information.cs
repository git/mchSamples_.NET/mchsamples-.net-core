﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ex_042_010_SinglePropertyNavigation_conventions
{
    public class Information
    {
        public int InformationId { get; set; }

        public string MadeBy { get; set; }

        public string MadeIn { get; set; } 
    }
}
