﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using static System.Console;

namespace ex_042_010_SinglePropertyNavigation_conventions
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    WriteLine("Contenu de la base :");
                    foreach (var n in db.NounoursSet.Include(n => n.Information))
                    {
                        WriteLine($"\t{n}");
                    }

                    db.NounoursSet.Add(new Nounours { Nom = "Porg", DateDeNaissance = new DateTime(2017, 07, 19), NbPoils = 123, Information = new Information { MadeIn = "Ahch-To", MadeBy = "Jake Lunt Davies" } });
                    db.SaveChangesAsync();
                }

                WriteLine("\nAjout d'un nouveau nounours...\n");

                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    WriteLine("Contenu de la base :");
                    foreach (var n in db.NounoursSet.Include(n => n.Information))
                    {
                        WriteLine($"\t{n}");
                    }
                }
            }
            catch (SqliteException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
