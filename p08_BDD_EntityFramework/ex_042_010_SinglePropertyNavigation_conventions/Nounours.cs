﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_010_SinglePropertyNavigation_conventions
{
    public class Nounours
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId
        {
            get; set;
        }

        public string Nom
        {
            get;
            set;
        }

        [Column("Naissance", TypeName = "date")]
        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public Information Information
        {
            get; set;
        }

        //public int InformationId
        //{ get; set; }

        public override string ToString()
        {
            return $"{UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils, fait à {Information.MadeIn} par {Information.MadeBy})";
        }

    }
}
