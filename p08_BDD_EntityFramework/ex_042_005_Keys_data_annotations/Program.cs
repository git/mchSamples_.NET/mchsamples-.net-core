﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using static System.Console;

namespace ex_042_005_Keys_data_annotations
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };

            Cylon c1 = new Cylon { FrakId = 2, Name = "John Cavil", Generation = 1 };
            Cylon c2 = new Cylon { FrakId = 4, Name = "Leoben Conoy", Generation = 2 };
            Cylon c3 = new Cylon { FrakId = 6, Name = "D'Anna Biers", Generation = 3 };
            Cylon c4 = new Cylon { FrakId = 8, Name = "Simon", Generation = 4 };
            Cylon c5 = new Cylon { FrakId = 10, Name = "Aaron Doral", Generation = 5 };
            Cylon c6 = new Cylon { FrakId = 12, Name = "Caprica 6", Generation = 6 };
            Cylon c7 = new Cylon { FrakId = 14, Name = "Daniel", Generation = 7 };
            Cylon c8 = new Cylon { FrakId = 16, Name = "Boomer", Generation = 8 };
            Cylon c9 = new Cylon { FrakId = 17, Name = "Athena", Generation = 8 };
            
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    //nettoyage de la base de données
                    if (db.NounoursSet.Count() > 0)
                    {
                        foreach (var n in db.NounoursSet)
                        {
                            db.NounoursSet.Remove(n);
                        }
                        db.SaveChanges();
                    }

                    if (db.CylonsSet.Count() > 0)
                    {
                        foreach (var c in db.CylonsSet)
                        {
                            db.CylonsSet.Remove(c);
                        }
                        db.SaveChanges();
                    }

                    //ajout des nounours dans la base de données
                    db.NounoursSet.AddRange(new Nounours[] { chewie, yoda, ewok });
                    db.CylonsSet.AddRange(new Cylon[] { c1, c2, c3, c4, c5, c6, c7, c8, c9 });
                    db.SaveChanges();
                }
                using (DBEntities db = new DBEntities())
                {
                    WriteLine("database after cleaning and adding 3 Nounours and 9 Cylons and saving changes :");
                    foreach (var n in db.NounoursSet)
                    {
                        WriteLine($"\t{n}");
                    }
                    foreach (var c in db.CylonsSet)
                    {
                        WriteLine($"\t{c}");
                    }
                }
            }
            catch (SqlException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
