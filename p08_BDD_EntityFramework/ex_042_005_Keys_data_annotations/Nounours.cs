﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_005_Keys_data_annotations
{
    public class Nounours
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UniqueId
        {
            get; set;
        }

        public string Nom
        {
            get;
            set;
        }

        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"Nounours {UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
