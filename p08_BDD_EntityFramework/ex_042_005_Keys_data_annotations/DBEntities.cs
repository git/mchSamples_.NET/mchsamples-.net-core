﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_005_Keys_data_annotations
{
    class DBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
        public DbSet<Cylon> CylonsSet { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_005_Keys_data_annotations.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
