#### to do
##### fundamentals (41)
* connection strings
  * V 001 exemple avec une connection string dans OnConfiguring
  * ? 002 exemple avec une connection string dans AppConfig ou �quivalent ?
  * ? 003 le m�me avec SecretManager pour les mots de passe ?
* ? 005 connection resiliency ?
* V testing
  * V 004 SQLite in memory
  * V 004 InMemory
* ? configuring a dbcontext ?
* ? nullable reference types => navigating (include, theninclude)
##### creating a model (42)
* V 001, 002, 003 entity types, entity properties
* V 004, 005, 006 keys, 007 generated values
* ? backing fields?
* V 008, 009 seeding data
* ###### relationships
  * V 010, 011 shadow properties
  * V 010, 011, single navigation property
  * cascade delete
  * V 012 013 one to one
  * 014 015 one to many
  * many to many
  * dictionaries
  * 012 013 foreign key
  * required and optional relationships
  * indexes?
* ###### inheritance
  * TPH
  * discriminator configuration
  * shared columns
* ? sequences?
* value conversion
* entity type constructors
* ? table splitting and table sharing?, owned types?
* ? keyless entity types?
* ? alternating model with same DbContext?
* ? spatial data?
##### managing database schemas + migrations (43)
* migrations
* create and drop APIs
* ? reverse engineering?
##### querying data: LINQ to SQL (44)
* loading all entity, loading a single entity, filtering
* client vs. server evaluation
* tracking vs. no-tracking
* complex query operators (join, groupjoin, selectmany, groupby, leftjoin)
* ###### loading related data (include, theninclude...)
  * eager loading
  * explicit loading
  * lazy loading
  * ? related data and serialization?
* ? asynchronous queries?
* ? raw sql queries?
* ? global query filters?
* ? query tags?
* how queries work
##### saving data (44)
* basic save (adding, removing, updating, multiple changes)
* related data (a graph of new entities, a related entity, changing or removing relationships)
* cascade delete
* concurrency conflicts
* transactions (TransactionScope ?)
* ? asynchronous saving?
* ? disconnected entities?
* ? setting explicit values for generated properties?
##### database providers (45)
* microsoft sql server
* sqlite
* cosmos?
* inmemory (for testing)
##### optional
* logging
* supported .NET implementations