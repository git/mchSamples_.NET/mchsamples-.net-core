﻿using Microsoft.EntityFrameworkCore;

namespace ex_042_001_EF_CF_conventions
{
    class NounoursDBEntities : DbContext
    {
        public DbSet<Nounours> NounoursSet { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
              optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ex_042_001_EF_CF_conventions.Nounours.mdf;Trusted_Connection=True;");
        }
    }
}
