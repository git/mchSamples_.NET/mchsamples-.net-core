﻿using System;

namespace ex_042_001_EF_CF_conventions
{
    public class Nounours
    {
        public int ID
        {
            get; set;
        }

        public string Nom
        {
            get;
            set;
        }

        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"{ID}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils)";
        }

    }
}
