using ex_041_004_TestingInMemory;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;
using Microsoft.Data.Sqlite;


namespace ex_041_004_UnitTests_w_SQLiteInMemory
{
    public class NounoursDB_Tests
    {
        [Fact]
        public void Add_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<NounoursContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                //context.Database.OpenConnection();
                context.Database.EnsureCreated();

                Nounours chewie = new Nounours { Nom = "Chewbacca" };
                Nounours yoda = new Nounours { Nom = "Yoda" };
                Nounours ewok = new Nounours { Nom = "Ewok" };

                context.Nounours.Add(chewie);
                context.Nounours.Add(yoda);
                context.Nounours.Add(ewok);
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new NounoursContext(options))
            {
                context.Database.EnsureCreated();
                
                Assert.Equal(3, context.Nounours.Count());
                Assert.Equal("Chewbacca", context.Nounours.First().Nom);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<NounoursContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new NounoursContext(options))
            {
                //context.Database.OpenConnection();
                context.Database.EnsureCreated();

                Nounours chewie = new Nounours { Nom = "Chewbacca" };
                Nounours yoda = new Nounours { Nom = "Yoda" };
                Nounours ewok = new Nounours { Nom = "Ewok" };

                context.Nounours.Add(chewie);
                context.Nounours.Add(yoda);
                context.Nounours.Add(ewok);
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new NounoursContext(options))
            {
                context.Database.EnsureCreated();
               
                string nameToFind = "ew";
                Assert.Equal(2, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                nameToFind = "wo";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                var ewok = context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).First();
                ewok.Nom = "Wicket";
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new NounoursContext(options))
            {
                context.Database.EnsureCreated();
             
                string nameToFind = "ew";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
                nameToFind = "wick";
                Assert.Equal(1, context.Nounours.Where(n => n.Nom.ToLower().Contains(nameToFind)).Count());
            }
        }
    }
}
