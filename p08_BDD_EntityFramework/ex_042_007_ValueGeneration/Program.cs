﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using static System.Console;

namespace ex_042_007_ValueGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours ();

            try
            {
                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    //nettoyage de la base de données
                    if (db.NounoursSet.Count() > 0)
                    {
                        foreach (var n in db.NounoursSet)
                        {
                            db.NounoursSet.Remove(n);
                        }
                        db.SaveChanges();
                    }

                    //ajout des nounours dans la base de données
                    db.NounoursSet.AddRange(new Nounours[] { chewie, yoda, ewok });
                    db.SaveChanges();

                    WriteLine("database after cleaning and adding 3 Nounours and saving changes :");
                    foreach (var n in db.NounoursSet)
                    {
                        WriteLine($"\t{n}");
                    }
                }

                WriteLine("Waits 3 seconds...");
                System.Threading.Thread.Sleep(3000);

                using (NounoursDBEntities db = new NounoursDBEntities())
                {

                    //modification d'un Nounours existant
                    WriteLine("\nModification of the name of Chewbacca to Chewie");
                    chewie = db.NounoursSet.First();
                    chewie.Nom = "Chewie";
                    db.SaveChanges();
                }

                using (NounoursDBEntities db = new NounoursDBEntities())
                {
                    foreach (var n in db.NounoursSet)
                    {
                        WriteLine($"\t{n}");
                    }

                    WriteLine("\nDisplay the last Nounours with default values");
                    Nounours e = db.NounoursSet.ToList().Last();
                    string nameStr = e.Nom != null ? e.Nom : "null";
                    WriteLine($"Name: {nameStr}; BirthDate: {e.DateDeNaissance}; Hair count: {e.NbPoils}; Insertion date: {e.DateDInsertion}; LastModified: {e.DernièreModification}") ;
                }
            }
            catch (SqlException)
            {
                WriteLine("Votre base de données n'existe pas. C'est peut-être la première fois que vous exécutez cet exemple.");
                WriteLine("Pour créer la base de données, suivez les instructions données dans le fichier ReadMe.md associé à cet exemple.");
            }

            ReadLine();
        }
    }
}
