﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ex_042_007_ValueGeneration
{
    [Table("TableNounours")]
    public class Nounours
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UniqueId
        {
            get; set;
        }

        [Required]
        [MaxLength(256)]
        public string Nom
        {
            get;
            set;
        }

        [Column("Naissance", TypeName = "date")]
        public DateTime DateDeNaissance
        {
            get;
            set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public DateTime DateDInsertion
        {
            get; set;
        }

        public DateTime DernièreModification
        {
            get; set;
        }

        public override string ToString()
        {
            return $"{UniqueId}: {Nom} ({DateDeNaissance:dd/MM/yyyy}, {NbPoils} poils, Inserted on: {DateDInsertion}, Last modified on: {DernièreModification})";
        }

    }
}
