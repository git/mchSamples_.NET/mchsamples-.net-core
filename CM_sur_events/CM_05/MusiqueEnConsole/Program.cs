﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiqueEnConsole
{
    class Program
    {
        static Dictionary<int, Func<Mélodie>> mélodies = new Dictionary<int,Func<Mélodie>>()
                                                  {
                                                   {1, () => new AuClairDeLaLune()},
                                                   {2, () => new TheGodfather()},
                                                   {3, () => new GOT()}
                                                  };
        static Mélodie melodie;

        static void Main(string[] args)
        {
            Console.WriteLine("Choisissez votre mélodie :");
            Console.WriteLine("1 - au clair de la lune");
            Console.WriteLine("2 - the godfather");
            Console.WriteLine("3 - got");
            
            int choix;
            if (!int.TryParse(Console.ReadLine(), out choix)
                || choix < 1 || choix > 3)
            {
                Console.WriteLine("choix non reconnu, bye bye !");
            }

            melodie = mélodies[choix]();

            Console.Clear();
            Console.WriteLine("Choisissez le nombre de pulsations par minute");

            if (!int.TryParse(Console.ReadLine(), out choix)
                || choix < 20 || choix > 200)
            {
                Console.WriteLine("non valide, bye bye");
            }

            melodie.PulsationsParMinute = choix;

            melodie.NoteLue += melodie_NoteRead;

            melodie.Démarrer();

            Console.ReadKey();
        }

        static void melodie_NoteRead(object sender, Mélodie.MélodieEventArgs e)
        {
            Console.WriteLine("{0} {1}", e.NoteFrequency, e.Duration);
            Console.Beep(e.NoteFrequency, e.Duration);
        }
    }
}
