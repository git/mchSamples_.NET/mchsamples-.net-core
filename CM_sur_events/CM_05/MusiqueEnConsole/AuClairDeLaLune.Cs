﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AuClairDeLaLune.Cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiqueEnConsole
{
    class AuClairDeLaLune : Mélodie
    {
        public AuClairDeLaLune()
            : base()
        {
            Notes.AddRange(new Note[] { 
                new Note(Tone.C, 1), 
                new Note(Tone.C, 1), 
                new Note(Tone.C, 1),
                new Note(Tone.D, 1),
                new Note(Tone.E, 2),
                new Note(Tone.D, 2),
                new Note(Tone.C, 1),
                new Note(Tone.E, 1),
                new Note(Tone.D, 1),
                new Note(Tone.D, 1),
                new Note(Tone.C, 4) });
        }
    }
}
