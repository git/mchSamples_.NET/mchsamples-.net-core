﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : GOT.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusiqueEnConsole
{
    class GOT : Mélodie
    {
        public GOT()
            : base()
        {
            Notes.AddRange(new Note[] { new Note(Tone.G, 3), new Note(Tone.C, 3),
                                        new Note(Tone.Dsharp,1), new Note(Tone.F,1), new Note(Tone.G,3),
                                        new Note(Tone.C, 3),
                                        new Note(Tone.Dsharp, 1), new Note(Tone.F, 1), new Note(Tone.D, 3),
                                        new Note(Tone.REST, 6),
                                        new Note(Tone.F, 3), new Note(Tone.A2sharp, 3),
                                        new Note(Tone.Dsharp, 1), new Note(Tone.D, 1), new Note(Tone.F, 3),
                                        new Note(Tone.A2sharp, 3),
                                        new Note(Tone.Dsharp, 1), new Note(Tone.D, 1), new Note(Tone.C,3)});
        }
    }
}
