﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MainWindow.xaml.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using giMorpionCore;

namespace MorpionGameWpf
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game game;
        public MainWindow()
        {
            InitializeComponent();

            game = new Game();

            game.GameStarted += (sender, args) =>
            {
                mTextBlockMessages.Text = "Game Starts at last !!!";
                mGrille.Update();
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.GameOver += (sender, args) =>
            {
                if (args.WinnerId == -1)
                {
                    mGrille.Update();

                    mTextBlockMessages.Text = "Deuce";
                    return;
                }

                mGrille.Update(args.WinningPlaces.ToArray());
                mTextBlockMessages.Text = string.Format("Congratulations Player {0} for your victory !", args.WinnerId);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.PieceInserted += (sender, args) =>
            {
                mGrille.Update(args.Place);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.PlayerNotified += (sender, args) =>
            {
                mTextBlockMessages.Text = string.Format("Player {0}, it's your turn !", args.PlayerId);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            mGrille.Game = game;
        }

        Action EmptyDelegate = delegate() { };

        private void Window_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            game.Start();
        }
    }
}
