﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using giMorpionCore;

namespace MorpionGame
{
    class Program
    {
        static void Main()
        {
            Game game = new Game();

            game.GameStarted += new EventHandler<EventArgs>((object sender, EventArgs args) => 
            {
                Console.WriteLine("Game Starts at last !!!");
                Display(sender as Game);
            });

            game.GameOver += OnGameOver;

            game.PieceInserted += new EventHandler<Game.PieceInsertedEventArgs>((object sender, Game.PieceInsertedEventArgs args) => Console.Clear());
            game.PieceInserted += (object sender, Game.PieceInsertedEventArgs args) => Display(sender as Game, args.Place);
            
            game.PlayerNotified += (sender, args) => Console.WriteLine("Player {0}, it's your turn !", args.PlayerId);
            game.Start();
        }

        private static void OnGameOver(object sender, Game.GameOverEventArgs args)
        {
            Console.Clear();
            if (args.WinnerId == -1)
            {
                Display(sender as Game);
                Console.WriteLine("Deuce");
                return;
            }

            Display(sender as Game, args.WinningPlaces.ToArray());
            Console.WriteLine("Congratulations Player {0} for your victory !", args.WinnerId);
        }

        private static void Display(giMorpionCore.Game game, params giMorpionCore.Case[] highlightedCases)
        {
            Console.ForegroundColor = ConsoleColor.White;
            int[,] board = game.Board;
            Console.WriteLine("    0  1  2 ");
            for (int line = 0; line < 3; line++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" " + line + " ");
                for (int column = 0; column < 3; column++)
                {
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    foreach (giMorpionCore.Case place in highlightedCases)
                    {
                        if (place.Line == line && place.Column == column)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            break;
                        }
                    }
                    DisplayCaseContent(board, line, column);
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// affiche le contenu d'une case en fonction du joueur
        /// </summary>
        /// <param name="board">la grille du jeu du Morpion prise en compte</param>
        /// <param name="line">la ligne de la case à prendre en compte</param>
        /// <param name="column">la colonne de la case à prendre en compte</param>
        private static void DisplayCaseContent(int[,] board, int line, int column)
        {
            if (board[line, column] == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write(" O ");
                return;
            }
            if (board[line, column] == 1)
            {
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write(" X ");
                return;
            }
            Console.Write("   ");
        }
    }
}
