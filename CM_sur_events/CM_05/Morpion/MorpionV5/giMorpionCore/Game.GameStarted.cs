﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Game.GameStarted.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    public partial class Game
    {
        public event EventHandler<EventArgs> GameStarted;

        void OnGameStarted()
        {
            if (GameStarted != null)
            {
                GameStarted(this, null);
            }
        }
    }
}
