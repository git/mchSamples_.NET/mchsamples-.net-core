﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Game.PlayerNotified.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    public partial class Game
    {
        public class PlayerNotifiedEventArgs : EventArgs
        {
            public int PlayerId
            {
                get;
                private set;
            }

            public PlayerNotifiedEventArgs(int playerId)
            {
                PlayerId = playerId;
            }
        }

        public event EventHandler<PlayerNotifiedEventArgs> PlayerNotified;

        void OnPlayerNotified(PlayerNotifiedEventArgs args)
        {
            if (PlayerNotified != null)
            {
                PlayerNotified(this, args);
            }
        }
    }
}
