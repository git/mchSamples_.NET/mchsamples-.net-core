﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Case.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    /// <summary>
    /// représente une case du jeu du Morpion
    /// </summary>
    public struct Case
    {
        /// <summary>
        /// ligne de la case
        /// </summary>
        public int Line
        {
            get
            {
                return mLine;
            }
            private set
            {
                mLine = value;
            }
        }
        private int mLine;

        /// <summary>
        /// colonne de la case
        /// </summary>
        public int Column
        {
            get
            {
                return mColumn;
            }
            private set
            {
                mColumn = value;
            }
        }
        private int mColumn;

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="line">ligne de la case</param>
        /// <param name="column">colonne de la case</param>
        public Case(int line, int column)
        {
            mLine = line;
            mColumn = column;
        }
    }
}
