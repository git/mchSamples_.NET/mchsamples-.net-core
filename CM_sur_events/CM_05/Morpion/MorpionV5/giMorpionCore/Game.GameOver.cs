﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Game.GameOver.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace giMorpionCore
{
    public partial class Game
    {
        public class GameOverEventArgs : EventArgs
        {
            public int WinnerId
            {
                get;
                private set;
            }

            public ReadOnlyCollection<Case> WinningPlaces
            {
                get;
                private set;
            }

            public GameOverEventArgs(int winnerId, params Case[] winningPlaces)
            {
                WinnerId = winnerId;
                WinningPlaces = new ReadOnlyCollection<Case>(winningPlaces);
            }
        }

        public event EventHandler<GameOverEventArgs> GameOver;

        void OnGameOver(GameOverEventArgs args)
        {
            if (GameOver != null)
            {
                GameOver(this, args);
            }
        }
    }
}
