﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Game.PieceInserted.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    public partial class Game
    {
        public class PieceInsertedEventArgs : EventArgs
        {
            public int PlayerId
            {
                get;
                private set;
            }

            public Case Place
            {
                get;
                private set;
            }

            public PieceInsertedEventArgs(int playerId, Case place)
            {
                PlayerId = playerId;
                Place = place;
            }
        }

        public event EventHandler<PieceInsertedEventArgs> PieceInserted;

        void OnPieceInserted(PieceInsertedEventArgs args)
        {
            if (PieceInserted != null)
            {
                PieceInserted(this, args);
            }
        }
    }
}
