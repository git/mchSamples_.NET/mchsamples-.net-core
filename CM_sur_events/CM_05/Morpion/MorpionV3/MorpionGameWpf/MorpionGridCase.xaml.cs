﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MorpionGridCase.xaml.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorpionGameWpf
{
    /// <summary>
    /// Logique d'interaction pour MorpionGridCase.xaml
    /// </summary>
    public partial class MorpionGridCase : UserControl
    {
        public MorpionGridCase()
        {
            InitializeComponent();
        }


        public string PlayerId
        {
            get { return (string)GetValue(PlayerIdProperty); }
            set { SetValue(PlayerIdProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlayerId.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlayerIdProperty =
            DependencyProperty.Register("PlayerId", typeof(string), typeof(MorpionGridCase), new PropertyMetadata(" "));



        public SolidColorBrush BackgroundColor
        {
            get { return (SolidColorBrush)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof(SolidColorBrush), typeof(MorpionGridCase), new PropertyMetadata(Brushes.AliceBlue));

        

        
    }
}
