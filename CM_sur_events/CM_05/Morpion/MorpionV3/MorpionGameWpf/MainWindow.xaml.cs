﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MainWindow.xaml.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using giMorpionCore;

namespace MorpionGameWpf
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game game;
        public MainWindow()
        {
            InitializeComponent();

            game = new Game();

            game.OnGameStarted += (Game g) =>
            {
                mTextBlockMessages.Text = "Game Starts at last !!!";
                mGrille.Update();
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.OnGameOver += (Game g, int winnerId, Case[] winningPlaces) => 
            {
                if (winnerId == -1)
                {
                    mGrille.Update();

                    mTextBlockMessages.Text = "Deuce";
                    return;
                }

                mGrille.Update(winningPlaces);
                mTextBlockMessages.Text = string.Format("Congratulations Player {0} for your victory !", winnerId);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.OnPieceInserted += (Game g, int playerId, Case place) =>
            {
                mGrille.Update(place);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            game.OnPlayerNotified += (Game g, int playerId) =>
            {
                mTextBlockMessages.Text = string.Format("Player {0}, it's your turn !", playerId);
                this.Dispatcher.Invoke(EmptyDelegate, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
            };

            mGrille.Game = game;
        }
        
        Action EmptyDelegate = delegate() { };

        private void Window_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            game.Start();
        }
    }
}
