﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using giMorpionCore;

namespace MorpionGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            game.OnGameStarted += new Game.GameStarted((Game g) => 
            {
                Console.WriteLine("Game Starts at last !!!");
                Display(g);
            });

            game.OnGameOver += OnGameOver;

            game.OnPieceInserted += new Game.PieceInserted((Game g, int playerId, Case place) => Console.Clear());
            game.OnPieceInserted += (Game g, int playerId, Case place) => Display(g, place);

            game.OnPlayerNotified += (Game g, int playerId) => Console.WriteLine("Player {0}, it's your turn !", playerId);
            game.Start();
        }

        private static void OnGameOver(Game game, int winnerId, Case[] winningPlaces)
        {
            Console.Clear();
            if (winnerId == -1)
            {
                Display(game);
                Console.WriteLine("Deuce");
                return;
            }

            Display(game, winningPlaces);
            Console.WriteLine("Congratulations Player {0} for your victory !", winnerId);
        }

        private static void Display(giMorpionCore.Game game, params giMorpionCore.Case[] highlightedCases)
        {
            Console.ForegroundColor = ConsoleColor.White;
            int[,] board = game.Board;
            Console.WriteLine("    0  1  2 ");
            for (int line = 0; line < 3; line++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" " + line + " ");
                for (int column = 0; column < 3; column++)
                {
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    foreach (giMorpionCore.Case place in highlightedCases)
                    {
                        if (place.Line == line && place.Column == column)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            break;
                        }
                    }
                    DisplayCaseContent(board, line, column);
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// affiche le contenu d'une case en fonction du joueur
        /// </summary>
        /// <param name="board">la grille du jeu du Morpion prise en compte</param>
        /// <param name="line">la ligne de la case à prendre en compte</param>
        /// <param name="column">la colonne de la case à prendre en compte</param>
        private static void DisplayCaseContent(int[,] board, int line, int column)
        {
            if (board[line, column] == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write(" O ");
                return;
            }
            if (board[line, column] == 1)
            {
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write(" X ");
                return;
            }
            Console.Write("   ");
        }
    }
}
