﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using giMorpionCore;

namespace MorpionGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(new ConsoleDisplayGame());
            game.Start();
        }
    }
}
