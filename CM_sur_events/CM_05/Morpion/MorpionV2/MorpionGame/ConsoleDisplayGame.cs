﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ConsoleDisplayGame.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorpionGame
{
    /// <summary>
    /// un afficheur du jeu de Morpion en mode Console
    /// </summary>
    class ConsoleDisplayGame : giMorpionCore.IDisplayGame
    {
        /// <summary>
        /// appelée lorsque le jeu démarre
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        public void GameStarted(giMorpionCore.Game game)
        {
            Console.WriteLine("Game Starts at last !!!");
            Display(game);
        }

        /// <summary>
        /// appelée lorsque le jeu se termine
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="winnerId">identifiant du vainqueur (-1 si égalité)</param>
        /// <param name="winningPlaces">la liste des cases qui lui ont permis de gagner</param>
        public void GameOver(giMorpionCore.Game game, int winnerId, giMorpionCore.Case[] winningPlaces)
        {
            Console.Clear();
            if (winnerId == -1)
            {
                Display(game);
                Console.WriteLine("Deuce");
                return;
            }

            Display(game, winningPlaces);
            Console.WriteLine("Congratulations Player {0} for your victory !", winnerId);
        }

        /// <summary>
        /// appelée lorsqu'une pièce est insérée
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="playerId">identifiant du joueur qui insère une pièce</param>
        /// <param name="place">case dans laquelle il a inséré sa pièce</param>
        public void PieceInserted(giMorpionCore.Game game, int playerId, giMorpionCore.Case place)
        {
            Console.Clear();
            Display(game, place);
        }

        /// <summary>
        /// appelée lorsqu'on invite un joueur à jouer
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="playerId">identifiant du joueur qui doit jouer</param>
        public void PlayerNotified(giMorpionCore.Game game, int playerId)
        {
            Console.WriteLine("Player {0}, it's your turn !", playerId);
        }

        /// <summary>
        /// affiche la grille du jeu de Morpion
        /// </summary>
        /// <param name="game">le jeu à afficher</param>
        /// <param name="highlightedCases">les éventuelles cases à mettre en surbrillance</param>
        private static void Display(giMorpionCore.Game game, params giMorpionCore.Case[] highlightedCases)
        {
            Console.ForegroundColor = ConsoleColor.White;
            int[,] board = game.Board; 
            Console.WriteLine("    0  1  2 ");
            for (int line = 0; line < 3; line++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" " + line + " ");
                for (int column = 0; column < 3; column++)
                {
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    foreach (giMorpionCore.Case place in highlightedCases)
                    {
                        if (place.Line == line && place.Column == column)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            break;
                        }
                    }
                    DisplayCaseContent(board, line, column);
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// affiche le contenu d'une case en fonction du joueur
        /// </summary>
        /// <param name="board">la grille du jeu du Morpion prise en compte</param>
        /// <param name="line">la ligne de la case à prendre en compte</param>
        /// <param name="column">la colonne de la case à prendre en compte</param>
        private static void DisplayCaseContent(int[,] board, int line, int column)
        {
            if (board[line, column] == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write(" O ");
                return;
            }
            if (board[line, column] == 1)
            {
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write(" X ");
                return;
            }
            Console.Write("   ");
        }
    }
}
