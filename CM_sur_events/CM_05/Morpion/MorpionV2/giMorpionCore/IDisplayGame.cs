﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IDisplayGame.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    /// <summary>
    /// un afficheur du jeu de Morpion
    /// </summary>
    public interface IDisplayGame
    {
        /// <summary>
        /// appelée lorsque le jeu démarre
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        void GameStarted(Game game);

        /// <summary>
        /// appelée lorsque le jeu se termine
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="winnerId">identifiant du vainqueur (-1 si égalité)</param>
        /// <param name="winningPlaces">la liste des cases qui lui ont permis de gagner</param>
        void GameOver(Game game, int winnerId, Case[] winningPlaces);

        /// <summary>
        /// appelée lorsqu'une pièce est insérée
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="playerId">identifiant du joueur qui insère une pièce</param>
        /// <param name="place">case dans laquelle il a inséré sa pièce</param>
        void PieceInserted(Game game, int playerId, Case place);

        /// <summary>
        /// appelée lorsqu'on invite un joueur à jouer
        /// </summary>
        /// <param name="game">le jeu de Morpion en cours</param>
        /// <param name="playerId">identifiant du joueur qui doit jouer</param>
        void PlayerNotified(Game game, int playerId);
    }
}
