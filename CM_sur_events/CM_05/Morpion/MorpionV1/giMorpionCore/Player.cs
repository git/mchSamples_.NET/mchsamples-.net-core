﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Player.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-07
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giMorpionCore
{
    /// <summary>
    /// joueur du Morpion (c'est un joueur automatique qui joue aléatoirement dans une case)
    /// </summary>
    public class Player
    {
        /// <summary>
        /// compteur statique pour créer un id automatique
        /// </summary>
        static private int mIdCounter = 0;

        /// <summary>
        /// identifiant du joueur
        /// </summary>
        public int Id
        {
            get;
            internal set;
        }

        /// <summary>
        /// constructeur (l'identifant est créé automatiquement et le jeu est relié à ce joueur)
        /// </summary>
        /// <param name="game">le jeu auquel ce joueur appartient</param>
        internal Player(Game game)
        {
            Id = mIdCounter;
            mIdCounter++;
            mGame = game;
        }

        /// <summary>
        /// le jeu auquel ce joueur appartient
        /// </summary>
        internal Game mGame;

        /// <summary>
        /// méthode permettant au joueur de jouer (choix aléatoire d'une case dans laquelle il va jouer)
        /// </summary>
        /// <returns>la case dans laquelle il veut jouer</returns>
        protected internal Case Play()
        {
            Random rdm = new Random();
            int line = rdm.Next(0,3);
            int column = rdm.Next(0,3);
            while (mGame.Board[line, column] != -1)
            {
                line = rdm.Next(0, 3);
                column = rdm.Next(0, 3);
            }
            return new Case(line, column);
        }
    }
}
