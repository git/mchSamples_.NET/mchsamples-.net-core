---
sidebar_position: 2
title: Unit Tests avec xUnit
---

# Unit Tests avec xUnit

Vous trouverez ici une vidéo de présentation des tests unitaires avec xUnit. Je ne montre pas tout, mais ça vous permettra de débuter avec :
- [Fact] : des tests simples,
- [Theory] + [InlineData] : des tests avec jeu de données (mais uniquement des constantes)
- [Theory] + [MemberData] : des tests avec des jeux de données pouvant posséder des paramètres construits à la volée, dans la classe de tests ou dans une classe extérieure.  

N'oubliez pas que vous pouvez trouver les exemples de cours ici sur les tests unitaires : [Unit Tests sample](https://codefirst.iut.uca.fr/git/mchSamples_.NET/mchsamples-.net-core/src/branch/master/p05_More_dotNet/ex_026_001_xUnit_Fact)

<iframe allowfullscreen src='https://opencast.dsi.uca.fr/paella/ui/embed.html?id=4563de39-f30d-4080-ab8b-96e29a4e9ca7' width='640' height='480'  frameborder='0' scrolling='no' marginwidth='0' marginheight='0' allowfullscreen='true' webkitallowfullscreen='true' mozallowfullscreen='true' ></iframe>