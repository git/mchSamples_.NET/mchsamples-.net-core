---
sidebar_label: 'Introduction'
sidebar_position: 1
description: 'Avec ce chapitre, vous aurez plus de détails sur les relations entre entités'
---

# Relationships

On parle de propriété de navigation dès qu'on a une propriété qui ne peut pas être une donnée simple (string, nombre, bool...).  
Par défaut (*ie* conventions d'écriture), dans une relation, 
c'est toujours la primary key qui est utilisée. 
On peut utiliser une autre clé en passant par la notation Fluent.  

  
Le plus commun c'est d'avoir des propriétés de navigation 
définies sur les deux entités et une clé étrangère (foreign key)
du côté de l'entité dépendante.  
* si a on une paire de propriétés de navigation sur les deux entités, l'une sera l'inverse de l'autre
* si l'entité dépendante possède une propriété dont le nom vérifie l'un des patrons suivants, alors elle est configurée comme une foreign key:
  * ```<navigationPropertyName><principalKeyPropertyName>```
  * ```<navigationPropertyName>Id```
  * ```<principalEntityName><principalKeyPropertyName>```
  * ```<principalEntityName>Id```
* si on a aucune propriété de clé étrangère, une shadow property est créée avec le nom ```<navigationPropertyName><principalKeyPropertyName>``` ou ```<principalEntityName><principalKeyPropertyName>```.
* Single Property Navigation vs. Pair Navigation Property
* Cascade Delete
* ex01: single navigation property (conventions & data annotations)
* ex02: single navigation property (fluent api)
* ex03: one to one (conventions & data annotations)
* ex04: one to one (fluent api)
* ex05: one to many (conventions & data annotations)
* ex06: one to many (fluent api)
* ex07: many to many (conventions & data annotations)
* ex08: many to many (fluent api)
* ex09: shadow property
* ex10: dictionaries (fluent api)