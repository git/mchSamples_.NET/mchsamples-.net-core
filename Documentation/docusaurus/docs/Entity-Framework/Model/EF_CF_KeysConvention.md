---
sidebar_label: '2.4. Keys with conventions (Entity Framework Code First)'
sidebar_position: 4
description: "explique comment créer les clés primaires d'une entité lorsqu'on utilise les conventions d'écriture"
---

# Keys with conventions
*06/01/2020 &sdot; Marc Chevaldonné*  
[**&#9654; Browse Sample &#8599;**](https://codefirst.iut.uca.fr/git/mchSamples_.NET/mchsamples-.net-core/src/branch/master/p08_BDD_EntityFramework/ex_042_004_Keys_conventions)  

---

Cet exemple traite des clés primaires associées aux entités.

:::info Prérequis
Je n'explique pas à travers cet exemple les principes de base d'**Entity Framework Core** et en particulier les chaînes de connexion et le lien entre entité et table.
Pour plus de renseignements sur :
* les chaînes de connexion : [**1.1. Connection Strings**](/docs/Entity-Framework/Fundamentals/ConnectionStrings) 
* les liens entre entités et tables : [**2.1. conventions d'écriture**](/docs/Entity-Framework/Model/EF_CF_namingConventions), [**2.2. data annotations**](/docs/Entity-Framework/Model/EF_CF_dataAnnotations) et [**2.3. Fluent API**](/docs/Entity-Framework/Model/EF_CF_FluentAPI)
:::
Cet exemple montre le cas particulier de la gestion des clés primaires lors de l'utilisation des **conventions d'écriture**.  

:::tip
Vous pourrez trouver une version plus ou moins équivalente avec les *data annotations* ici : [**2.5. Keys with data annotations**](/docs/Entity-Framework/Model/EF_CF_KeysDataAnnotations).  
Vous pourrez trouver une version plus ou moins équivalente avec la *Fluent API* ici : [**2.6. Keys with Fluent API**](/docs/Entity-Framework/Model/EF_CF_Keys_FluentAPI).  
:::

---

## Les clés primaires
Une clé permet de rendre unique chaque instance d'une entité. La plupart des entités n'ont qu'une seule clé qui est alors transformée en *clé primaire* pour les bases de données relationnelles.  
:::note 
une entité peut avoir d'autres clés, on parle d'__alternate keys__. Elles seront présentées dans les exemples sur les relations entre entités.
:::
Si on utilise les *conventions d'écriture*, une propriété pour être transformée en clé doit respecter les contraintes suivantes :
* elle doit être nommée ```Id``` ou ```ID```,
* elle doit être nommée ```<typeDeLEntite>Id```, e.g. ```NounoursId```.

Les autres contraintes sur une clé dans le cas de l'utilisation des *conventions d'écriture* sont :
* elle doit être de type ```int```, ```string```, ```byte[]```. Toutefois, certains types nécessitent l'utilisation de converteurs pour être utilisés avec certains fournisseurs. Je conseille donc l'utilisation de ```int``` qui marche avec la grande majorité des fournisseurs.
* elle est générée lors de l'insertion en base.

Les autres modes (*data annotations* et *Fluent API*) offrent plus de solutions quant à la gestion des clés.

## La classe ```Nounours```
La classe ```Nounours``` utilise les conventions d'écriture.
* Par défaut, les propriétés utilisées comme clés primaires sont en mode **Generated on add**. 
Une nouvelle valeur est donc générée lors de l'insertion d'une nouvelle entité en base. Les valeurs des autres propriétés ne sont pas générées lors de l'insertion ou de la mise à jour. 
* Dans cette classe, j'ai respecté la contrainte de nommage qui propose ```Id``` ou ```ID```
```csharp title='Nounours.cs'
public int ID
{
    get; set;
}
```

## La classe ```Cylon```
La classe ```Cylon``` utilise les conventions d'écriture.
* Dans cette classe, j'ai respecté la contrainte de nommage qui propose ```<TypeDeLEntité>Id```
```csharp title='Nounours.cs'
public int CylonId
{
    get; set;
}
```

## La classe ```Program```
Cette classe est le point d'entrée du programme :
* Elle crée des instances de ```Nounours``` et de ```Cylon``` et les ajoute en base après avoir nettoyé les tables au préalables. 
* Elle affiche les ```Nounours``` et les ```Cylon```.
:::tip
Notez la génération des identifiants !
:::

## Comment exécuter cet exemple ?
Pour tester cette application, n'oubliez pas les commandes comme présentées dans l'exemple ex_041_001 : pour générer l'exemple, il vous faut d'abord préparer les migrations et les tables.
  * Ouvrez la *Console du Gestionnaire de package*, pour cela, dirigez-vous dans le menu *Outils*, puis *Gestionnaire de package NuGet*, puis *Console du Gestionnaire de package*.
  * Dans la console que vous venez d'ouvrir, déplacez-vous dans le dossier du projet .NET Core, ici :
```
cd .\p08_BDD_EntityFramework\ex_042_004_Keys_conventions
``` 
:::note
  si vous n'avez pas installé correctement EntityFrameworkCore, il vous faudra peut-être utiliser également :

* ```dotnet tool install --global dotnet-ef``` si vous utilisez la dernière version de .NET Core (3.1 aujourd'hui),  

* ```dotnet tool install --global dotnet-ef --version 3.0.0``` si vous vous utiliser spécifiquement .NET Core 3.0.
:::

  * Migration : 
```
dotnet ef migrations add migration_ex_042_004
```
  * Création de la table :
```
dotnet ef database update
```
  * Génération et exécution
Vous pouvez maintenant générer et exécuter l'exemple **ex_042_004_Keys_conventions**.

  * Comment vérifier le contenu des bases de données SQL Server ?
Vous pouvez vérifier le contenu de votre base en utilisant l'*Explorateur d'objets SQL Server*.
* Pour cela, allez dans le menu *Affichage* puis *Explorateur d'objets SQL Server*.  
![Sql Server](../Fundamentals/ConnectionStringsFiles/sqlserver_01.png)  

* Déployez dans l'*Explorateur d'objets SQL Server* :
  *  *SQL Server*, 
  *  puis *(localdb)\MSSQLLocalDB ...*, 
  *  puis *Bases de données*
  *  puis celle portant le nom de votre migration, dans mon cas : *ex_042_004_Keys_conventions.Nounours.mdf*
  *  puis *Tables*   
  *  Faites un clic droit sur la table *dbo.Nounours* puis choisissez *Afficher les données*  
![Sql Server](../Fundamentals/ConnectionStringsFiles/sqlserver_02.png)  

* Le résultat de l'exécution peut être :
```
database after cleaning and adding 3 Nounours and 9 Cylons and saving changes :
        Nounours 1: Chewbacca (27/05/1977, 1234567 poils)
        Nounours 2: Yoda (21/05/1980, 3 poils)
        Nounours 3: Ewok (25/05/1983, 3456789 poils)
        Cylon 1: John Cavil, Number 1
        Cylon 2: Leoben Conoy, Number 2
        Cylon 3: D'Anna Biers, Number 3
        Cylon 4: Simon, Number 4
        Cylon 5: Aaron Doral, Number 5
        Cylon 6: Caprica 6, Number 6
        Cylon 7: Daniel, Number 7
        Cylon 8: Boomer, Number 8
        Cylon 9: Athena, Number 8
```
:::note 
les identifiants peuvent varier en fonction du nombre d'exécution de l'exemple depuis la création de la base de données.
:::