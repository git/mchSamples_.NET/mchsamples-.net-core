---
sidebar_label: '2.8. Data Seeding Before Entity Framework 2.1'
sidebar_position: 8
description: "explique comment utiliser un stub (méthode qui était recommandée avant EF Core 2.1)"
---

# Data Seeding Before Entity Framework 2.1
*13/01/2020 &sdot; Marc Chevaldonné*  
[**&#9654; Browse Sample &#8599;**](https://codefirst.iut.uca.fr/git/mchSamples_.NET/mchsamples-.net-core/src/branch/master/p08_BDD_EntityFramework/ex_042_008_DataSeeding_before_EF2.1)  

---

Cet exemple montre comment il était recommandé d'utiliser du Stub avec Entity Framework Core avant la version 2.1.  
:::caution Conseil
Il est conseillé d'utiliser une méthode plus moderne (cf. [**2.9. Data Seeding**](/docs/Entity-Framework/Model/DataSeeding)).
:::

---

## Comment est construit cet exemple ?
* Le projet est de type .NET Core
* Il contient trois classes :
  * ```Nounours```
  * ```NounoursDBEntities```
  * ```DataSeeder```
  
Le contenu des classes ```Nounours``` et ```NounoursDBEntities``` correspond à ce qui a été vu dans les exemples précédents. Seule la classe ```DataSeeder``` sera donc expliquée ici.

### La classe ```DataSeeder```

* ```DataSeeder``` est une classe statique possédant une méthode ```SeedData``` dont le but est d'ajouter des éléments à la base.
```csharp title='DataSeeder.cs'
using Microsoft.EntityFrameworkCore;
using System;

namespace ex_042_008_DataSeeding_before_EF2_1
{
    public static class DataSeeder
    {
        public static void SeedData(DbContext context)
        {
            Nounours chewie = new Nounours { Nom = "Chewbacca", DateDeNaissance = new DateTime(1977, 5, 27), NbPoils = 1234567 };
            Nounours yoda = new Nounours { Nom = "Yoda", DateDeNaissance = new DateTime(1980, 5, 21), NbPoils = 3 };
            Nounours ewok = new Nounours { Nom = "Ewok", DateDeNaissance = new DateTime(1983, 5, 25), NbPoils = 3456789 };

            context.AddRange(new Nounours[] { chewie, yoda, ewok });

            context.SaveChanges();
        }
    }
}
```
* Elle est utilisée dans ```Program``` pour remplir la base.
```csharp title='Program.cs'
using (NounoursDBEntities db = new NounoursDBEntities())
{
    //...
    DataSeeder.SeedData(db);
    //...
}
```

## Comment exécuter cet exemple ?
Pour tester cette application, n'oubliez pas les commandes comme présentées dans l'exemple ex_041_001 : pour générer l'exemple, il vous faut d'abord préparer les migrations et les tables.
  * Ouvrez la *Console du Gestionnaire de package*, pour cela, dirigez-vous dans le menu *Outils*, puis *Gestionnaire de package NuGet*, puis *Console du Gestionnaire de package*.
  * Dans la console que vous venez d'ouvrir, déplacez-vous dans le dossier du projet .NET Core, ici :
```
cd .\p08_BDD_EntityFramework\ex_042_008_DataSeeding_before_EF2_1
``` 
:::note
  si vous n'avez pas installé correctement EntityFrameworkCore, il vous faudra peut-être utiliser également :

* ```dotnet tool install --global dotnet-ef``` si vous utilisez la dernière version de .NET Core (3.1 aujourd'hui),  

* ```dotnet tool install --global dotnet-ef --version 3.0.0``` si vous vous utiliser spécifiquement .NET Core 3.0.
:::

  * Migration : 
```
dotnet ef migrations add migration_ex_042_008
```
  * Création de la table :
```
dotnet ef database update
```
  * Génération et exécution
Vous pouvez maintenant générer et exécuter l'exemple **ex_042_008_DataSeeding_before_EF2.1**.

  * Le résultat de l'exécution peut ressembler à :
```
nettoyage de la base car elle n'était pas vide
remplissage avec du stub
Contenu de la base :
        9540729e-67d9-442c-63fb-08d798777717: Chewbacca (27/05/1977, 1234567 poils)
        df1ce76a-97ee-42c7-63fc-08d798777717: Yoda (21/05/1980, 3 poils)
        0b921251-12a6-480a-63fd-08d798777717: Ewok (25/05/1983, 3456789 poils)
```
:::note
Les identifiants seront bien sûr différents
:::

  * Comment vérifier le contenu des bases de données SQL Server ?
Vous pouvez vérifier le contenu de votre base en utilisant l'*Explorateur d'objets SQL Server*.
* Pour cela, allez dans le menu *Affichage* puis *Explorateur d'objets SQL Server*.  
![Sql Server](../Fundamentals/ConnectionStringsFiles/sqlserver_01.png)   

* Déployez dans l'*Explorateur d'objets SQL Server* :
  *  *SQL Server*, 
  *  puis *(localdb)\MSSQLLocalDB ...*, 
  *  puis *Bases de données*
  *  puis celle portant le nom de votre migration, dans mon cas : *ex_042_008_DataSeeding_before_EF2_1.Nounours.mdf*
  *  puis *Tables*   
  *  Faites un clic droit sur la table *dbo.Nounours* puis choisissez *Afficher les données*  
![Sql Server](../Fundamentals/ConnectionStringsFiles/sqlserver_02.png)   

  *  Vous devriez maintenant pouvoir voir les données suivantes dans le tableau :  
 
  |UniqueId   |Nom|Naissance
  |---|---|---
  |9540729e-67d9-442c-63fb-08d798777717|Chewbacca|27/05/1977
  |df1ce76a-97ee-42c7-63fc-08d798777717|Yoda|21/05/1980
  |0b921251-12a6-480a-63fd-08d798777717|Ewok|25/05/1983  
:::note
les identifiants seront bien sûr différents.
:::
:::note
Notez l'absence de la colonne "NbPoils"
:::
:::note
Notez le nom de la colonne "Naissance" et le formatage de la date
:::