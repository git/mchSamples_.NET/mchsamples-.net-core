---
sidebar_label: '6.1. MAUI, EF et SQLite'
sidebar_position: 1
description: "explique comment utiliser le fournisseur SQLite et Entity Framework dans une application MAUI"
---
import Mermaid from '@theme/Mermaid';

# Utilisation du fournisseur SQLite, via Entity Framework, dans une application MAUI
*23/02/2023 &sdot; Marc Chevaldonné*  

---

:::info Pré-requis
Cette page n'explique pas comment écrire un ```DbContext``` ou comment utiliser *Entity Framework*.  
Référez-vous aux articles précédents pour récupérer ces informations.
:::

## Structure

Considérons une application **MAUI_App** consommant une bibliothèque de classe **EFLib** 
contenant une classe fille de ```DbContext```, appelée ```MyDbContext```.

<Mermaid chart={`
  flowchart LR
    MAUI_App -.-> EFLib;
`}/>

```MyDbContext``` possède un constructeur permettant l'injection d'un ```DbContextOptions<MyDbContext>``` afin de proposer le fournisseur de son choix et la chaîne de connexion adaptée.

```csharp title='MyDbContext.cs'
public class MyDbContext : DbContext
{
   public MyDbContext() { }

   public MyDbContext(DbContextOptions<MyDbContext> options)
      : base(options)
   { }

   protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
   {
      if(!optionsBuilder.IsConfigured)
      {
        //...
      }
   }
}
```

## Injection d'un ```DbContext``` dans l'application MAUI

Pour injecter un ```DbContext```, dans la classe ```MauiProgram``` (ou son équivalent), on peut utiliser la méthode d'extension ```AddDbContext```.  

```csharp title='MauiProgram.cs'
public static class MauiProgram
{
	const string DatabaseFilename = "MyDatabase.db";

	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		
    //...

		builder.Services.AddDbContext<MyDbContext>(opt => opt.UseSqlite($"Filename={Path.Combine(FileSystem.AppDataDirectory, DatabaseFilename)}"));

    //...

		return builder.Build();
	}
}
```

```AddDbContext``` prend en paramètre une ```Action<DbContextOptionsBuilder>``` vous permettant notamment de passer un ```DbContextOptionsBuilder``` construit à la volée.  
  
```FileSystem.AppDataDirectory``` permet d'atteindre le dossier spécial de données de l'application, quel que soit l'OS.  
C'est le dossier recommandé pour placer votre base de données, mais vous pouvez choisir un autre emplacement.  

## Consommation dans une page, une VM, ou un autre service
Vous pouvez désormais bénéficier de l'injection de dépendances de MAUI pour utiliser votre DbContext, dans une page, dans une VM ou dans un autre service.  
  
Si on veut par exemple l'utiliser dans la page ```MainPage```, il suffit d'ajouter un membre de type ```MyDbContext``` à ```MainPage```, et de l'injecter via le constructeur.  
```csharp title='MainPage.xaml.cs'
public partial class MainPage : ContentPage
{
	private ArtistsDbContext context;

	public MainPage(ArtistsDbContext context)
	{
    //...
		this.context = context;
		context.Database.EnsureCreated();
		InitializeComponent();
    //...
	}

  //...
}
```

:::caution context.Database.EnsureCreated();
Notez l'appel de ```context.Database.EnsureCreated();``` qui permet de garantir l'existence de la base de données avant son utilisation.  
Si elle n'existe pas, elle est créée lors de l'appel de cette méthode.
:::

:::tip EnsuredCreated() dans la classe ```DbContext```
Vous pouvez aussi appeler ```Database.EnsureCreated();``` directement dans le code de votre classe dérivant de ```DbContext``` pour
vous éviter d'avoir à le faire dans vos vues ou vos VMs, si vous avez accès à ce code.
:::
  
On peut ensuite déclarer la page et réaliser l'injection du ```DbContext``` dans la classe ```MauiProgram``` (ou son équivalent) :  
```csharp title='MauiProgram.cs'
builder.Services.AddDbContext<MyDbContext>(opt => opt.UseSqlite($"Filename={Path.Combine(FileSystem.AppDataDirectory, DatabaseFilename)}"))
                .AddSingleton<MainPage>();
```
  
On peut aussi récupérer le service à l'aide d'un ```ServiceProvider``` injecté. Par exemple, dans ```MainPage``` (mais réalisable n'importe où) :
```csharp title='MainPage.xaml.cs'
public partial class MainPage : ContentPage
{
	private readonly IServiceProvider serviceProvider;

	public MainPage(IServiceProvider service)
	{
		serviceProvider = service;
		var ctxt = serviceProvider.GetRequiredService<ArtistsDbContext>();
    ctxt.Database.EnsureCreated();
		InitializeComponent();

    //...
	}
  //...
}
```

À vous d'adapter ce code pour injecter votre ```DbContext``` dans une VM ou un service.
