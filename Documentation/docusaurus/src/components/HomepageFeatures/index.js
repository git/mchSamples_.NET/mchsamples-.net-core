import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'C# .NET Fundamentals',
    Svg: require('@site/static/img/csharp_original_logo_icon_146578.svg').default,
    description: (
      <>
        Samples from basics (classes, events, collections) to fundamentals (LINQ, persistance)
      </>
    ),
  },
  {
    title: 'Entity Framework Core',
    Svg : require('@site/static/img/EFCore.svg').default,
    description: (
      <>
        All about using EF Core with the Code First approach to create and consume a local database.
      </>
    ),
  },
  {
    title: 'Unit Tests',
    Svg: require('@site/static/img/xUnit.svg').default,
    description: (
      <>
        How to test your .NET code with xUnit?
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
