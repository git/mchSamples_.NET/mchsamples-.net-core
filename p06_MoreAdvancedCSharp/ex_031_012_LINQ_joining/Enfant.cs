﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Enfant.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_031_012_LINQ_joining
{
    class Enfant
    {
        public string Prénom
        {
            get;
            set;
        }

        public string NounoursPréféré
        {
            get;
            set;
        }
    }
}
