﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static System.Console;

namespace ex_031_012_LINQ_joining
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.UTF8;

            List<Nounours> nounours = new List<Nounours> {
                                      new Nounours("mouton", new DateTime(2009, 09, 16), 1000),
                                      new Nounours("ours", new DateTime(2009, 08, 15), 2000),
                                      new Nounours("chien", new DateTime(2009, 06, 13), 500),
                                      new Nounours("lapin", new DateTime(2009, 05, 12), 500),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 2000),
                                      new Nounours("macaque", new DateTime(2009, 04, 11), 500)};

            List<Enfant> enfants = new List<Enfant> {
                                        new Enfant {Prénom="Ulysse", NounoursPréféré = "chat" },
                                        new Enfant {Prénom="Télémaque", NounoursPréféré = "lapin" },
                                        new Enfant {Prénom="Pénélope", NounoursPréféré = "chien" } };
            ////////////
            //Joining //
            ////////////

            //Join : applique une stratégie de recherche pour faire correspondre des éléments de deux collections
            WriteLine("Join");
            var joined = nounours.Join(enfants, n => n.Nom, e => e.NounoursPréféré, (n, e) => new { Nounours = n.Nom, Enfant = e.Prénom });
            foreach (var j in joined)
            {
                WriteLine(j);
            }
            WriteLine();

            //Join en Query Syntax
            WriteLine("Join");
            var joinedQS = from n in nounours
                           join e in enfants on n.Nom equals e.NounoursPréféré
                           select new { Nounours = n.Nom, Enfant = e.Prénom };
            foreach (var j in joinedQS)
            {
                WriteLine(j);
            }
            WriteLine();

            //Zip operator : la fermeture éclair !
            string[] nombresEnLettres = { "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf" };
            int[] nombres = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var nombresEnChiffresEtEnLettres = nombres.Zip(nombresEnLettres, (i, s) => $"{i}. {s}");
            foreach (var s in nombresEnChiffresEtEnLettres)
            {
                WriteLine(s);
            }
        }
    }
}
