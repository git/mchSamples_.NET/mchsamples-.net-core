﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Droite.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_003_interface_as_parameter
{
    class Droite : IModifieur
    {
        public int Offset
        {
            get;
            private set;
        }

        public int Pente
        {
            get;
            private set;
        }

        public Droite(int offset, int pente)
        {
            Offset = offset;
            Pente = pente;
        }

        public int Modifier(int nombreAModifier)
        {
            return Offset + Pente * nombreAModifier;
        }
    }
}
