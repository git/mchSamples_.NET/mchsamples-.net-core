﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_028_003_interface_as_parameter
{
    class Program : IModifieur
    {
        static void Main(string[] args)
        {
            int[] tableau = { 1, 2, 3, 4, 5 };
            IModifieur m = new Program();
            Modifie(tableau, m);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();

            m = new Carré();
            Modifie(tableau, m);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();

            m = new Cube();
            Modifie(tableau, m);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();

            m = new Droite(2, 3);
            Modifie(tableau, m);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();
        }

        public int Modifier(int nombreAModifier)
        {
            return 2 * nombreAModifier;
        }

        public static void Modifie(int[] tab, IModifieur mod)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = mod.Modifier(tab[i]);
            }
        }
    }
}
