﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Cube.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_003_interface_as_parameter
{
    class Cube : IModifieur
    {
        public int Modifier(int nombreAModifier)
        {
            return nombreAModifier * nombreAModifier * nombreAModifier;
        }
    }
}
