﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using static System.Console;
using System.Linq;
using System.Collections.Generic;
using System;

namespace ex_031_016_LINQ_conversion_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            ////////////////////////
            //conversion methods  //
            ////////////////////////
            WriteLine("Conversion methods");

            //Cast : convertit une collection en une collection d'un certain type, si possible (sinon exception)
            //utile pour convertir une collection non typée en une collection typée
            WriteLine("Cast");
            List<object> trucs = new List<object> { 1, 2, 3 };
            IEnumerable<int> entiers = trucs.Cast<int>();
            foreach (int i in entiers) Write($"{i} ");
            WriteLine();
            WriteLine();

            //ToArray, ToList : convertit en tableau ou en List
            //ATTENTION : casse l'exécution différée
            WriteLine("ToArray, ToList");
            int[] tabEntiers = entiers.ToArray();
            List<int> listEntiers = entiers.ToList();
            foreach (var i in tabEntiers) Write($"{i} ");
            WriteLine();
            foreach (var i in listEntiers) Write($"{i} ");
            WriteLine();
            WriteLine();

            //ToDictionary : permet de construire un dictionnaire à partir d'une collection en spécifiant la clé et la valeur à partir de chaque item
            List<Nounours> nounours = new List<Nounours> {
                                      new Nounours("mouton", new DateTime(2009, 09, 16), 1000),
                                      new Nounours("ours", new DateTime(2009, 08, 15), 2000),
                                      new Nounours("chien", new DateTime(2009, 06, 13), 500),
                                      new Nounours("lapin", new DateTime(2009, 05, 12), 500),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 2000),
                                      new Nounours("macaque", new DateTime(2009, 04, 11), 500)};
            WriteLine("ToDictionary");
            Dictionary<string, Nounours> dico1 = nounours.ToDictionary(n => n.Nom);
            foreach (var kvp in dico1) WriteLine($"{kvp.Key} : {kvp.Value}");
            WriteLine();
            Dictionary<string, int> dico2 = nounours.ToDictionary(n => n.Nom, n => n.NbPoils);
            foreach (var kvp in dico2) WriteLine($"{kvp.Key} : {kvp.Value}");
            WriteLine();
            WriteLine();
        }
    }
}
