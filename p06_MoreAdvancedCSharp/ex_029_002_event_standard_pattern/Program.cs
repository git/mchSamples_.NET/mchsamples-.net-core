﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Text;
using static System.Console;

namespace Broadcaster_Subscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
            
            //déclaration et affectation d'un Broadcaster
            Broadcaster info_sport = new Broadcaster("info sport", "rien");

            //déclaration et abonnement de deux Subscriber
            Subscriber arthur = new Subscriber("Arthur");
            arthur.Subscribe(info_sport);
            Subscriber richard = new Subscriber("Richard");
            richard.Subscribe(info_sport);

            //génération d'une nouvelle info
            info_sport.Info = "Del Potro bat Federer en finale de l'US Open";
        }
    }
}

