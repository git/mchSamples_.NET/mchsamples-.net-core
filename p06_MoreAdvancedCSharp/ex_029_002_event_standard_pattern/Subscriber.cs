﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Subscriber.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;
using static System.Console;
namespace Broadcaster_Subscriber
{
    /// <summary>
    /// la classe dont les instances vont s'abonner à l'événement
    /// </summary>
    public class Subscriber
    {
        /// <summary>
        /// nom du subscriber
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// méthode ayant la même signature que l'événement, elle peut donc s'abonner.
        /// </summary>
        /// <param name="sender">celui qui génère l'événement</param>
        /// <param name="args">arguments de l'événement</param>
        public void ReceiveInfo(object sender, InfoChangedEventArgs args)
        {
            ForegroundColor = ConsoleColor.Red;
            Write(Name);
            ForegroundColor = ConsoleColor.White;
            Write(" a reçu l'info : ");
            ForegroundColor = ConsoleColor.DarkGreen;
            Write(args.NewInfo);
            ForegroundColor = ConsoleColor.White;
            Write(" à ");
            ForegroundColor = ConsoleColor.DarkGreen;
            Write(args.InfoDate.ToString("t"));
            ForegroundColor = ConsoleColor.White;
            Write(" provenant de ");
            ForegroundColor = ConsoleColor.Red;
            WriteLine((sender as Broadcaster).Name);
            ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="name">nom de l'abonné</param>
        public Subscriber(string name)
        {
            Name = name;
        }

        /// <summary>
        /// abonnement de cette instance de Subscriber à l'événement de Broadcaster
        /// </summary>
        /// <param name="br">le Broadcaster possédant l'événement auquel s'abonne ce Subscriber</param>
        public void Subscribe(Broadcaster br)
        {
            //abonnement, on peut utiliser += avec une méthode de cette instance de Subscriber
            br.InfoChanged += ReceiveInfo;
        }
    }
}

