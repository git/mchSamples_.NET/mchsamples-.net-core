﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;

namespace ex_031_007_LINQ_ordering
{
    class Nounours : IEquatable<Nounours>
    {
        public Nounours(string nom, DateTime naissance, int nbPoils)
        {
            Nom = nom;
            Naissance = naissance;
            NbPoils = nbPoils;
        }

        public string Nom
        {
            get;
            private set;
        }

        public DateTime Naissance
        {
            get;
            private set;
        }

        public int NbPoils
        {
            get;
            private set;
        }

        public override int GetHashCode()
        {
            return Nom.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            //check null
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return this.Equals(obj as Nounours);
        }

        public bool Equals(Nounours other)
        {
            return (this.Nom.Equals(other.Nom) && this.Naissance == other.Naissance);
        }

        public override string ToString()
        {
            return $"{Nom} ({Naissance.ToString("d")}, {NbPoils} poils)";
        }
    }
}
