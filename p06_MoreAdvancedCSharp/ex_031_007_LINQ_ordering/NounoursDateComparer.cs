﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : NounoursDateComparer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Collections.Generic;

namespace ex_031_007_LINQ_ordering
{
    /// <summary>
    /// Si on a besoin d'un autre compareur et qu'on n'a pas accès au code source, alors on utilise un IComparer générique externe
    /// </summary>
    class NounoursDateComparer : IComparer<Nounours>
    {
        public int Compare(Nounours x, Nounours y)
        {
            return x.Naissance.CompareTo(y.Naissance);
        }
    }
}
