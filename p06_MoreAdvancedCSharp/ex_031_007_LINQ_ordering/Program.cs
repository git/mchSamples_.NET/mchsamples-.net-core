﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace ex_031_007_LINQ_ordering
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Nounours> nounours = new List<Nounours> {
                                      new Nounours("mouton", new DateTime(2009, 09, 16), 1000),
                                      new Nounours("ours", new DateTime(2009, 08, 15), 2000),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 1000),
                                      new Nounours("chien", new DateTime(2009, 06, 13), 500),
                                      new Nounours("lapin", new DateTime(2009, 05, 12), 500),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 2000),
                                      new Nounours("macaque", new DateTime(2009, 04, 11), 500)};

            ////////////
            //Ordering//
            ////////////
            WriteLine("Ordering");

            //OrderBy
            WriteLine("OrderBy");
            IEnumerable<Nounours> ordered = nounours.OrderBy(n => n.Nom);
            foreach (Nounours n in ordered)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderBy en query syntax
            WriteLine("OrderBy");
            IEnumerable<Nounours> orderedQS = from n in nounours
                                              orderby n.Nom
                                              select n;
            foreach (Nounours n in orderedQS)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderBy + ThenBy
            WriteLine("OrderBy + ThenBy");
            IEnumerable<Nounours> orderedThenBy = nounours.OrderBy(n => n.NbPoils).ThenBy(n => n.Nom);
            foreach (Nounours n in orderedThenBy)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderBy + ThenBy en Query Syntax
            WriteLine("OrderBy + ThenBy");
            IEnumerable<Nounours> orderedThenByQS = from n in nounours
                                                    orderby n.NbPoils, n.Nom
                                                    select n;
            foreach (Nounours n in orderedThenByQS)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderBy + OrderBy : ATTENTION ! NE FAIT PAS CE QU'ON ATTEND DE LUI 
            WriteLine("OrderBy + OrderBy : le 2ème orderby casse le premier");
            IEnumerable<Nounours> orderedOrderBy = nounours.OrderBy(n => n.NbPoils).OrderBy(n => n.Nom);
            foreach (Nounours n in orderedOrderBy)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderByDescending et ThenByDescending
            WriteLine("OrderByDescending + ThenByDescending");
            IEnumerable<Nounours> orderedByDescendingThenByDescending = nounours.OrderByDescending(n => n.NbPoils).ThenByDescending(n => n.Nom);
            foreach (Nounours n in orderedByDescendingThenByDescending)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderByDescending et ThenByDescending en Query Syntax
            WriteLine("OrderByDescending + ThenByDescending");
            IEnumerable<Nounours> orderedByDescendingThenByDescendingQS = from n in nounours
                                                                          orderby n.NbPoils descending, n.Nom descending
                                                                          select n;
            foreach (Nounours n in orderedByDescendingThenByDescendingQS)
            {
                WriteLine(n);
            }
            WriteLine();

            //OrderBy + comparer
            WriteLine("OrderBy + Comparer");
            IEnumerable<Nounours> orderedCompared = nounours.OrderBy(n => n, new NounoursDateComparer());
            foreach (Nounours n in orderedCompared)
            {
                WriteLine(n);
            }
            WriteLine();
        }
    }
}
