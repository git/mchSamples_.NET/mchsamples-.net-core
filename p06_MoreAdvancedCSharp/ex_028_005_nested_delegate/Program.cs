﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;
using static System.Console;

using Namespace2;
using System.Text;

namespace Namespace1
{
    class Program
    {

        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
 
            //on déclare une instance d'un type délégué déclaré dans une autre classe, le reste fonctionne pareil
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            Write("utilisation d'une méthode ");
            ForegroundColor = ConsoleColor.Magenta;
            Write("statique ");
            ForegroundColor = ConsoleColor.White;
            Write("comme instance de délégué d'un type délégué d'une autre classe : ");
            ForegroundColor = ConsoleColor.Red;
            WriteLine("Double");
            ForegroundColor = ConsoleColor.DarkGreen;
            WriteLine("MaClasseDeMath.Modifieur m = Double;");
            WriteLine("int resultat = m(3)");
            ForegroundColor = ConsoleColor.Red;
            #endregion
            MaClasseDeMaths.Modifieur m = Double;
            int resultat = m(3);
            WriteLine("m(3) = " + resultat);
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.DarkGray;
            #endregion
            WriteLine($"m.Target == null ? {m.Target == null}");
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            WriteLine();
            #endregion



            //on affecte MaClasseDeMath.Carré (méthode static d'une autre classe que Main) à m
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            Write("utilisation d'une méthode ");
            ForegroundColor = ConsoleColor.Magenta;
            Write("statique d'une autre classe ");
            ForegroundColor = ConsoleColor.White;
            Write("comme instance de délégué d'un type délégué d'une autre classe : ");
            ForegroundColor = ConsoleColor.Red;
            WriteLine("MaClasseDeMath.Carré");
            ForegroundColor = ConsoleColor.DarkGreen;
            WriteLine("MaClasseDeMath.Modifieur m = MaClasseDeMath.Carré;");
            WriteLine("int resultat = m(3)");
            ForegroundColor = ConsoleColor.Red;
            #endregion
            m = MaClasseDeMaths.Carré;
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.DarkGray;
            #endregion
            WriteLine($"m.Target == null ? {m.Target == null}");
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            WriteLine();
            #endregion

            //on affecte Droite.Image (méthode non static d'une autre classe que Main) à m
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            Write("utilisation d'une méthode ");
            ForegroundColor = ConsoleColor.Magenta;
            Write("non statique d'une autre classe ");
            ForegroundColor = ConsoleColor.White;
            Write("comme instance de délégué d'un type délégué d'une autre classe : ");
            ForegroundColor = ConsoleColor.Red;
            WriteLine("Droite.Image");
            ForegroundColor = ConsoleColor.DarkGreen;
            WriteLine("Droite droite = new Droite(1, 2);");
            WriteLine("MaClasseDeMath.Modifieur m = droite.Image;");
            WriteLine("int resultat = m(3)");
            ForegroundColor = ConsoleColor.Red;
            #endregion
            Droite droite = new Droite(1, 2);
            m = droite.Image;
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.DarkGray;
            #endregion
            WriteLine($"m.Target == null ? {m.Target == null}");
            WriteLine($"m.Target : {m.Target}");
            #region Affichage des infos dans la Console
            ForegroundColor = ConsoleColor.White;
            WriteLine();
            #endregion
        }

        /// <summary>
        /// méthode static de type "Modifieur" car elle a la même signature que le délégué Modifieur
        /// </summary>
        /// <param name="x">un entier</param>
        /// <returns>le double de l'entier pris en paramètre</returns>
        static int Double(int x)
        {
            return x * 2;
        }
    }

}
