﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ConcreteSubscriber.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_028_011_Broadcaster_Subscriber
{
    class ConcreteSubscriber : ISubscriber
    {
        public string Name
        {
            get;
            private set;
        }

        InfoSportBroadcaster mCaster;

        public ConcreteSubscriber(string name, InfoSportBroadcaster caster)
        {
            Name = name;
            mCaster = caster;
        }

        public void Update()
        {
            WriteLine($"{Name} a reçu l'info {mCaster.LastInfo} le {mCaster.LastDateTime.ToString("d")} à {mCaster.LastDateTime.ToString("t")}");
        }
    }
}
