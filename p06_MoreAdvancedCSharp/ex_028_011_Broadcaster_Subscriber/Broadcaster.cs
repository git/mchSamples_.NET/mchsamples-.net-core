﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Broadcaster.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Collections.Generic;

namespace ex_028_011_Broadcaster_Subscriber
{
    class Broadcaster
    {
        List<ISubscriber> mSubscribers = new List<ISubscriber>();

        public void Attach(ISubscriber subscriber)
        {
            mSubscribers.Add(subscriber);
        }

        public void Detach(ISubscriber subscriber)
        {
            mSubscribers.Remove(subscriber);
        }

        public void Notify()
        {
            foreach (ISubscriber subscriber in mSubscribers)
            {
                subscriber.Update();
            }
        }
    }
}
