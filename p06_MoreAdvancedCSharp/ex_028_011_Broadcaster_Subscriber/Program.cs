﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Text;
using static System.Console;

namespace ex_028_011_Broadcaster_Subscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            InfoSportBroadcaster caster = new InfoSportBroadcaster();

            ConcreteSubscriber arthur = new ConcreteSubscriber("Arthur", caster);
            ConcreteSubscriber richard = new ConcreteSubscriber("Richard", caster);

            caster.Attach(arthur);
            caster.AddInfo("L'ASM bat le Stade Français 28 à 25");
            System.Threading.Thread.Sleep(2000);

            caster.Attach(richard);
            caster.AddInfo("Grenoble domine le Racing-Metro 27 à 13");
            System.Threading.Thread.Sleep(2000);

            caster.Detach(arthur);
            caster.AddInfo("La Nouvelle-Zélande gagne le Four Nations en battant l'Argentine 54 à 15");
            System.Threading.Thread.Sleep(2000);
        }
    }
}
