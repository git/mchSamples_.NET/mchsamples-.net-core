﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : InfoSportBroadcaster.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;

namespace ex_028_011_Broadcaster_Subscriber
{
    class InfoSportBroadcaster : Broadcaster
    {
        public string LastInfo
        {
            get
            {
                return mLastInfo;
            }
            private set
            {
                if (value != mLastInfo)
                {
                    mLastInfo = value;
                    Notify();
                }
            }
        }
        string mLastInfo;

        public DateTime LastDateTime
        {
            get
            {
                return mLastDateTime;
            }
            private set
            {
                if (value != mLastDateTime)
                {
                    mLastDateTime = value;
                }
            }
        }
        DateTime mLastDateTime;

        public void AddInfo(string info)
        {
            LastDateTime = DateTime.Now;
            LastInfo = info;
        }
    }
}
