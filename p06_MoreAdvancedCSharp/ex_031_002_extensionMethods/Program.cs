﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;
using System.Text;

namespace ex_031_002_extensionMethods
{
    //la classe doit être statique
    public static class MyStringAdds
    {
        //il faut utiliser le this dans la signature
        public static string Comment(this string s)
        {
            return $"/* {s} */";
        }

        public static string DeleteWhiteSpaces(this string s)
        {
            StringBuilder str = new StringBuilder(s);
            str.Replace(" ", "-");
            return str.ToString();
        }

        public static int Inverse(this int i)
        {
            return -i;
        }

        public static int CountNonWhiteSpacesCharacters(this string s, int index)
        {
            int nb = 0;
            for (int i = index; i < s.Length; i++)
            {
                if (s[i] != ' ')
                {
                    nb++;
                }
            }
            return nb;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            string s = "Voici une phrase inintéressante";
            WriteLine(s);
            WriteLine(s.Comment());
            WriteLine(s.DeleteWhiteSpaces());
            WriteLine(s.Comment().DeleteWhiteSpaces());
            WriteLine(s.DeleteWhiteSpaces().Comment());
            WriteLine(s.CountNonWhiteSpacesCharacters(0));
            WriteLine(s.CountNonWhiteSpacesCharacters(12));
            WriteLine(s.DeleteWhiteSpaces().CountNonWhiteSpacesCharacters(0).Inverse());
        }
    }
}
