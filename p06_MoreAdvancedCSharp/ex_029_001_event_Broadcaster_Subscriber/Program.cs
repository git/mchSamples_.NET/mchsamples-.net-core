﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Text;
using static System.Console;

namespace Broadcaster_Subscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
          
            //création d'un diffuseur
            Broadcaster info_sport = new Broadcaster("Info Sport");
            info_sport.Info = "Toulouse s'impose face à Toulon 32 à 9";

            //deux instances d'Subscriber s'abonnent à l'instance de délégué info_sport.Informer
            Subscriber arthur = new Subscriber("Arthur");
            arthur.Abonnement(info_sport);
            info_sport.Info = "Grenoble domine le Racing-Metro 27 à 13";

            Subscriber richard = new Subscriber("Richard");
            richard.Abonnement(info_sport);

            //une nouvelle info tombe, l'appel du setter de la propriété Broadcaster.Info va exécuter les méthodes pointées par l'instance 
            // de délégué info_sport.Informer, soit arthur.RecevoirInfo et richard.RecevoirInfo
            info_sport.Info = "L'ASM bat le Stade Français 28 à 25";
            //Informer("L'ASM bat le Stade Français 28 à 25", DateTime.Now);
            // <=> arthur.RecevoirInfo("L'ASM bat le Stade Français 28 à 25", DateTime.Now);
            //     richard.RecevoirInfo("L'ASM bat le Stade Français 28 à 25", DateTime.Now);
            WriteLine();

            //arthur désabonne richard et s'abonne tout seul
            arthur.DésabonnerToutLeMondeSaufMoi(info_sport);
            //un nouvelle info tombe, arthur reçoit l'info mais pas richard
            info_sport.Info = "La Nouvelle-Zélande gagne le Four Nations en battant l'Argentine 54 à 15";
        }
    }
}

