﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ex_031_010_LINQ_aggregation_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Nounours> nounours = new List<Nounours> {
                                      new Nounours("mouton", new DateTime(2009, 09, 16), 1000),
                                      new Nounours("ours", new DateTime(2009, 08, 15), 2000),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 1000),
                                      new Nounours("chien", new DateTime(2009, 06, 13), 500),
                                      new Nounours("lapin", new DateTime(2009, 05, 12), 500),
                                      new Nounours("chat", new DateTime(2009, 07, 14), 2000),
                                      new Nounours("macaque", new DateTime(2009, 04, 11), 500)};

            ///////////////////////
            //Aggregation Methods//
            ///////////////////////
            WriteLine("Aggregation Methods");

            //Count
            WriteLine("Count");
            int nbNounours = nounours.Count();
            WriteLine($"{nbNounours} nounours dans la collection");
            int nbNounours2000poils = nounours.Count(n => n.NbPoils == 2000);
            WriteLine($"{nbNounours2000poils} nounours de 2000 poils dans la collection");
            WriteLine();

            //Min, Max
            WriteLine("Min, Max");
            int[] numbers = { 8, 4, 13, 7, 4, 6, 7, 4 };
            int max = numbers.Max();
            WriteLine($"le plus grand nombre est {max}");
            int plusPetitNbPoils = nounours.Min(n => n.NbPoils); // avec une expression lambda, une projection est d'abord effectuée
            WriteLine($"le plus petit nb de poils est : {plusPetitNbPoils}");
            WriteLine();

            //Sum
            WriteLine("Sum");
            int sum = numbers.Sum();
            WriteLine($"la somme des nombres est {sum}");
            int sumNbPoils = nounours.Sum(n => n.NbPoils);
            WriteLine($"la somme des poils des nounours est : {sumNbPoils}");
            WriteLine();

            //Average
            WriteLine("Average");
            double avg = numbers.Average();
            WriteLine($"la moyenne des nombres est {avg:0.00}");
            double avgNbPoils = nounours.Average(n => n.NbPoils);
            WriteLine($"la moyenne des poils des nounours est : {sumNbPoils}");
            WriteLine();

            //Aggregate
            WriteLine("Aggregate");
            int aggregate = numbers.Aggregate(0, (somme, i) => somme + i);
            WriteLine($"la somme des nombres est {aggregate}");

            string nomsDesNounours = nounours.Aggregate(String.Empty, (chaîne, n) => chaîne + $"{n.Nom}, ");
            WriteLine(nomsDesNounours.Remove(nomsDesNounours.Length - 2));
            WriteLine();
        }
    }
}
