﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IProgression.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_008_multicast_et_interfaces
{
    public interface IProgression
    {
        int Progression // void SetProgression(int value)
        {
            set;
        }
    }
}
