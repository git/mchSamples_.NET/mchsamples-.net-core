﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ProgressBar.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;
using static System.Console;

namespace ex_028_008_multicast_et_interfaces
{
    class ProgressBar : IProgression
    {
        static int MaxWidth = WindowWidth - 1;

        public int Progression
        {
            set
            {
                WriteLine(String.Empty.PadLeft(value * MaxWidth / 100, '*'));
            }
        }
    }
}
