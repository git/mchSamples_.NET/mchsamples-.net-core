﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : AffichePourcentage.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_028_008_multicast_et_interfaces
{
    class AffichePourcentage : IProgression
    {
        public int Progression
        {
            set
            {
                WriteLine($"{value}%...");
            }
        }
    }
}
