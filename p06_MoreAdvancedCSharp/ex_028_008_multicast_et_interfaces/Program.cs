﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Text;
using static System.Console;

namespace ex_028_008_multicast_et_interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            WriteLine("Lancement du gros travail");
            ClasseQuiFaitDuGrosTravail cqfdgt = new ClasseQuiFaitDuGrosTravail();
            cqfdgt.GrosTravail();


            WriteLine();
            WriteLine("On attache une progression de type AffichePourcentage");
            WriteLine("Lancement du gros travail");
            IProgression p1 = new AffichePourcentage();
            cqfdgt.Attacher(p1);
            cqfdgt.GrosTravail();

            WriteLine();
            WriteLine("On attache une progression de type ProgressBar");
            WriteLine("Lancement du gros travail");
            IProgression p2 = new ProgressBar();
            cqfdgt.Attacher(p2);
            cqfdgt.GrosTravail();

            WriteLine();
            WriteLine("On détache une progression de type AffichePourcentage");
            WriteLine("Lancement du gros travail");
            cqfdgt.Détacher(p1);
            cqfdgt.GrosTravail();

            WriteLine();
            WriteLine("On détache une progression de type ProgressBar");
            cqfdgt.Détacher(p2);

            WriteLine("On attache une progression de type ClearProgression");
            WriteLine("On attache une progression de type ProgressBar");
            WriteLine("On attache une progression de type AffichePourcentage");
            WriteLine("Lancement du gros travail");
            cqfdgt.Attacher(new ClearProgression());
            cqfdgt.Attacher(new ProgressBar());
            cqfdgt.Attacher(new AffichePourcentage());
            cqfdgt.GrosTravail();
        }
    }
}
