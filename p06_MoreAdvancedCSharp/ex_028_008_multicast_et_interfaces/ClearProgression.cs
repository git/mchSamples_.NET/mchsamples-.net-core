﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : ClearProgression.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;

namespace ex_028_008_multicast_et_interfaces
{
    class ClearProgression : IProgression
    {
        public int Progression
        {
            set { Console.Clear(); }
        }
    }
}
