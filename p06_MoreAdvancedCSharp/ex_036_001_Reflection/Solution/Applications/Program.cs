﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-05-26
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using Animal;
using System.Linq;
using static System.Console;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            //permet d'accéder au dossier courant (ici l'endroit où se trouve l'exécutable)
            WriteLine(System.IO.Directory.GetCurrentDirectory());

            DirectoryInfo folder = new DirectoryInfo(Directory.GetCurrentDirectory().ToString()+"..\\..\\..\\..\\plugin\\net5.0\\");
            FileInfo[] files = folder.GetFiles();

            foreach (FileInfo file in files.Where(f => f.Extension.Equals(".dll")))
            {
                AnalyseDLL(file.FullName);
            }
        }

        static void AnalyseDLL(string filePath)
        {
            //chargement d'une dll dans ce dossier courant
            Assembly a = Assembly.LoadFile(filePath);

            //on récupère tous les types contenus dans cette dll dans un tableau "types"
            Type[] types = a.GetExportedTypes();

            //pour chacun de ces types :
            foreach (Type t in types)
            {
                //on écrit le nom du type (nom d'une classe par exemple)
                Write(t.Name);

                //teste si le type dérive de Mammifère ou non
                if(t.GetTypeInfo().IsSubclassOf(typeof(Animal.Mammifère)))
                {
                    //si le type dérive de Mammifère, on écrit le nom du type Mammifère entre parenthèses
                    Write(" (" + t.GetTypeInfo().BaseType.Name + ")\n");

                    //on appelle le constructeur du type (chat ou chien par exemple) et on le référence à l'aide d'une
                    //référence sur un Mammifère (chat et chien dérivent de mammifère)
                    Mammifère bitxo = Activator.CreateInstance(t) as Mammifère;

                    //on affiche à l'écran le membre mName du type concerné (minou pour chat, toutou pour chien)
                    WriteLine("      je m'appelle : " + bitxo.Name);
                }
                else
                {
                    //si le type ne dérive pas de Mammifère, alors on le dit
                    WriteLine(" ne dérive pas de Animal.Mammifère");
                }
                WriteLine();
            }
        }
    }
}
