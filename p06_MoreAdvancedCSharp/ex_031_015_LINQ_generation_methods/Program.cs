﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using static System.Console;
using System.Linq;

namespace ex_031_015_LINQ_generation_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            ////////////////////////
            //generation methods  //
            ////////////////////////
            WriteLine("Generation methods");

            //Range : prend une valeur de démarrage et un nombre de valeurs pour générer la collection
            WriteLine("Range");
            var ranged = Enumerable.Range(3, 4);
            foreach (var i in ranged) Write($"{i} ");
            WriteLine();

            //Repeat : prend une valeur à répéter et le nombre de répétitions pour générer la collection
            WriteLine("Repeat");
            var repeated = Enumerable.Repeat(3, 4);
            foreach (var i in repeated) Write($"{i} ");
            WriteLine();
        }
    }
}
