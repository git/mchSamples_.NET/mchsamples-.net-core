﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : GrosTravail.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-08
//
// ========================================================================

using System;

namespace nsUtils
{
    /// <summary>
    /// une classe qui fait un gros travail ...
    /// </summary>
    public class ClasseQuiFaitDuGrosTravail
    {
        /// <summary>
        /// instance de délégué
        /// </summary>
        private Action<int> InstanceProgression;

        public void Initialiser(Action<int> prog)
        {
            InstanceProgression = prog;
        }

        public void Attacher(Action<int> prog)
        {
            InstanceProgression += prog;
        }

        public void Détacher(Action<int> prog)
        {
            InstanceProgression -= prog;
        }

        /// <summary>
        /// méthode statique qui appelle les méthodes pointées par la méthode p de type délégué Progression toutes les demi-secondes
        /// </summary>
        /// <param name="p">instance de délégué de type délégué "Progression" qui sera appelée à chaque dem-seconde</param>
        public void GrosTravail()
        {
            for (int i = 0; i <= 100; i++)
            {
                //exécution de l'instance de délégué
                InstanceProgression?.Invoke(i);
                //la ligne précédente est exactement équivalente à :
                //if (InstanceProgression != null)
                //{
                //    InstanceProgression(i);
                //}
                System.Threading.Thread.Sleep(50);
            }
        }
    }
}
