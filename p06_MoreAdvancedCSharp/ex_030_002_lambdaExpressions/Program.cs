﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_030_002_lambdaExpressions
{
    class Program
    {
        delegate int Modification(int i);

        delegate int Modification2(int i, int j);


        static void Main(string[] args)
        {
            Modification carré = x => x * x;
            WriteLine(carré(3));

            //équivalent à : 
            //Modification carré = Carré
            //WriteLine(carré(3));

            Modification2 multi = (i, j) => i * j;
            Modification2 somme = (i, j) => i + j;
            WriteLine(multi(1, 2));
            WriteLine(somme(1, 2));



            //on affecte une expression lambda à m
            Modification m = i => i * 2;
            int resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");

            //on affecte une autre expression lambda à m
            m = i => i * i;
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");

            //on affecte à m une autre expression lambda utilisant des variables locales à Program 
            int offset = 1, pente = 2;
            m = i => offset + pente * i;
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");
            WriteLine($"m.Target : {m.Target}");
        }
    }
}
