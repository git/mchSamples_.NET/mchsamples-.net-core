﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ex_031_005_LINQ_deferredExecution
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            List<Nounours> nounours = new List<Nounours> {
                                      new Nounours("mouton", new DateTime(2009, 09, 16)),
                                      new Nounours("ours", new DateTime(2009, 08, 15)),
                                      new Nounours("chat", new DateTime(2009, 07, 14)),
                                      new Nounours("chien", new DateTime(2009, 06, 13)),
                                      new Nounours("lapin", new DateTime(2009, 05, 12)),
                                      new Nounours("macaque", new DateTime(2009, 04, 11))};

            //définition de la requête
            //ATTENTION : ici, la requête est définie, mais n'est pas exécutée !
            var filteredNounours = nounours.Where(n => n.Nom.StartsWith("c"));

            //ici, nous avons besoin du résultat de la requête, donc elle est exécutée maintenant
            //c'est l'exécution différée
            WriteLine("1ère exécution de la requête");
            foreach (var n in filteredNounours)
            {
                WriteLine($"\t{n.Nom}");
            }
            WriteLine();

            //on ajoute un nouveau nounours
            nounours.Add(new Nounours("canari", DateTime.Today));

            //notez qu'ici, nous ne redéfinissons pas la requête,
            //elle est juste exécutée à nouveau, mais comme la collection sur laquelle elle est construite a changé,
            //alors le résultat de la requête également
            //=> exécution différée
            WriteLine("2ème exécution de la requête");
            foreach (var n in filteredNounours)
            {
                WriteLine($"\t{n.Nom}");
            }
            WriteLine();
        }
    }
}
