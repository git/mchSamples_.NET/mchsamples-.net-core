﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;

namespace ex_031_005_LINQ_deferredExecution
{
    class Nounours
    {
        public Nounours(string nom, DateTime naissance)
        {
            Nom = nom;
            Naissance = naissance;
        }

        public string Nom
        {
            get;
            private set;
        }

        public DateTime Naissance
        {
            get;
            private set;
        }
    }
}
