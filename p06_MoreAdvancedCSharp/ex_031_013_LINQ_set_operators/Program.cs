﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace ex_031_013_LINQ_set_operators
{
    class Program
    {
        static void Main(string[] args)
        {
            /////////////////
            //set operators//
            /////////////////
            WriteLine("Set operators");

            int[] séquence1 = { 1, 2, 3 };
            int[] séquence2 = { 3, 4, 5 };

            //Concat : tous les éléments de la séquence 1, puis tous les éléments de la séquence 2
            WriteLine("Concat");
            IEnumerable<int> concat = séquence1.Concat(séquence2);
            foreach (int n in concat)
            {
                Write($"{n} ");
            }
            WriteLine();

            //Union : tous les éléments de la séquence 1, puis tous les éléments de la séquence 2 (sans doublons)
            WriteLine("Union");
            IEnumerable<int> union = séquence1.Union(séquence2);
            foreach (int n in union)
            {
                Write($"{n} ");
            }
            WriteLine();

            //Intersect : tous les éléments qui sont dans la séquence 1 ET dans la séquence 2
            WriteLine("Intersect");
            IEnumerable<int> intersect = séquence1.Intersect(séquence2);
            foreach (int n in intersect)
            {
                Write($"{n} ");
            }
            WriteLine();

            //Except : tous les éléments de la séquence 1 SAUF ceux qui sont aussi dans la séquence 2
            WriteLine("Except");
            IEnumerable<int> except = séquence1.Except(séquence2);
            foreach (int n in except)
            {
                Write($"{n} ");
            }
            WriteLine();
            IEnumerable<int> except2 = séquence2.Except(séquence1);
            foreach (int n in except2)
            {
                Write($"{n} ");
            }
            WriteLine();
        }
    }
}
