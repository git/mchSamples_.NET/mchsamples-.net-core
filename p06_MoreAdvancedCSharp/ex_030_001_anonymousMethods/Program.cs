﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System.Text;
using static System.Console;

namespace ex_030_001_anonymousMethods
{
    class Program
    {
        /// <summary>
        /// le délégué 
        /// </summary>
        /// <param name="x">un entier</param>
        /// <returns>un autre entier dépendant du paramètre et de l'exécution de la fonction de type Modifieur</returns>
        delegate int Modifieur(int x);

        delegate int Opération(int x, int y);

        /// <summary>
        /// ici, on a une méthode "m" de type "Modifieur" (le délégué juste au-dessus)
        /// et on l'affecte avec différentes "valeurs" (ici méthodes) possibles.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
         
            //on affecte une méthode anonyme à m
            Modifieur m = delegate (int x) { return x * 2; };
            int resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");

            //on affecte une autre méthode anonyme à m
            m = delegate (int x) { return x * x; };
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");

            //on affecte à m une méthode anonyme utilisant des variables locales
            int offset = 1, pente = 2;
            m = delegate (int x) { return offset + pente * x; };
            resultat = m(3);
            WriteLine($"m(3) = {resultat}");
            WriteLine($"m.Target == null ? {m.Target == null}");
            WriteLine($"m.Target : {m.Target}");

            Opération o = delegate (int x, int y) { return x * y; };
            WriteLine($"2x3 = {o(2, 3)}");

            o = delegate (int x, int y) { return y - 2; };
            WriteLine($"3-2 = {o(2, 3)}");
        }
    }
}
