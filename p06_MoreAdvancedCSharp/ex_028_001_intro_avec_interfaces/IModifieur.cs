﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IModifieur.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_001_intro_avec_interfaces
{
    public interface IModifieur
    {
        int Modifier(int nombreAModifier);
    }
}
