﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_028_001_intro_avec_interfaces
{
    class Program : IModifieur
    {
        static void Main(string[] args)
        {
            IModifieur m = new Program();
            WriteLine(m.Modifier(5));

            m = new Carré();
            WriteLine(m.Modifier(5));

            m = new Cube();
            WriteLine(m.Modifier(5));

            m = new Droite(2, 3);
            WriteLine(m.Modifier(5));
        }

        public int Modifier(int nombreAModifier)
        {
            return 2 * nombreAModifier;
        }
    }
}
