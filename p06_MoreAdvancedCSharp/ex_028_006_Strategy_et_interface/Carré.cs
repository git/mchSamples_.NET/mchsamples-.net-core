﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Carré.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_006_Strategy_et_interface
{
    class Carré : IModifieur
    {
        public int Modifier(int nombreAModifier)
        {
            return nombreAModifier * nombreAModifier;
        }
    }
}
