﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using static System.Console;

namespace ex_028_006_Strategy_et_interface
{
    class Program
    {
        IModifieur mModifieur;

        public Program(IModifieur modifieur)
        {
            mModifieur = modifieur;
        }

        public void Modifie(int[] tab)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = mModifieur.Modifier(tab[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] tableau = { 1, 2, 3, 4, 5 };
            Program p = new Program(new Carré());
            p.Modifie(tableau);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();

            p = new Program(new Cube());
            p.Modifie(tableau);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();

            p = new Program(new Droite(2, 3));
            p.Modifie(tableau);
            foreach (int i in tableau)
            {
                Write($"{i} ");
            }
            WriteLine();
        }


    }
}
