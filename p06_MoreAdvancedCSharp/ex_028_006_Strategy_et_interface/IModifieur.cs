﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : IModifieur.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

namespace ex_028_006_Strategy_et_interface
{
    public interface IModifieur
    {
        int Modifier(int nombreAModifier);
    }
}
