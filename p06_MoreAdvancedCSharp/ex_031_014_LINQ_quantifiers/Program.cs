﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-04
//
// ========================================================================

using static System.Console;
using System.Linq;

namespace ex_031_014_LINQ_quantifiers
{
    class Program
    {
        static void Main(string[] args)
        {
            /////////////////
            //quantifiers  //
            /////////////////
            WriteLine("Quantifiers");

            int[] séquence1 = { 2, 4, 6 };
            int[] séquence2 = { 3, 6, 9 };

            //Contains : retourne true si la collection contient l'élément donné
            WriteLine("Contains");
            Write(séquence1.Contains(2) ? "true" : "false");
            Write(séquence2.Contains(2) ? "true" : "false");
            WriteLine();

            //Any : retourne true si au moins 1 élément de la collection vérifie le prédicat
            WriteLine("Any");
            Write(séquence1.Any(i => i % 2 == 0) ? "true" : "false");
            Write(séquence2.Any(i => i % 2 == 0) ? "true" : "false");
            Write(séquence2.Where(i => i % 2 == 0).Any() ? "true" : "false");
            WriteLine();

            //All : retourne true si tous les éléments de la collection vérifient le prédicat
            WriteLine("Any");
            Write(séquence2.All(i => i % 2 == 0) ? "true" : "false");
            Write(séquence2.All(i => i % 3 == 0) ? "true" : "false");
            WriteLine();

            //SequenceEqual : retourne true si les deux collections contiennent le même nombre d'éléments, égaux, et dans le même ordre
            WriteLine("SequenceEqual");
            Write(Enumerable.SequenceEqual(séquence1, séquence2) ? "true" : "false");
        }
    }
}
