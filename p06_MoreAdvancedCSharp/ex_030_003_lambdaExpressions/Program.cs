﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-03
//
// ========================================================================

using System;
using static System.Console;
using nsUtils;
using System.Text;

namespace ex_030_003_lambdaExpressions
{
    class Program
    {
        //ex_04_009 avec des expressions lambda
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            ClasseQuiFaitDuGrosTravail travail = new ClasseQuiFaitDuGrosTravail();
            //travail.GrosTravail();
            WriteLine("fini");

            //on prépare une instance de délégué du type délégué Progression pointant sur une seule expression lambda,
            //et on appelle la méthode GrosTravail avec cette instance en paramètre.
            WriteLine("PREMIER APPEL");
            ClasseQuiFaitDuGrosTravail.Progression affiche = pourcentage => WriteLine($"{pourcentage}% effectués...");
            travail.Attacher(affiche);
            //travail.GrosTravail();

            //on ajoute une instance de délégué à notre "pointeur" : une autre expression lambda.
            //L'instance de délégué p pointe maintenant sur deux méthodes qui sont exécutées l'une après l'autre à chaque appel.
            //On appelle la méthode GrosTravail avec cette instance en paramètre.
            WriteLine("DEUXIEME APPEL");
            travail.Attacher(pourcentage => WriteLine(String.Empty.PadLeft(pourcentage * (WindowWidth - 1) / 100, '*')));
            //travail.GrosTravail();

            //on retire une instance de délégué à notre "pointeur", mais ce n'est plus possible avec les méthodes anonymes, même si on retape la même !
            WriteLine("TROISIEME APPEL");
            travail.Détacher(affiche);
            //travail.GrosTravail();

            //on réinitialise p avec une nouvelle expression lambda, puis on en ajoute 2 autres
            travail.Initialiser(pourcentage => Clear());
            travail.Attacher(pourcentage => WriteLine(String.Empty.PadLeft(pourcentage * (WindowWidth - 1) / 100, '*')));
            travail.Attacher(pourcentage => WriteLine($"{pourcentage}% effectués..."));
            travail.GrosTravail();
        }
    }
}
