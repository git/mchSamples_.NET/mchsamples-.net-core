﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_017_001_heritage_syntaxe
{
    //classe mère
    //notez que les champs private ne sont pas accessibles par les classes filles,
    //alors que les champs protected sont toujours invisibles de l'extérieur, mais visibles dans les classes filles
    class MediaObjet
    {
        protected string mTitre;
        protected int mAnnéeCréation;

        public MediaObjet(string titre, int année)
        {
            mTitre = titre;
            mAnnéeCréation = année;
        }

        public void Affiche()
        {
            WriteLine($"Ce média s'intitule {mTitre} et a été créé en {mAnnéeCréation}");
        }
    }

    //une classe fille
    class Disque : MediaObjet
    {
        private string mArtiste;

        //notez de quelle manière on appelle le constructeur de la classe mère avec base
        public Disque(string titre, int année, string artiste)
            : base(titre, année)
        {
            mArtiste = artiste;
        }

        //méthode redéfinie
        public void Affiche()
        {
            WriteLine($"Le disque {mTitre} a été enregistré par {mArtiste} en {mAnnéeCréation}");
        }
    }

    //une autre classe fille
    class Livre : MediaObjet
    {
        private string mAuteur;

        public Livre(string titre, int année, string auteur)
            : base(titre, année)
        {
            mAuteur = auteur;
        }

        public void Affiche()
        {
            WriteLine($"Le livre {mTitre} a été écrit par {mAuteur} en {mAnnéeCréation}");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Livre l = new Livre("Attenti al Gorilla", 1995, "Sandrone Dazieri");
            Disque d = new Disque("Underground", 2007, "Chris Potter");
            MediaObjet mo = new MediaObjet("La Grande Vadrouille", 1966);

            l.Affiche();
            d.Affiche();
            mo.Affiche();
        }
    }
}
