﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_016_004_readonly
{
    /// <summary>
    /// classe représentant un disque
    /// </summary>
    class Disque
    {
        /// <summary>
        /// artiste du disque (en readonly) (public pour la simplicité de l'exemple !)
        /// </summary>
        public readonly Artiste mArtiste;

        /// <summary>
        /// année de sortie (en readonly) (public pour la simplicité de l'exemple !)
        /// </summary>
        public readonly int mAnnée;

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="artiste"></param>
        /// <param name="année"></param>
        public Disque(Artiste artiste, int année)
        {
            mArtiste = artiste;
            mAnnée = année;
        }
    }

    /// <summary>
    /// classe représentant un artiste
    /// </summary>
    class Artiste
    {
        /// <summary>
        /// nom de l'artiste (public pour la simplicité de l'exemple !)
        /// </summary>
        public string mName;
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Disque d = new Disque(new Artiste { mName = "Miles Davis" }, 1956);
            WriteLine($"artiste : {d.mArtiste.mName}; année : {d.mAnnée}");
            //d.mAnnée = 1951; // ne compile pas (pas d'assignation possible car readonly)
            //d.mArtiste = new Artiste { mName = "Charlie Parker" };// ne compile pas (pas d'assignation possible car readonly)
            d.mArtiste.mName = "Charlie Parker"; //compile par l'adresse mArtiste n'a pas changé (readonly), mais le contenu pointé par la référence
            // n'est pas en readonly et peut donc être modifié
            WriteLine($"artiste : {d.mArtiste.mName}; année : {d.mAnnée}");
        }
    }
}
