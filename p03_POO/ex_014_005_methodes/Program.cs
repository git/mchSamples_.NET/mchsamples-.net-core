﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using static System.Console;

namespace ex_014_005_methodes
{
    internal class Disque
    {
        //une méthode est définie juste après sa déclaration
        //elle peut avoir aussi les modifieurs public, protected, private, internal
        //elle peut être surchargée, tant que les paramètres sont différents (ref et out comptent)
        public void Affiche()
        {
            WriteLine("Affiche()");
        }
        public void Affiche(int bof)
        {
            WriteLine("Affiche(int bof)");
        }
        public void Affiche(ref int bof)
        {
            WriteLine("Affiche(ref int bof)");
        }

        //constructeurs 
        //une classe peut avoir un ou plusieurs constructeurs, qui peuvent s'appeler à l'aide du mot clé this
        //le constructeur sans paramètre est le constructeur par défaut
        public Disque()
        {
        }
    }

    class Artiste
    {
        //rien pour l'instant...
    }

    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            Disque d = new Disque();
            d.Affiche();
            d.Affiche(a);
            d.Affiche(ref a);
        }
    }
}
