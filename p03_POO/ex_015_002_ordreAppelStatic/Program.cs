﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using static System.Console;

namespace ex_015_002_ordreAppelStatic
{
    class SansIntérêt
    {
        static int n = 0;

        public SansIntérêt(string nom)
        {
            n++;
            WriteLine($"exécution du constructeur de l'instance sans intérêt n°{n}({nom})");
        }
    }

    class UneClasse
    {
        public static SansIntérêt mMembreStatique1 = new SansIntérêt("membre statique 1");
        public static SansIntérêt mMembreStatique2 = new SansIntérêt("membre statique 2");
        static UneClasse()
        {
            WriteLine("exécution du constructeur par défaut statique de UneClasse");
        }
        public SansIntérêt mMembreNonStatique1 = new SansIntérêt("membre instance 1");

        public SansIntérêt mMembreNonStatique2 = new SansIntérêt("membre instance 2");



        public UneClasse()
        {
            WriteLine("exécution du constructeur de l'instance de UneClasse");
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            UneClasse c = new UneClasse();
        }
    }
}
