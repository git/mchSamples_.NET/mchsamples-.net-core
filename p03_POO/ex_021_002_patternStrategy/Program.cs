﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_021_002_patternStrategy
{
    interface IStrategy
    {
        void Operation();
    }

    class StrategyA : IStrategy
    {
        public void Operation()
        {
            WriteLine("Je fais le travail demandé en faisant \"wouf wouf\"");
        }
    }

    class StrategyB : IStrategy
    {
        public void Operation()
        {
            WriteLine("Je fais le travail demandé en faisant \"miaou miaou\"");
        }
    }

    class Program
    {
        static IStrategy maStrategie;

        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            WriteLine("J'utilise la stratégie A pour faire le travail");

            maStrategie = new StrategyA();
            maStrategie.Operation();

            WriteLine("J'utilise la stratégie B pour faire le travail");

            maStrategie = new StrategyB();
            maStrategie.Operation();
        }
    }
}
