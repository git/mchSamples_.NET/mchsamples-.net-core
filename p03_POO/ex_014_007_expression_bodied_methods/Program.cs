﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using static System.Console;

namespace ex_014_007_expression_bodied_methods
{
    class Rectangle
    {
        float mLargeur;
        float mLongueur;

        public Rectangle(float largeur, float longueur)
        {
            mLargeur = largeur;
            mLongueur = longueur;
        }

        /// <summary>
        /// une méthode simple ne renvoyant rien (void) et prenant un paramètre
        /// </summary>
        /// <param name="largeur">nouvelle largeur</param>
        public void ChangeLargeur(float largeur)
        {
            mLargeur = largeur;
        }

        /// <summary>
        /// une méthode très similaire à la précédente (ne rendant rien et prenant un paramètre) mais écrite avec une expression 
        /// Notez la flèche => 
        /// Marche bien pour les méthodes avec une seule instruction
        /// </summary>
        /// <param name="longueur">nouvelle longueur</param>
        public void ChangeLongeur(float longueur) => mLongueur = longueur;

        /// <summary>
        /// méthode simple ne prenant rien et rendant un float
        /// </summary>
        /// <returns>l'aire de ce Rectangle</returns>
        public float CalculeAire()
        {
            return mLargeur * mLongueur;
        }

        /// <summary>
        /// une méthode très similaire à la précédente (ne prenant rien et rendant un float) mais écrite avec une expression
        /// Notez la flèche =>
        /// Notez l'absence du mot-clef return
        /// Marche bien pour les méthodes avec une seule instruction
        /// </summary>
        /// <returns>le périmètre de ce Rectangle</returns>
        public float CalculePérimètre() => 2 * (mLargeur + mLongueur);
    }
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Rectangle r = new Rectangle(2, 3);
            WriteLine("Rectangle r = new Rectangle(2, 3);");
            WriteLine($"r.CalculeAire() : {r.CalculeAire()} u²");
            WriteLine($"r.CalculePérimètre() : {r.CalculePérimètre()} u");
            r.ChangeLargeur(3);
            r.ChangeLongeur(4);
            WriteLine("r.ChangeLargeur(3);");
            WriteLine("r.ChangeLongeur(4);");
            WriteLine($"r.CalculeAire() : {r.CalculeAire()} u²");
            WriteLine($"r.CalculePérimètre() : {r.CalculePérimètre()} u");

            WriteLine();
            WriteLine("où CalculeAire() est écrite classiquement :");
            WriteLine("public float CalculeAire() { return mLargeur * mLongueur; }");
            WriteLine();
            WriteLine("et CalculePérimètre() est écrite avec une expression :");
            WriteLine("public float CalculePérimètre() => 2*(mLargeur+mLongueur);");
            WriteLine();
            WriteLine("ChangeLargeur() est écrite classiquement :");
            WriteLine("public void ChangeLargeur(float largeur) { mLargeur = largeur; }");
            WriteLine();
            WriteLine("et ChangeLongeur() est écrite avec une expression :");
            WriteLine("public void ChangeLongeur(float longueur) => mLongueur = longueur;");
        }
    }
}
