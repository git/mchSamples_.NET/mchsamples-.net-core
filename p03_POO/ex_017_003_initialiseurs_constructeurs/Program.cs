﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_017_003_initialiseurs_constructeurs
{
    class UneClasse
    {
        public UneClasse(string info)
        {
            WriteLine($"appel du constructeur de UneClasse {info}");
        }
    }

    class BaseClass
    {
        static UneClasse une_classe = new UneClasse("(depuis un membre statique de BaseClass) ");

        UneClasse une_classe2 = new UneClasse("(depuis un membre d'une instance de BaseClass) ");

        static BaseClass()
        {
            WriteLine("appel du constructeur statique de BaseClass");
        }

        public BaseClass()
        {
            WriteLine("appel du constructeur d'une instance de BaseClass");
        }
    }

    class ChildClass : BaseClass
    {
        static UneClasse une_classe3 = new UneClasse(" (depuis un membre statique de ChildClass)");

        UneClasse une_classe4 = new UneClasse(" (depuis un membre d'une instance de ChildClass) ");

        static ChildClass()
        {
            WriteLine("appel du constructeur statique de ChildClass");
        }

        public ChildClass()
        {
            WriteLine("appel du constructeur d'une instance de ChildClass");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            ChildClass c = new ChildClass();
        }
    }
}
