﻿// ========================================================================
//
// Copyright (C) 2017-2018 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2017-09-28
//
// ========================================================================

using System;
using static System.Console;

namespace ex_014_008_deconstruction
{
    /// <summary>
    /// NOUVEAUTE C# 7.0
    /// 
    /// Comme les Tuples (cf. ex_004_009), n'importe quel type peut-être déconstruit, à l'aide d'une méthode de déconstruction
    /// Dans la classe Nounours ci-dessous, la méthode Decontruct possède des variables en paramètres de sortie out permettant de déconstruire une instance de Nounours.
    /// Le Main ci-dessous l'utilise.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            //construction
            Nounours monNounours = new Nounours("Chucky", 4, new DateTime(1988, 11, 9));

            //déconstruction
            monNounours.Deconstruct(out string leNom, out int leNbPoils, out DateTime laDateDeNaissance);

            WriteLine($"{leNom} a {leNbPoils} poils et est né en {laDateDeNaissance.Year}");

            //on peut également ignorer certaines variables avec _ (discard)
            monNounours.Deconstruct(out string leNom2, out _, out DateTime laDateDeNaissance2);
            WriteLine($"{leNom2} est né en {laDateDeNaissance2.Year}");
        }
    }

    class Nounours
    {
        private string nom;

        private int nbPoils;

        private DateTime naissance;

        public Nounours(string _nom, int _nbPoils, DateTime _naissance)
        {
            nom = _nom;
            nbPoils = _nbPoils;
            naissance = _naissance;
        }

        public void Deconstruct(out string outNom, out int outNbPoils, out DateTime outNaissance)
        {
            outNom = nom;
            outNbPoils = nbPoils;
            outNaissance = naissance;
        }
    }
}
