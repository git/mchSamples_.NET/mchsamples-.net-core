﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using System;
using static System.Console;

namespace ex_017_004_casting
{
    class BaseClass
    {
        public string Affiche()
        {
            return "je suis une instance de BaseClass";
        }
    }

    class ChildClass : BaseClass
    {
        public string Affiche()
        {
            return "je suis une instance de ChildClass";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;
     
            //upcasting : implicite
            WriteLine("Upcasting");
            ChildClass c = new ChildClass();
            WriteLine(c.Affiche());
            BaseClass b = c;
            WriteLine(b.Affiche());
            WriteLine();

            //downcasting (C-like) : explicite
            WriteLine("Downcasting (C-like)");
            BaseClass b2 = new BaseClass();
            c = (ChildClass)b; //pas d'erreur
            WriteLine(c.Affiche());
            try
            {
                c = (ChildClass)b2; // erreur, b2 n'est pas une ChildClass
                WriteLine(c.Affiche());
            }
            catch (Exception e)
            {
                WriteLine(e.Message);
            }
            WriteLine();

            //downcasting en C# : explicite
            WriteLine("Downcasting en C# avec is et as");
            WriteLine($"b  is ChildClass ? {b is ChildClass}");
            WriteLine($"b2 is ChildClass ? {b2 is ChildClass}");

            ChildClass c2 = b as ChildClass;
            if (c2 != null)
            {
                WriteLine(c2.Affiche());
            }
            else
            {
                WriteLine("c2 n'est pas de type ChildClass");
            }
            c2 = b2 as ChildClass;
            if (c2 != null)
            {
                WriteLine(c2.Affiche());
            }
            else
            {
                WriteLine("c2 n'est pas de type ChildClass");
            }
        }
    }
}
