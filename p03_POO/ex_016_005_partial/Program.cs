﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_016_005_partial
{
    class Program
    {
        static void Main(string[] args)
        {
            MaClasse c = new MaClasse();
            c.N1 = 1;
            c.N2 = 2;
            c.N3 = 3;
            WriteLine(c);
        }
    }
}
