﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MaClasse.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

namespace ex_016_005_partial
{
    partial class MaClasse
    {
        public int N1
        {
            get;
            set;
        }
        public int N2
        {
            get;
            set;
        }

        public override string ToString()
        {
            return $"{N1} {N2} {N3}";
        }

    }
}
