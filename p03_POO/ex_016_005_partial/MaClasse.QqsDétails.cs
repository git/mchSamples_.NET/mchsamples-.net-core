﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MaClasse.QqsDétails.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

namespace ex_016_005_partial
{
    partial class MaClasse
    {
        public int N3
        {
            get;
            set;
        }

    }
}
