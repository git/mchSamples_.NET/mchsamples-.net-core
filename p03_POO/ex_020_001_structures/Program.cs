﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-26
//
// ========================================================================

using static System.Console;

namespace ex_020_001_structures
{
    /// <summary>
    /// présentation rapide des structures en C# : 
    /// - les variables structurées sont de type "valeur" (allouées en mémoire sur la pile, plus rapide que sur le tas, mais moins de place)
    /// - pas d'héritage possible (à part d'object (implicite) et System.ValueType)
    /// - pas de constructeur par défaut et pas d'initialiseur dans la définition des champs.
    /// - pas de membres, propriétés ou méthodes virtuelles
    /// </summary>
    struct StructDisque
    {
        public string Titre { get; set; }

        public int Année { get; set; }

        public StructDisque(string titre, int année)
        {
            Titre = titre;
            Année = année;
        }
    }

    class ClassDisque
    {
        public string Titre { get; set; }

        public int Année { get; set; }

        public ClassDisque(string titre, int année)
        {
            Titre = titre;
            Année = année;
        }
    }

    /// <summary>
    /// utilisation d'une structure
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            {
                WriteLine("Avec des structures :");
                StructDisque sd1 = new StructDisque("Voice In The Night", 1999);
                int year = sd1.Année;
                string title = sd1.Titre;
                WriteLine($"sd1 : {sd1.Titre} ({sd1.Année})");
                WriteLine();

                StructDisque sd2 = new StructDisque("The Water Is Wide", 2000);
                year = sd2.Année;
                title = sd2.Titre;
                WriteLine($"sd2 : {sd2.Titre} ({sd2.Année})");
                WriteLine();

                sd2 = sd1;
                WriteLine("sd2 = sd1;");
                WriteLine($"sd1 : {sd1.Titre} ({sd1.Année})");
                WriteLine($"sd2 : {sd2.Titre} ({sd2.Année})");
                WriteLine();

                WriteLine("modification de sd1");
                sd1.Titre = "Hyperion With Higgins";
                sd1.Année = 2001;
                WriteLine($"sd1 : {sd1.Titre} ({sd1.Année})");
                WriteLine($"sd2 : {sd2.Titre} ({sd2.Année})");
                WriteLine();
            }

            {
                WriteLine("Avec des classes :");
                ClassDisque cd1 = new ClassDisque("Voice In The Night", 1999);
                int year = cd1.Année;
                string title = cd1.Titre;
                WriteLine($"cd1 : {cd1.Titre} ({cd1.Année})");
                WriteLine();

                ClassDisque cd2 = new ClassDisque("The Water Is Wide", 2000);
                year = cd2.Année;
                title = cd2.Titre;
                WriteLine($"cd2 : {cd2.Titre} ({cd2.Année})");
                WriteLine();

                cd2 = cd1;
                WriteLine("cd2 = cd1;");
                WriteLine($"cd1 : {cd1.Titre} ({cd1.Année})");
                WriteLine($"cd2 : {cd2.Titre} ({cd2.Année})");
                WriteLine();

                WriteLine("modification de cd1");
                cd1.Titre = "Hyperion With Higgins";
                cd1.Année = 2001;
                cd1 = new ClassDisque("Hyperion With Higgins", 2001);
                WriteLine($"cd1 : {cd1.Titre} ({cd1.Année})");
                WriteLine($"cd2 : {cd2.Titre} ({cd2.Année})");
                WriteLine();
            }
        }
    }
}
