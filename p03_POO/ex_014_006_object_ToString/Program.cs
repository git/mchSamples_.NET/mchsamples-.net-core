﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-23
//
// ========================================================================

using static System.Console;

namespace ex_014_006_object_ToString
{
    //toutes les classes descendent de la classe Object. 
    //Object contient un constructeur, et les méthodes : Equals, GetType, et ToString
    //ici nous réécrivons ToString pour les classes Disque et Livre et Equals pour la classe Livre

    class Disque
    {
        private string mArtiste;
        string mTitre;
        int mAnnéeCréation;

        public Disque(string titre, int année, string artiste)
        {
            mTitre = titre;
            mAnnéeCréation = année;
            mArtiste = artiste;
        }

        //on réécrit ToString
        public override string ToString()
        {
            return $"Le disque {mTitre} a été enregistré par {mArtiste} en {mAnnéeCréation}";
        }
    }

    class Livre
    {
        protected string mTitre;
        int mAnnéeSortie;
        string mAuteur;

        public Livre(string titre, int année, string auteur)
        {
            mTitre = titre;
            mAnnéeSortie = année;
            mAuteur = auteur;
        }

        //on réécrit ToString
        public override string ToString()
        {
            return $"Le livre {mTitre} a été écrit par {mAuteur} en {mAnnéeSortie}";
        }

        //on réécrit Equals, ici on décide par exemple que l'année n'a pas d'importance (une nouvelle édition par exemple)
        public override bool Equals(object obj)
        {
            if (mTitre == ((Livre)obj).mTitre && mAuteur == ((Livre)obj).mAuteur)
                return true;
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            Livre l = new Livre("Attenti al Gorilla", 1995, "Sandrone Dazieri");
            Disque d = new Disque("Underground", 2007, "Chris Potter");

            Write($"{l.GetType().Name} : ");
            WriteLine(l);

            Write($"{d.GetType().Name} : ");
            WriteLine(d);

            Livre l2 = new Livre("Attenti al Gorilla", 1997, "Sandrone Dazieri");
            Livre l3 = new Livre("Attenti", 1995, "Sandrone Dazieri");
            if (l2.Equals(l))
                WriteLine("l2 = l");
            if (l3.Equals(l))
                WriteLine("l3 = l");
        }
    }
}
