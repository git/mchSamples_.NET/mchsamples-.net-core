﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace HtmlDecode
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient wc = new WebClient();
            string result = wc.DownloadString(new Uri(@"http://radiofrance-podcast.net/podcast09/rss_13100.xml", UriKind.Absolute));


            result = HttpUtility.HtmlDecode(result);

            Console.WriteLine(result);


            //XDocument xdom = XDocument.Parse(result);
            //var items = xdom.Descendants("item");
            //Console.WriteLine(items.ElementAt(0).Element("description").Value);
        }
    }
}
