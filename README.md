# mchSamples C# .NET 

## Tools to install

### Integrated Development Environment
These samples have been updated to .NET 6.0. You only need Visual Studio 2022 or Visual Studio 2019 tu run these samples.  
Visual Studio 2022 can be found here : https://visualstudio.microsoft.com/fr/vs/  
Visual Studio 2019 can be found here : https://visualstudio.microsoft.com/fr/vs/older-downloads/  

### Docker image
If you need a docker image of .NET 6.0, you can use this one : mcr.microsoft.com/dotnet/sdk:6.0

### .NET 6.0 SDK and runtime
If you want to use the SDK and runtime of .NET 6.0 without using Visual Studio, you can find them here : https://dotnet.microsoft.com/en-us/download/dotnet/6.0
