﻿using System;
using System.Runtime.CompilerServices;

namespace ex_026_000_ClassLibrary
{
    public class Artist : IEquatable<Artist>
    {
        public string FirstName
        {
            get => firstname;
            private set
            {
                firstname = value;
                if(string.IsNullOrWhiteSpace(firstname))
                {
                    firstname = "";
                }
            }
        }
        private string firstname;

        public string LastName
        {
            get => lastname;
            private set
            {
                lastname = value;
                if(string.IsNullOrWhiteSpace(lastname))
                {
                    lastname = "";
                }
            }
        }
        private string lastname;

        public Artist(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            if(string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
            {
                throw new ArgumentException("Artist should have at least a first name or a last name");
            }
        }

        public bool Equals(Artist other)
        {
            return FirstName.Equals(other.FirstName)
                && LastName.Equals(other.LastName);
        }

        public override bool Equals(object obj)
        {
            if(ReferenceEquals(obj, null)) return false;
            if(ReferenceEquals(obj, this)) return true;
            if(GetType() != obj.GetType()) return false;
            return Equals(obj as Artist);
        }

        public override int GetHashCode()
        {
            return LastName.GetHashCode();
        }
    }
}

