﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ex_026_000_ClassLibrary
{
    public class Album
    {
        public long Id { get; private set; }

        public string Title { get; private set; }

        public DateTime Date { get; private set; }

        public ReadOnlyCollection<Artist> Artists { get; private set; }
        private List<Artist> artists = new List<Artist>();

        public Album(long id, string title, DateTime date, params Artist[] artists)
        {
            Id = id;
            Title = title;
            Date = date;
            Artists = new ReadOnlyCollection<Artist>(this.artists);
            AddArtists(artists);
            
        }

        public Album(string title, DateTime date, params Artist[] artists)
            : this(0, title, date, artists) {}

        public Album(string title, params Artist[] artists)
            : this(0, title, DateTime.Now, artists) {}

        public bool AddArtist(Artist artist)
        {
            if (artists.Contains(artist))
            {
                return false;
            }
            artists.Add(artist);
            return true;
        }

        public IEnumerable<Artist> AddArtists(params Artist[] artists)
        {
            List<Artist> result = new();
            foreach(var a in artists)
            {
                if(AddArtist(a))
                {
                    result.Add(a);
                }
            }
            return result;
        }

        public bool RemoveArtist(Artist artist)
            => artists.Remove(artist);

        public void ClearArtists()
            => artists.Clear();
    }
}

