﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2019-12-08
//
// ========================================================================

using System;
using System.Text;
using static System.Console;

namespace ex_024_005_exceptions_personnalisees
{
    /// <summary>
    /// ma classe d'exception
    /// </summary>
    class MyException : Exception
    {
        public MyException(string msg) : base(msg) { }
    }

    /// <summary>
    /// programme utilisant une exception personnalisée si les deux noms rentrés par les joueurs sont les mêmes
    /// </summary>
    class Program
    {
        static void GetPlayerNames()
        {
            WriteLine("Cet exemple permet de lancer une exception personnalisée si les deux noms rentrés sont les mêmes.");
            WriteLine("Rentrez le nom du premier joueur");
            string player1 = ReadLine();
            WriteLine("Rentrez le nom du deuxième joueur");
            string player2 = ReadLine();

            if (player1 == player2)
            {
                throw new MyException($"Les deux joueurs s'appellent {player1}. Ils doivent avoir des noms différents");
            }
        }

        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            try
            {
                GetPlayerNames();
            }
            catch (MyException exc)
            {
                WriteLine(exc.Message);
            }
            WriteLine("pas de jeu en fait...");
        }
    }
}
