﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
//
// ========================================================================

using System;
using static System.Console;

namespace ex_024_003_exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            //exemple 1
            try
            {
                WriteLine("avant division : ");
                int a = 10, b = 0;
                int c = a / b;
                WriteLine("après division");
            }
            catch (Exception exc)
            {
                WriteLine("erreur de division par 0");
            }

            //exemple 2
            try
            {
                WriteLine("avant d'accéder au tableau");
                int[] tab = { 1, 2, 3 };
                int d = tab[3];
                WriteLine("après");
            }
            catch (Exception exc)
            {
                WriteLine("erreur d'accès au tableau");
            }

            //exemple 3 : on peut catcher des exceptions différentes en spécifiant la classe d'exception
            try
            {
                WriteLine("début");
                int[] tab = { 1, 2, 3 };
                int a = 10, b = 1;
                tab[3] = a / b;
                WriteLine("après");
            }

            catch (ArithmeticException exc)
            {
                WriteLine("erreur sur opération arithmétique");
            }
            catch (IndexOutOfRangeException exc)
            {
                WriteLine("erreur d'accès à un tableau");
            }
            catch (Exception exc)
            {
                WriteLine("je crois que nous avons un problème");
            }
            finally
            {
                WriteLine("fin des tests");
            }
        }
    }
}
