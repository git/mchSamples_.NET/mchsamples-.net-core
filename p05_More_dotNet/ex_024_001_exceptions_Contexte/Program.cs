﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
//
// ========================================================================

using static System.Console;

namespace ex_024_001_exceptions_Contexte
{
    class Program
    {
        /// <summary>
        /// Deux exemples d'exception lancée par le système
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //WriteLine("exemple d'exception gérée par le système : ");
            //int a = 10, b = 0;
            //int c = a / b;
            //WriteLine("pas d'exception gérée");

            WriteLine("exemple d'exception gérée par le système : ");
            int[] tab = { 1, 2, 3 };
            int d = tab[3];
            WriteLine("pas d'exception gérée");
        }
    }
}
