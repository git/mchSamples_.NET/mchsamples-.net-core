﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-02
// Mise à jour   : 2019-12-08
//
// ========================================================================

using System;
using System.Text;
using static System.Console;

namespace ex_024_004_exceptions
{
    /// <summary>
    /// cet exemple montre comment lancer une exception depuis une méthode pour quelle puisse être attrapée par l'appelant
    /// Vous pouvez lancer le programme en rentrant les deux suites de nombres suivantes :
    /// A. 1,2,3 et vous obtiendrez une IndexOutOfRangeException
    /// B. 1,0,2 et vous obtiendrez une ArithmeticException
    /// 
    /// 1. on rentre dans le bloc try du Main
    ///   2. à l'intérieur, fonction() est appelé
    ///     3. on rentre dans le bloc try de fonction()
    ///       4. une exception est lancée 
    ///     5. on sort du bloc try de fonction()
    ///     6. on rentre dans le bloc catch correspondant à l'exception lancée dans fonction()
    ///       7a. si vous rentrez dans catch (IndexOutOfRangeException), il n'y a pas de propagation
    ///       7b. si vous rentrez dans catch (ArithmeticException), celui-ci propage l'exception en la lançant avec : throw new ArithmeticException();
    ///     8. on sort du bloc catch de fonction()
    ///     9. on exécute le bloc finally dans fonction() (celui-ci est toujours exécuté à la fin d'un try, même s'il n'y a pas d'exception)
    ///   10. on sort de fonction()
    ///   11a. si vous avez eu IndexOutOfRangeException, comme il n'y a pas eu de propagation, le reste du try du Main s'exécute normalement, puis le bloc finally
    ///   11b. si vous avez eu ArithmeticException, comme il y a eu propagation, vous vous retrouvez dans le bloc catch(ArithmeticException), puis le bloc finally
    ///   
    /// </summary>
    class Program
    {
        /// <summary>
        /// une fonction qui fait des trucs et qui peut lancer une exception
        /// </summary>
        /// <exception cref="ArithmeticException">il est conseillé de rajouter une balise "exception" dans les commentaires de la fonction pour préciser qu'elle peut être lancée</exception>
        /// <exception cref="IndexOutOfRangeException">et hop</exception>
        static void fonction()
        {
            WriteLine("soit : int[] tab = { 1, 2, 3 };");
            WriteLine("nous allons calculer : tab[c] = a / b;");
            WriteLine("vous pouvez tester des exceptions en rentrant b = 0, ou c en dehors des limites du tableau.");
            WriteLine("Rentrez a :");
            string s1 = ReadLine();
            WriteLine("Rentrez b :");
            string s2 = ReadLine();
            WriteLine("Rentrez c :");
            string s3 = ReadLine();
            Int32.TryParse(s1, out var a);
            Int32.TryParse(s2, out var b);
            Int32.TryParse(s3, out var c);
            int[] tab = { 1, 2, 3 };
            try
            {
                WriteLine("début du try de fonction");
                tab[c] = a / b;
                WriteLine("fin du groupe try de fonction");
            }

            //mettez ou enlevez les commentaires sur les catch pour voir les différences de résultats
            catch (ArithmeticException)
            {
                WriteLine("ArithmeticException dans fonction");
                throw new ArithmeticException();
            }
            catch (IndexOutOfRangeException)
            {
                WriteLine("IndexOutOfRangeException dans fonction");
            }
            finally
            {
                WriteLine("finally dans fonction");
            }
        }

        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            try
            {
                WriteLine("début du try de Main");
                fonction();
                WriteLine("fin du groupe try de Main");
            }
            catch (ArithmeticException)
            {
                WriteLine("ArithmeticException dans Main");
            }
            catch (IndexOutOfRangeException)
            {
                WriteLine("IndexOutOfRangeException dans Main");
            }
            finally
            {
                WriteLine("finally dans Main");
            }
        }
    }
}
