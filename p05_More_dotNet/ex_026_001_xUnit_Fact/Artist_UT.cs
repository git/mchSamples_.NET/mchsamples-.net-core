﻿using Xunit;
using System;
using ex_026_000_ClassLibrary;

namespace ex_026_001_xUnit_Fact
{
    public class Artist_UT
    {
        [Fact]
        public void TestConstructorWithValidNames()
        {
            Artist a = new Artist("Neil", "Young");
            Assert.NotNull(a);
            Assert.Equal("Neil", a.FirstName);
            Assert.Equal("Young", a.LastName);
        }

        [Fact]
        public void TestConstructorWithInvalidNames()
        {
            Assert.Throws<ArgumentException>(() => new Artist(null, null));
        }
    }
}
