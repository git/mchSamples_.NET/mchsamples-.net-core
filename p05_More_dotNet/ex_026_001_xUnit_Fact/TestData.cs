﻿using System;
using ex_026_000_ClassLibrary;

namespace ex_026_001_xUnit_Fact
{
    public static class TestData
    {
        public static IEnumerable<object[]> Data_AddArtistsToAlbum()
        {
            yield return new object[]
            {
                1,
                new Artist[]
                {
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter"),
                    new Artist("Tony", "Williams")
                },
                new Artist[]
                {
                    new Artist("Tony", "Williams")
                },
                new Album("Miles Smiles", new DateTime(1967, 1, 1),
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter")),
                new Artist("Tony", "Williams")
            };

            yield return new object[]
            {
                2,
                new Artist[]
                {
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter"),
                    new Artist("Tony", "Williams")
                },
                new Artist[]
                {
                    new Artist("Ron", "Carter"),
                    new Artist("Tony", "Williams")
                },
                new Album("Miles Smiles", new DateTime(1967, 1, 1),
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock")),
                new Artist("Ron", "Carter"),
                new Artist("Miles", "Davis"),
                new Artist("Tony", "Williams")
            };

            yield return new object[]
            {
                1,
                new Artist[]
                {
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter"),
                    new Artist("Tony", "Williams")
                },
                new Artist[]
                {
                    new Artist("Tony", "Williams")
                },
                new Album("Miles Smiles", new DateTime(1967, 1, 1),
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter")),
                new Artist("Tony", "Williams"),
                new Artist("Tony", "Williams")
            };
        }
    }
}

