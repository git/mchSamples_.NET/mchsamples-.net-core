﻿using System;
using ex_026_000_ClassLibrary;
using Xunit;

namespace ex_026_001_xUnit_Fact
{
    public class Album_UT
    {
        public static IEnumerable<object[]> Data_AddArtistToAlbum()
        {
            yield return new object[]
            {
                true,
                new Artist[]
                {
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter"),
                    new Artist("Tony", "Williams")
                },
                new Album("Miles Smiles", new DateTime(1967, 1, 1),
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter")),
                new Artist("Tony", "Williams")
            };
            yield return new object[]
            {
                false,
                new Artist[]
                {
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter")
                },
                new Album("Miles Smiles", new DateTime(1967, 1, 1),
                    new Artist("Miles", "Davis"),
                    new Artist("Wayne", "Shorter"),
                    new Artist("Herbie", "Hancock"),
                    new Artist("Ron", "Carter")),
                new Artist("Ron", "Carter")
            };
        }

        [Theory]
        [MemberData(nameof(Data_AddArtistToAlbum))]
        public void Test_AddArtistToAlbum(bool expectedResult,
                                          IEnumerable<Artist> expectedArtists,
                                          Album album,
                                          Artist artistToAdd)
        {
            bool result = album.AddArtist(artistToAdd);
            Assert.Equal(expectedResult, result);
            Assert.Equal(expectedArtists.Count(), album.Artists.Count());
            Assert.All(expectedArtists, a => album.Artists.Contains(a));
        }

        [Theory]
        [MemberData(nameof(TestData.Data_AddArtistsToAlbum), MemberType=typeof(TestData))]
        public void Test_AddArtistsToAlbum(int expectedResult,
                                           IEnumerable<Artist> expectedArtists,
                                           IEnumerable<Artist> expectedAddedArtists,
                                           Album album,
                                           params Artist[] artistsToAdd)
        {
            var addedArtists = album.AddArtists(artistsToAdd);
            Assert.Equal(expectedResult, addedArtists.Count());
            Assert.All(expectedAddedArtists, a => addedArtists.Contains(a));

            Assert.Equal(expectedArtists.Count(), album.Artists.Count());
            Assert.All(expectedArtists, a => album.Artists.Contains(a));
        }
    }
}

