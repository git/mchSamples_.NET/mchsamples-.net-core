﻿using System;
using ex_026_000_ClassLibrary;
using Xunit;
namespace ex_026_001_xUnit_Fact
{
    public class Artist_InlineData_UT
    {
        [Theory]
        [InlineData(true, "Neil", "Young", "Neil", "Young")]
        [InlineData(false, "", "", "", "")]
        [InlineData(false, "", "", "", "    ")]
        [InlineData(false, "", "", "    ", "")]
        [InlineData(false, "", "", "", null)]
        [InlineData(false, "", "", null, null)]
        [InlineData(false, "", "", "  ", null)]
        [InlineData(true, "", "Young", "   ", "Young")]
        [InlineData(true, "", "Young", "", "Young")]
        [InlineData(true, "", "Young", null, "Young")]
        [InlineData(true, "Neil", "", "Neil", "")]
        [InlineData(true, "Neil", "", "Neil", null)]
        [InlineData(true, "Neil", "", "Neil", "  ")]
        public void TestConstructor(bool isValid, string expectedFirstName, string expectedLastName,
            string firstname, string lastname)
        {
            if(!isValid)
            {
                Assert.Throws<ArgumentException>(
                    () => new Artist(firstname, lastname));
                return;
            }

            Artist a = new Artist(firstname, lastname);
            Assert.Equal(expectedFirstName, a.FirstName);
            Assert.Equal(expectedLastName, a.LastName);
        }
    }
}

