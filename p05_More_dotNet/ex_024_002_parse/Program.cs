﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
//
// ========================================================================

using System;
using static System.Console;
using System.Linq;
using System.Text;

namespace ex_024_002_parse
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;

            int n;
            double d;
            bool res;

            string choix = String.Empty;
            string[] tabChoix = { "1", "2", "3" };

            string s = String.Empty;

            while (!tabChoix.Contains(choix))
            {
                Clear();
                WriteLine("Que voulez-vous faire ?");
                WriteLine("1. Entrer un entier ?");
                WriteLine("2. Entrer un réel en utilisant la culture de votre ordinateur ?");
                WriteLine("   (si votre windows est en français, la virgule s'écrit avec une , )");
                WriteLine("3. Entrer un réel avec la culture US ?");
                WriteLine("   (la virgule s'écrit avec un . )");
                choix = ReadLine();
            }

            Clear();

            switch (choix)
            {
                case "1":
                    WriteLine("Entrez une chaîne de caractères, je vais tenter de la convertir en entier :");
                    s = ReadLine();
                    //1. Parse
                    //n = Int32.Parse(s); //lance une exception si Parse échoue
                    //2. TryParse : renvoie true si le Parse réussit, false s'il échoue
                    //              le résultat est donné dans le paramètre de sortie
                    res = Int32.TryParse(s, out n);
                    if (res == true)
                    {
                        WriteLine(n);
                    }
                    else
                    {
                        WriteLine("conversion impossible");
                    }
                    break;

                case "2":
                    WriteLine("Entrez une chaîne de caractères, je vais tenter de la convertir en réel en utilisant la culture actuelle :");
                    s = ReadLine();
                    //1. Parse
                    //d = Double.Parse(s); //lance une exception si Parse échoue
                    //2. TryParse : renvoie true si le Parse réussit, false s'il échoue
                    //              le résultat est donné dans le paramètre de sortie
                    res = Double.TryParse(s, out d);
                    if (res == true)
                    {
                        WriteLine(d);
                    }
                    else
                    {
                        WriteLine("conversion impossible");
                    }
                    break;

                case "3":
                    WriteLine("Entrez une chaîne de caractères, je vais tenter de la convertir en réel en utilisant la culture US :");
                    s = ReadLine();
                    //1. Parse
                    //d = Double.Parse(s); //lance une exception si Parse échoue
                    //2. TryParse : renvoie true si le Parse réussit, false s'il échoue
                    //              le résultat est donné dans le paramètre de sortie
                    res = Double.TryParse(s, System.Globalization.NumberStyles.Number, new System.Globalization.CultureInfo("en-US"), out d);
                    if (res == true)
                    {
                        WriteLine(d);
                    }
                    else
                    {
                        WriteLine("conversion impossible");
                    }
                    break;
            }
        }
    }
}
