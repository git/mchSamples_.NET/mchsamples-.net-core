﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-30
//
// ========================================================================

using System;

namespace ex_023_017_Deep_Read_only_collections_dll
{
    /// <summary>
    /// Nounours est un type mutable, interne (pas de modificateur de visibilité devant class Nounours, donc par défaut, c'est internal)
    /// Le type Nounours n'est donc accessible qu'à l'intérieur de l'assemblage dans lequel il est défini.
    /// </summary>
    class Nounours : INounours
    {
        public int Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public DateTime DateDeNaissance
        {
            get;
            private set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public Nounours(int id, string name, DateTime dateDeNaissance, int nbPoils)
        {
            Id = id;
            Name = name;
            DateDeNaissance = dateDeNaissance;
            NbPoils = nbPoils;
        }
    }
}
