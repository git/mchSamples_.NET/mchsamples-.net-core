﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using System;
using System.Collections.Generic;
using static System.Console;

namespace ex_023_006_Stack
{
    class Program
    {
        static void Display(Stack<int> stack)
        {
            Write("stack :");
            foreach (int i in stack) Write($"{i} ");
            WriteLine();
        }

        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(1); Display(stack);
            stack.Push(2); Display(stack);
            stack.Push(3); Display(stack);
            WriteLine(stack.Count);
            WriteLine(stack.Peek()); Display(stack);
            WriteLine(stack.Pop()); Display(stack);
            WriteLine(stack.Pop()); Display(stack);
            WriteLine(stack.Pop()); Display(stack);

            try
            {
                WriteLine(stack.Pop()); Display(stack);
            }
            catch (Exception e)
            {
                WriteLine(e);
            }
        }
    }
}
