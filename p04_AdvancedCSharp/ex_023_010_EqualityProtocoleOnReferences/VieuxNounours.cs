﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : VieuxNounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

namespace ex_023_010_EqualityProtocoleOnReferences
{
    class VieuxNounours
    {
        public int Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public VieuxNounours(int id, string name)
        {
            Id = id;
            Name = name;
        }

        //...
    }
}
