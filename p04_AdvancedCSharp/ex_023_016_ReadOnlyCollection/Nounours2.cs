﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours2.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-30
//
// ========================================================================

using System;

namespace ex_023_016_ReadOnlyCollection
{
    /// <summary>
    /// Nounours est mutable (Name et NbPoils ont un setter public)
    /// </summary>
    class Nounours2
    {
        public int Id
        {
            get;
            private set;
        }

        public string Nom
        {
            get;
            set;
        }

        public DateTime DateDeNaissance
        {
            get;
            private set;
        }

        public int NbPoils
        {
            get;
            set;
        }

        public Nounours2(int id, string nom, DateTime dateDeNaissance, int nbPoils)
        {
            Id = id;
            Nom = nom;
            DateDeNaissance = dateDeNaissance;
            NbPoils = nbPoils;
        }
    }
}
