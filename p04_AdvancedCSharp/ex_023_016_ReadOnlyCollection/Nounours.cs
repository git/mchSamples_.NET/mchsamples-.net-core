﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-30
//
// ========================================================================

using System;

namespace ex_023_016_ReadOnlyCollection
{
    /// <summary>
    /// Nounours est immuable
    /// </summary>
    struct Nounours
    {
        public int Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public DateTime DateDeNaissance
        {
            get;
            private set;
        }

        public int NbPoils
        {
            get;
            private set;
        }

        public Nounours(int id, string name, DateTime dateDeNaissance, int nbPoils)
        {
            Id = id;
            Name = name;
            DateDeNaissance = dateDeNaissance;
            NbPoils = nbPoils;
        }
    }
}
