﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : MonLit.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-01-10
//
// ========================================================================

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ex_023_018_ReadOnlyDictionaries
{
    /// <summary>
    /// L'exemple suivant montre comment encapsuler un dictionnaire dans les cas suivants :
    /// => les clés et/ou valeurs sont mutables, c'est-à-dire qu'on peut modifier leur état interne en appelant leurs méthodes ou propriétés
    ///     => MonLit possède un dictionnaire de paire clé (string)- valeur (Nounours) privée mNounoursDico où Nounours est mutable
    /// Comment encapsuler correctement un dictionnaire de ce type ?
    /// Dans l'exemple suivant, nous voulons protéger le dictionnaire contre :
    /// - les modifications du dictionnaire (ajout ou suppression d'éléments),
    /// - la modification du dictionnaire lui-même (on ne doit pas pouvoir faire de new)
    /// 
    /// En résumé, nous cherchons à rendre publique en lecture seule, le dictionnaire privé, en laissant chacun de ses éléments modifiables.
    /// 
    /// La bonne solution consiste à :
    /// - envelopper (wrapper) le dictionnaire privé avec un ReadOnlyDictionary construit dans le constructeur de MonLit.
    /// - tant qu'en interne, dans la classe MonLit, on ne change pas la référence de mNounoursDico, le wrapper donne accès en lecture seule au dictionnaire
    /// - attention de bien penser à le reconstruire si la référence change !!!
    public class MonLit
    {
        //on veut encapsuler une collection d'un type muable
        private Dictionary<string, Nounours> mNounoursDico = new Dictionary<string, Nounours>
        {
            ["Beluga"] = new Nounours(0, "Beluga", new DateTime(2012, 07, 29), 0),
            ["Singe"] = new Nounours(1, "Singe", new DateTime(2009, 08, 09), 1345),
            ["Girafe"] = new Nounours(2, "Girafe", new DateTime(2007, 11, 02), 567)
        };

        public ReadOnlyDictionary<string, Nounours> Nounours
        {
            get;
            private set;
        }

        public MonLit()
        {
            Nounours = new ReadOnlyDictionary<string, Nounours>(mNounoursDico);
        }
    }
}
