﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using System;
using System.Text;
using static System.Console;

namespace ex_023_004_ArrayClass
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            StringBuilder[] data = new StringBuilder[4];
            data[0] = new StringBuilder("sb1");
            data[1] = new StringBuilder("sb2");
            data[2] = new StringBuilder("sb3");

            //Copie, attention aux références !
            StringBuilder[] data2 = data; //data et data2 pointent sur les mêmes cases mémoire !
            StringBuilder[] data3 = (StringBuilder[])data.Clone(); //la ième case de data et la ième case de data3 contiennent les mêmes références

            //Length, Rank, GetLength
            int[,,] tab = { { {1, 2, 3}, {4, 5, 6} },
                            { {7, 8, 9}, {0, 1, 2} },
                            { {3, 4, 5}, {6, 7, 8} },
                            { {9, 0, 1}, {2, 3, 4} } };
            WriteLine($"Length: {tab.Length}");
            WriteLine($"Rank: {tab.Rank}");
            WriteLine($"GetLength(0): {tab.GetLength(0)}");
            WriteLine($"GetLength(1): {tab.GetLength(1)}");
            WriteLine($"GetLength(2): {tab.GetLength(2)}");

            //Sort
            WriteLine("Sort");
            int[] nombres = { 3, 2, 1 };
            foreach (int i in nombres) Write($"{i} ");
            WriteLine();
            Array.Sort(nombres);
            foreach (int i in nombres) Write($"{i} ");
            WriteLine();

            //on peut aussi trier deux tableaux en tandem : le premier est trié et les éléments du second prennent les mêmes positions
            int[] nombres2 = { 3, 2, 1 };
            string[] nombres3 = { "trois", "deux", "un" };
            WriteLine("tandem Sort");
            foreach (int i in nombres2) Write($"{i} ");
            WriteLine();
            foreach (string s in nombres3) Write($"{s} ");
            WriteLine();
            Array.Sort(nombres2, nombres3);
            foreach (int i in nombres2) Write($"{i} ");
            WriteLine();
            foreach (string s in nombres3) Write($"{s} ");
            WriteLine();

            //inverser les éléments 
            WriteLine("Reverse");
            int[] nombres4 = { 1, 2, 3, 4, 5, 6 };
            foreach (int i in nombres4) Write($"{i} ");
            WriteLine();
            Array.Reverse(nombres4);
            foreach (int i in nombres4) Write($"{i} ");
            WriteLine();

            //Read only
            System.Collections.ObjectModel.ReadOnlyCollection<int> nombres5 = new System.Collections.ObjectModel.ReadOnlyCollection<int>(nombres4);
            foreach (int i in nombres5) Write($"{i} ");
            WriteLine();
            //nombres5[0] += 10; //erreur de compilation

            //faire des recherches
            int lookedElement = 2;
            Write($"l'élément {lookedElement} se trouve à la position ");
            WriteLine($"{Array.IndexOf<int>(nombres4, lookedElement)} dans le tableau nombres4");
            WriteLine();

            Write("Le premier élément du tableau nombres3 à contenir un 'u' est ");
            ForegroundColor = ConsoleColor.Red;
            WriteLine(Array.Find<string>(nombres3, n => n.Contains("u")));
            ForegroundColor = ConsoleColor.White;
            WriteLine();

            Write("Les éléments du tableau nombres3 qui contiennent un 'u' sont ");
            ForegroundColor = ConsoleColor.Red;
            foreach (string s in Array.FindAll<string>(nombres3, n => n.Contains("u")))
            {
                Write($"{s} ");
            }
            ForegroundColor = ConsoleColor.White;
            WriteLine();
            WriteLine();

            string àchercher = "deux";
            Write($"existe-t-il un élément nommé {àchercher} dans le tableau nombres3 ? {Array.Exists<string>(nombres3, n => n.Equals(àchercher))}");
            WriteLine();

            àchercher = "quatre";
            Write($"existe-t-il un élément nommé {àchercher} dans le tableau nombres3 ? {Array.Exists<string>(nombres3, n => n.Equals(àchercher))}");
            WriteLine();

        }
    }
}
