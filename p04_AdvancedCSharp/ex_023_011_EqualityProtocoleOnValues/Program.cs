﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using static System.Console;

namespace ex_023_011_EqualityProtocoleOnValues
{
    class Program
    {
        static void Main(string[] args)
        {
            VieuxNounours grouinf = new VieuxNounours(1, "grouinf");
            VieuxNounours grouinf2 = new VieuxNounours(1, "grouinf2");
            //WriteLine("grouinf == grouinf2 ? " + (grouinf == grouinf2));
            WriteLine($"grouinf.Equals(grouinf2) ? {grouinf.Equals(grouinf2)}");

            Nounours grouinf3 = new Nounours(1, "grouinf3");
            Nounours grouinf4 = new Nounours(1, "grouinf4");
            WriteLine($"grouinf3 == grouinf4 ? {grouinf3 == grouinf4}");
            WriteLine($"grouinf3.Equals(grouinf4) ? {grouinf3.Equals(grouinf4)}");
        }
    }
}
