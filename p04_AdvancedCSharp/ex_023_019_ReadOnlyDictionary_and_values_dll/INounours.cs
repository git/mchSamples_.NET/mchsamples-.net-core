﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : INounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-10-01
//
// ========================================================================

using System;

namespace ex_023_019_ReadOnlyDictionary_and_values_dll
{
    /// <summary>
    /// INounours est une façade immuable de Nounours :
    /// Nounours est un type mutable interne, mais INounours n'offre accès publiquement qu'à des méthodes de lecture.
    /// En accédant à la référence via le type INounours, elle aparaît immuable, en façade.
    /// </summary>
    public interface INounours
    {
        int Id
        {
            get;
        }

        string Nom
        {
            get;
        }

        DateTime DateDeNaissance
        {
            get;
        }

        int NbPoils
        {
            get;
        }
    }
}
