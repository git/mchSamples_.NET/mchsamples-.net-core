﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using static System.Console;
using System.Collections.Generic;

namespace ex_023_008_List
{
    class Program
    {
        static void Display(List<string> colors)
        {
            WriteLine("début");
            foreach (string s in colors)
            {
                WriteLine($"\t{s}");
            }
            WriteLine("fin");
        }

        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            List<string> colors = new List<string>();
            WriteLine("Add");
            colors.Add("rouge");
            colors.Add("jaune");
            Display(colors);

            WriteLine("AddRange");
            colors.AddRange(new string[] { "vert", "bleu", "orange" });
            Display(colors);

            WriteLine("Insert");
            colors.Insert(1, "violet");
            Display(colors);

            WriteLine("InsertRange");
            colors.InsertRange(3, new string[] { "beige", "gris", "noir", "rose" });
            Display(colors);

            WriteLine("Remove");
            colors.Remove("bleu");
            colors.RemoveAt(2);
            colors.RemoveRange(3, 2);
            Display(colors);

            WriteLine("RemoveAll string starting with v");
            colors.RemoveAll(delegate (string s) { return s.StartsWith("v"); });
            Display(colors);

            WriteLine("indexer et Count");
            WriteLine(colors[0]);
            WriteLine(colors[colors.Count - 1]);

            WriteLine("subsets");
            Display(colors.GetRange(1, 2));

            WriteLine("ToArray");
            string[] tab = colors.ToArray();
            foreach (string s in tab) WriteLine($"{s} ");
            WriteLine();

            WriteLine("CopyTo");
            colors.CopyTo(2, tab, 0, 2); //copie, à partir du 3 ème élément de colors, dans tab à partir du 1er élément, 2 éléments.
            foreach (string s in tab) WriteLine($"{s} ");
            WriteLine();

            //searching and sorting
            WriteLine("Dans Colors, les éléments suivants commencent par 'r' : ");
            foreach (string s in colors.FindAll(n => n.StartsWith("r")))
            {
                WriteLine($"{s} ");
            }
            WriteLine();
        }


    }
}
