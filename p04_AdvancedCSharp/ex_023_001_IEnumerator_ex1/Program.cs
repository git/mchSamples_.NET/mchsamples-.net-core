﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-27
//
// ========================================================================

using static System.Console;
using System.Collections.Generic;

namespace ex_023_001_IEnumerator_ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = { 1, 2, 3 };
            System.Collections.IEnumerator etor = tab.GetEnumerator();

            while (etor.MoveNext())
            {
                int a = (int)etor.Current;
                WriteLine(a);
            }

            WriteLine("ou");

            foreach (int a in tab)
            {
                WriteLine(a);
            }

            //2ème exemple
            string s = "Bonjour";
            System.Collections.IEnumerator str_etor = (s as IEnumerable<char>).GetEnumerator();
            while (str_etor.MoveNext())
            {
                Write($"{str_etor.Current}.");
            }
        }
    }
}
