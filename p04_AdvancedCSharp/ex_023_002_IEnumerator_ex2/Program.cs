﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using static System.Console;

namespace ex_023_002_IEnumerator_ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            StrangeCollection col = new StrangeCollection();
            foreach (int i in col)
            {
                WriteLine(i);
            }
        }
    }
}
