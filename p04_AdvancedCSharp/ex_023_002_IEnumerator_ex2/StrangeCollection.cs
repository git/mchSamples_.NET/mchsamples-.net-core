﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : StrangeCollection.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using System.Collections.Generic;

namespace ex_023_002_IEnumerator_ex2
{
    class StrangeCollection : IEnumerable<int>
    {
        int[] data = { 1, 2, 3, 4, 5, 6 };

        public IEnumerator<int> GetEnumerator()
        {
            foreach (int i in data)
            {
                if (i % 2 == 0)
                {
                    yield return i;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
