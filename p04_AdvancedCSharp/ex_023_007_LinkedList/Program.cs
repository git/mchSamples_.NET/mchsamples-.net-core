﻿// ========================================================================
//
// Copyright (C) 2013-2014 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2014-03-30
// Mise à jour   : 2016-09-28
//
// ========================================================================

using static System.Console;
using System.Collections.Generic;

namespace ex_023_007_LinkedList
{
    class Program
    {
        static void Display(LinkedList<string> notes)
        {
            WriteLine("début");
            foreach (string s in notes)
            {
                Write($"{s} ");
            }
            WriteLine();
            WriteLine("fin");
        }

        static void Main(string[] args)
        {
            OutputEncoding = System.Text.Encoding.UTF8;

            LinkedList<string> notes = new LinkedList<string>();

            WriteLine("AddFirst, AddLast");
            notes.AddFirst("do");
            notes.AddLast("sol");
            Display(notes);

            WriteLine("AddAfter, AddBefore, First, Next, Last");
            notes.AddAfter(notes.First, "re");
            notes.AddAfter(notes.First.Next, "mi");
            notes.AddBefore(notes.Last, "fa");
            Display(notes);

            WriteLine("RemoveFirst, RemoveLast");
            notes.RemoveFirst();
            notes.RemoveLast();
            Display(notes);

            WriteLine("manipulation de nodes");
            LinkedListNode<string> mi = notes.Find("mi");
            notes.Remove(mi);
            notes.AddFirst(mi);
            Display(notes);
        }
    }
}
