﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.EqualityComparer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System;
using System.Collections.Generic;

namespace ex_023_012_EqualityComparer
{
    /// <summary>
    /// Très bien quand on veut un deuxième equality comparer qui n'est pas celui par défaut, et qu'on a accès au code source de la classe
    /// </summary>
    partial class Nounours
    {
        private class DateDeNaissanceEqualityComparer : EqualityComparer<Nounours>
        {
            public override bool Equals(Nounours x, Nounours y)
            {
                return x.DateDeNaissance == y.DateDeNaissance;
            }

            public override int GetHashCode(Nounours obj)
            {
                return (int)(obj.DateDeNaissance.Ticks % Int32.MaxValue);
            }
        }

        public static IEqualityComparer<Nounours> DateDeNaissanceEqualityCompare
        {
            get
            {
                if (mDateDeNaissanceEqualityCompare == null)
                {
                    mDateDeNaissanceEqualityCompare = new DateDeNaissanceEqualityComparer();
                }
                return mDateDeNaissanceEqualityCompare;
            }
        }
        private static DateDeNaissanceEqualityComparer mDateDeNaissanceEqualityCompare = null;
    }
}
