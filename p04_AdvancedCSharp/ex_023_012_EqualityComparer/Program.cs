﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System;
using static System.Console;

namespace ex_023_012_EqualityComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            Nounours chucky1 = new Nounours(1, "chucky 1", new DateTime(1988, 11, 9), 150);
            Nounours chucky2 = new Nounours(2, "chucky 2", new DateTime(1988, 11, 9), 125);
            Nounours chucky3 = new Nounours(3, "chucky 3", new DateTime(1992, 11, 9), 125);
            WriteLine($"chucky 1 == chucky 2 ? {chucky1 == chucky2}");
            WriteLine($"chucky1.Equals(chucky2) ? {chucky1.Equals(chucky2)}");
            WriteLine($"Nounours.DateDeNaissanceEqualityCompare.Equals(chucky1, chucky2) ? {Nounours.DateDeNaissanceEqualityCompare.Equals(chucky1, chucky2)}");
            WriteLine($"new NounoursNbPoilsEqualityComparer().Equals(chucky2, chucky3) ? {new NounoursNbPoilsEqualityComparer().Equals(chucky2, chucky3)}");
        }
    }
}
