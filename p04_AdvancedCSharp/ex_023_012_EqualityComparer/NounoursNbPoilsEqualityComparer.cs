﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.NbPoilsEqualityComparer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System.Collections.Generic;

namespace ex_023_012_EqualityComparer
{
    /// <summary>
    /// Lorsqu'on n'a pas accès au code source de la classe, on doit passer par un equality comparer externe comme celui-ci
    /// </summary>
    class NounoursNbPoilsEqualityComparer : EqualityComparer<Nounours>
    {
        public override bool Equals(Nounours x, Nounours y)
        {
            return x.NbPoils == y.NbPoils;
        }

        public override int GetHashCode(Nounours obj)
        {
            return obj.NbPoils;
        }
    }
}
