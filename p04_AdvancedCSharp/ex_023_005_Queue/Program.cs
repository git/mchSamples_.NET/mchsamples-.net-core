﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-28
//
// ========================================================================

using System;
using System.Collections.Generic;
using static System.Console;

namespace ex_023_005_Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue("client 1");
            queue.Enqueue("client 2");
            queue.Enqueue("client 3");
            queue.Enqueue("client 4");
            WriteLine("ToArray");
            string[] tab = queue.ToArray();
            foreach (string s in tab) WriteLine(s);

            WriteLine("Count");
            WriteLine(queue.Count);
            WriteLine("Peek"); // prend le prochain élément qui sortira, sans le sortir
            WriteLine(queue.Peek());
            WriteLine("Dequeue"); // sort le prochain élément et le rend
            WriteLine(queue.Dequeue());
            WriteLine(queue.Dequeue());
            WriteLine(queue.Dequeue());
            WriteLine(queue.Dequeue());
            try
            {
                WriteLine(queue.Dequeue()); //exception
            }
            catch (Exception e)
            {
                WriteLine(e);
            }
        }

    }
}
