﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Program.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System;
using static System.Console;

namespace ex_023_013_OrderComparisonProtocole
{
    class Program
    {
        static void Main(string[] args)
        {

            Nounours[] monLit = { new Nounours(1, "Beluga", new DateTime(2012, 07, 29), 0, 35f),
                                    new Nounours(2, "Singe", new DateTime(2009, 08, 09), 1345, 15f),
                                    new Nounours(3, "Girafe", new DateTime(2007, 11, 02), 567, 215f)};
            foreach (Nounours n in monLit)
            {
                WriteLine(n.Name);
            }
            WriteLine("*****************");

            Array.Sort(monLit);
            foreach (Nounours n in monLit)
            {
                WriteLine(n.Name);
            }
            WriteLine("*****************");

            Array.Sort(monLit, Nounours.NbPoilsCompare);
            foreach (Nounours n in monLit)
            {
                WriteLine(n.Name);
            }
            WriteLine("*****************");

            Array.Sort(monLit, new NounoursDateComparer());
            foreach (Nounours n in monLit)
            {
                WriteLine(n.Name);
            }
            WriteLine("*****************");
        }
    }
}
