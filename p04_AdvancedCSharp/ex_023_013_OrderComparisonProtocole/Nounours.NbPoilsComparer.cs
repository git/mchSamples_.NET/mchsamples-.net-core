﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : Nounours.NbPoilsComparer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System;
using System.Collections.Generic;

namespace ex_023_013_OrderComparisonProtocole
{
    /// <summary>
    /// Si on a besoin d'un autre compareur et qu'on a accès au code source, on peut ajouter un IComparer générique statique à la classe
    /// </summary>
    partial class Nounours : IComparable<Nounours>, IComparable
    {
        private class NbPoilsComparer : IComparer<Nounours>
        {
            int IComparer<Nounours>.Compare(Nounours left, Nounours right)
            {
                return left.NbPoils.CompareTo(right.NbPoils);
            }
        }

        public static IComparer<Nounours> NbPoilsCompare
        {
            get
            {
                if (nbPoilsCompare == null)
                {
                    nbPoilsCompare = new NbPoilsComparer();
                }
                return nbPoilsCompare;
            }
        }
        private static NbPoilsComparer nbPoilsCompare = null;
    }
}
