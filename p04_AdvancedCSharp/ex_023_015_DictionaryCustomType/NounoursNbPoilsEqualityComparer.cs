﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : NounoursNbPoilsEqualityComparer.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System.Collections.Generic;

namespace ex_023_015_DictionaryCustomType
{
    class NounoursNbPoilsEqualityComparer : EqualityComparer<Nounours>
    {
        public override bool Equals(Nounours x, Nounours y)
        {
            return x.NbPoils == y.NbPoils;
        }

        public override int GetHashCode(Nounours obj)
        {
            return obj.NbPoils;
        }
    }
}
