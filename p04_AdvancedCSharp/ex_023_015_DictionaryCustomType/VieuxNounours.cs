﻿// ========================================================================
//
// Copyright (C) 2016-2017 MARC CHEVALDONNE
//                         marc.chevaldonne.free.fr
//
// Module        : VieuxNounours.cs
// Author        : Marc Chevaldonné
// Creation date : 2016-09-29
//
// ========================================================================

using System;

namespace ex_023_015_DictionaryCustomType
{
    class VieuxNounours
    {
        public string Name
        {
            get;
            private set;
        }

        public DateTime DateDeNaissance
        {
            get;
            private set;
        }

        public int NbPoils
        {
            get;
            private set;
        }

        public VieuxNounours(string name, DateTime dateDeNaissance, int nbPoils)
        {
            Name = name;
            DateDeNaissance = dateDeNaissance;
            NbPoils = nbPoils;
        }
    }
}
